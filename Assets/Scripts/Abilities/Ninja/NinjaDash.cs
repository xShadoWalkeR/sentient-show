﻿using System.Collections;
using System.Collections.Generic;
using Controller;
using Photon.Pun;
using UnityEngine;

/// <summary>
/// The ninja's dash ability
/// </summary>
public class NinjaDash : Ability
{
    [SerializeField] private float dashForce = 160f;    // The dash's speed
    [SerializeField] private float dashDuration = 0.16f;    // The dash's duration
    [SerializeField] private float dashDamage = 100f; // The dash's damage
    
    [SerializeField] public override float AbilityCooldown { get; } = 6f;   // The dash total cooldown

    // Override the event from our parent
    public override UnityEngine.Events.UnityEvent PlayerCast { get; set; }

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // If it's a charging ability
    public override bool IsCharged => false;

    // Support variable so we can only chage it's values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // Our player controller
    private NinjaController controller;
    private CharacterController charController;
    private Camera myCam;

    // Get our enemy
    private Player enemy;
    private List<CharacterController> enemies;

    private Player player;

    private WaitForSeconds delay;

    // Array for hits
    RaycastHit[] hits;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    private void Start()
    {
        PlayerCast = new UnityEngine.Events.UnityEvent();

        player = GetComponent<Player>();

        delay = new WaitForSeconds(0.1f);

        // Get our character controller
        controller = GetComponent<NinjaController>();
        charController = GetComponent<CharacterController>();
        enemies = new List<CharacterController>();
        myCam = Camera.main;

        // Initiallise stuff
        currentCooldown = AbilityCooldown;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    private void Update()
    {
        if (!photonView.IsMine)
            return;

        // If our ability is on cooldown
        if (IsOnCooldown)
        {
            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0)
            {
                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }
    }

    /// <summary>
    /// Casts the ability
    /// </summary>
    /// <returns>The duration of the dash</returns>
    public override IEnumerator Cast()
    {
        PlayerCast.Invoke();

        // Update animation state
        player.TpAnim.SetBool(AnimationTags.DO_DASH, true);
        player.FpAnim.SetBool(AnimationTags.DO_DASH, true);

        yield return delay;

        if (photonView.IsMine)
        {
            // Get all we'll hit
            hits = Physics.RaycastAll(transform.position + Vector3.up, myCam.transform.forward * dashForce * dashDuration);
            Debug.DrawRay(transform.position + Vector3.up, myCam.transform.forward * dashForce * dashDuration, Color.red, 30f);

            for (int i = 0; i < hits?.Length; i++)
            {
                if (hits[i].transform.TryGetComponent(out enemy))
                {
                    enemies.Add(enemy.GetComponent<CharacterController>());
                    // Ignore collisions while we dash
                    Physics.IgnoreCollision(enemies[enemies.Count - 1], charController, true);

                    // Deal damage to all hit players
                    enemy.photonView.RPC("RPC_TakeDamage", RpcTarget.All, (int)dashDamage, gameObject.name);
                }
            }

            // Adds a force to the player in the direction he's looking (Dash)
            controller.AddForce(myCam.transform.forward, dashForce);

            // Waits for the duration of the dash
            yield return new WaitForSeconds(dashDuration);

            // Stops the dash by removing all the velocity from the player
            controller.ResetImpact();

            // Stop ignoring colisions
            for (int i = 0; i < enemies?.Count; i++)
            {
                Physics.IgnoreCollision(enemies[i], charController, false);
            }
            enemies?.Clear();

            // And we put the ability on cooldown
            isOnCooldown = true;
        }
    }
}
