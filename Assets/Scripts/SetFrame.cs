﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetFrame : MonoBehaviour
{
    public string animationName;
    public int layer;
    public float normalizedTime;

    Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        SetAnimationFrame();
    }

    private void SetAnimationFrame()
    {
        _animator.speed = 0f;
        _animator.Play(animationName, layer, normalizedTime);
        _animator.Update(Time.deltaTime);
    }
}
