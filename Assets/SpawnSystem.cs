﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem : MonoBehaviour
{
    private const float friendlyDistanceCap = 8f;
    private const float enemyDistanceCap = 22.5f;

    [SerializeField] private List<Transform> spawnPoints;

    private ScoresTabManager scoresManager;

    private Tuple<float, Transform> bestSpawn;

    private List<Player> enemy;

    private List<Player> friendly;

    private bool isVisible;

    private List<Tuple<float, Transform>> bestSpawns;

    private LayerMask mask;

    public static SpawnSystem spawn;

    /// <summary>
    /// When the object is enabled
    /// </summary>
    private void OnEnable()
    {
        // Set the singleton
        if (SpawnSystem.spawn == null)
        {
            SpawnSystem.spawn = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        mask = ~(1 << 17);

        enemy = new List<Player>();
        friendly = new List<Player>();
        bestSpawns = new List<Tuple<float, Transform>>();

        scoresManager = GetComponent<ScoresTabManager>();
    }

    public Transform RespawnPlayer(Player toRespawn)
    {
        UpdatePlayerLists();

        float closestEnemy = 3000f;

        for(int i = 0; i < spawnPoints.Count; i++)
        {
            closestEnemy = 3000f;
            isVisible = false;

            for (int e = 0; e < enemy.Count; e++)
            {
                float dis;

                if (Physics.Raycast(spawnPoints[i].position, enemy[e].transform.position + Vector3.up, out RaycastHit hit, 500f, mask))
                {
                    if (hit.transform.gameObject.CompareTag(Tags.ENEMY_TAG))
                    {
                        isVisible = true;
                        break;
                    }
                }

                if ((dis = Vector3.Distance(enemy[e].transform.position, spawnPoints[i].position)) < closestEnemy)
                {
                    closestEnemy = dis;
                }
            }

            if (friendly.Count > 0)
            {
                for (int f = 0; f < friendly.Count; f++)
                {
                    if (Vector3.Distance(friendly[f].transform.position, spawnPoints[i].position) < friendlyDistanceCap && closestEnemy > enemyDistanceCap)
                    {
                        return spawnPoints[i];
                    }
                }
            }

            if (!isVisible)
            {
                bestSpawns.Add(new Tuple<float, Transform>(closestEnemy, spawnPoints[i]));
            }
        }

        bestSpawn = bestSpawns[0];

        for (int i = 1; i < bestSpawns.Count; i++)
        {
            if (bestSpawns[i].Item1 > bestSpawn.Item1)
            {
                bestSpawn = bestSpawns[i];
            }
        }

        return bestSpawn.Item2;
    }

    private void UpdatePlayerLists()
    {
        enemy.Clear();
        friendly.Clear();

        foreach(PlayerStats ps in scoresManager.PlayerStats)
        {
            if (ps.CompareTag(Tags.ALLY_TAG))
            {
                friendly.Add(ps.GetComponent<Player>());
            } else
            {
                enemy.Add(ps.GetComponent<Player>());
            }
        }
    }
}
