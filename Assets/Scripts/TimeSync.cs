﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using System;

public class TimeSync : MonoBehaviourPunCallbacks, IPunObservable
{
    // How long has passed since the start
    private float timeToStart;
    public float TimeToStart { get => timeToStart == 0 ? 30 : timeToStart; set { timeToStart = value; } }

    private void Start()
    {
        timeToStart = 0;
    }

    /// <summary>
    /// Serializes and deserializes data to be sent over the network
    /// </summary>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(TimeToStart);
        }
        else if (stream.IsReading)
        {
            timeToStart = (float)stream.ReceiveNext();
        }
    }
}
