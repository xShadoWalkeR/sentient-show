﻿using System.Collections;
using UnityEngine;

public class SniperIKHandler : MonoBehaviour {

    [SerializeField] private bool enableIK = true;

    [SerializeField] private Transform lookAtObject = null;

    [SerializeField] private float weight = 1.0f;
    [SerializeField] private float headBodyWeight = 0.9f;

    private Animator anim;

    private void Awake() {

        anim = GetComponent<Animator>();
    }

    void OnAnimatorIK() {

        if (enableIK) {

            anim.SetLookAtWeight(weight, headBodyWeight, headBodyWeight, weight, weight);

            anim.SetLookAtPosition(lookAtObject.position);
        }
    }

    public IEnumerator BlendWeight(float duration, float interruption) {

        float currDuration = 0.0f;

        float startWeight = weight;
        float startHeadWeight = headBodyWeight;

        while (weight > 0 && headBodyWeight > 0) {

            currDuration += Time.deltaTime / duration;

            weight = Mathf.Lerp(startWeight, 0.0f, currDuration);
            headBodyWeight = Mathf.Lerp(startHeadWeight, 0.0f, currDuration);

            yield return null;
        }

        weight = headBodyWeight = 0.0f;

        yield return new WaitForSeconds(interruption);

        currDuration = 0.0f;

        while (weight < startWeight && headBodyWeight < startHeadWeight) {

            currDuration += Time.deltaTime / duration;

            weight = Mathf.Lerp(0.0f, startWeight, currDuration);
            headBodyWeight = Mathf.Lerp(0.0f, startHeadWeight, currDuration);

            yield return null;
        }

        weight = headBodyWeight = 1.0f;
    }
}
