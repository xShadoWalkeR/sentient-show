﻿using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Controlls the scores tab display
/// </summary>
public class ScoresTabManager : MonoBehaviourPunCallbacks
{
    /// <summary> Objects that parent a grid </summary>
    [Header("Scores Grid objects")]
    [SerializeField] private GameObject allyTeamScores;
    [SerializeField] private GameObject enemyTeamScores;

    /// <summary> Sprites for each state of the player </summary>
    [Header("Scores Row sprites")]
    [SerializeField] private Sprite blueAlive;
    [SerializeField] private Sprite blueDead;
    [SerializeField] private Sprite redAlive;
    [SerializeField] private Sprite redDead;

    /// <summary> Prefab for an empty stats row </summary>
    [Header("Score Row Prefab")]
    [SerializeField] private GameObject rowPrefab;

    /// <summary> A list with all the players stats </summary>
    public List<PlayerStats> PlayerStats { get; private set; }

    /// <summary> A list of stats for each player (Each SetRowInfo controls 1 row) </summary>
    private List<SetRowInfo> statsRows;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        PlayerStats = new List<PlayerStats>();

        statsRows = new List<SetRowInfo>();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        // If the number of items in the playerStats list is diferent from 
        // the number of players in the lobby
        if (PlayerStats.Count != PhotonRoom.room.playersInRoom)
        {

            // Destroys all the objects in the stats list
            for (int i = statsRows.Count - 1; i >= 0; i--)
            {
                Destroy(statsRows[i].gameObject);
            }

            // Clears both lists
            PlayerStats.Clear();
            statsRows.Clear();

            // Finds every object that is a player on the game get the PlayerStats component from
            // it and hads it to the list

            GameObject gb = GameObject.FindGameObjectWithTag(Tags.PLAYER_TAG);
            if (gb != null && gb.GetComponent<PlayerStats>() != null)
            {
                PlayerStats.Add(gb.GetComponent<PlayerStats>());
            }
            foreach(GameObject go in GameObject.FindGameObjectsWithTag(Tags.ENEMY_TAG))
            {
                PlayerStats.Add(go.GetComponent<PlayerStats>());
            }
            foreach (GameObject go in GameObject.FindGameObjectsWithTag(Tags.ALLY_TAG))
            {
                PlayerStats.Add(go.GetComponent<PlayerStats>());
            }

            // Goes through every PlayerStats on the list and sets the sprite acording to player's team
            for (int i = 0; i < PlayerStats.Count; i++)
            {
                statsRows.Add(Instantiate(rowPrefab, 
                    PlayerStats[i].CompareTag(Tags.PLAYER_TAG) || PlayerStats[i].CompareTag(Tags.ALLY_TAG) ?
                    allyTeamScores.transform : enemyTeamScores.transform).GetComponent<SetRowInfo>());

                statsRows[i].GetComponent<Image>().sprite = 
                    PlayerStats[i].CompareTag(Tags.PLAYER_TAG) || PlayerStats[i].CompareTag(Tags.ALLY_TAG) ?
                    blueAlive : redAlive;
            }
        } 
        else
        {
            // Updates all the stats
            UpdateStats();
        }
    }

    /// <summary>
    /// Updates all the stats
    /// </summary>
    private void UpdateStats()
    {
        // Goes through every PlayerStats
        for (int i = 0; i < PlayerStats.Count; i++)
        {
            // Updates each stats
            statsRows[i].UpdateMyStats(PlayerStats[i]);
            
            // If the player is dead 
            if (PlayerStats[i].IsDead)
            {
                // Set the appropriete sprite to represent it
                statsRows[i].GetComponent<Image>().sprite =
                    PlayerStats[i].CompareTag(Tags.PLAYER_TAG) || PlayerStats[i].CompareTag(Tags.ALLY_TAG) ?
                    blueDead : redDead;
            } else
            {
                // Else set the apropriete sprite to represent it
                statsRows[i].GetComponent<Image>().sprite =
                    PlayerStats[i].CompareTag(Tags.PLAYER_TAG) || PlayerStats[i].CompareTag(Tags.ALLY_TAG) ?
                    blueAlive : redAlive;
            }
        }
    }
}
