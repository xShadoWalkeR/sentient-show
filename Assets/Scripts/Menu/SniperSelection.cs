﻿using UnityEngine;
using System;

/// <summary>
/// Struct for the selected button of the sniper
/// </summary>
[Serializable]
public struct SniperSelection
{
    [SerializeField] private GameObject[] mySelectionButtons;
    public GameObject[] MySelectionButtons { get => mySelectionButtons; }

    [SerializeField] private GameObject[] mySelectedButtons;
    public GameObject[] MySelectedButtons { get => mySelectedButtons; }
}
