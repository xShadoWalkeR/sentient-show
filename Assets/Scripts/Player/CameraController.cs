﻿using Photon.Pun;
using System;
using UnityEngine;

/// <summary>
/// The player's main camera controller
/// </summary>
public class CameraController : MonoBehaviourPunCallbacks
{
    // All camera related settings
    [Header("Camera")]
    public GameObject cameraPivot;    // Player camera pivot
    [SerializeField] private float camRotationLimit = 85f;  // Limit for the camera rotation (Y Axis)
    [SerializeField] private float ySensitivity = 3f;   // Mouse Y Sensitivity
    [SerializeField] private float xSensitivity = 3f;   // Mouse X Sensitivity
    [SerializeField] private bool invertYAxis;  // If we want to invert the Y Axis

    // The mouse sensitivity
    public float XSense { get => xSensitivity; set => xSensitivity = value; }
    public float YSense { get => ySensitivity; set => ySensitivity = value; }

    // The base sensitivity
    public float BaseXSense { get; private set; } // Save the base value for the X sensitivity
    public float BaseYSense { get; private set; } // Save the base value for the Y sensitivity

    private float camRotation;  // Angle to rotate the camera on the Y
    private float curCameraRotation;    // Current camera rotation
    public float CurCameraRot { get => curCameraRotation; set => curCameraRotation = value; }

    private Vector3 rotation;   // Rotation from the Mouse X Input

    public float Recoil { get; set; }
    private float recoilRot;

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start() {
        xSensitivity = PlayerPrefs.GetFloat("MouseSense");
        ySensitivity = PlayerPrefs.GetFloat("MouseSense");

        BaseXSense = xSensitivity;
        BaseYSense = ySensitivity;

        invertYAxis = Convert.ToInt32(PlayerPrefs.GetInt("InvertYAxis")) == 1;
    }

    /// <summary>
    /// Called once every frame
    /// </summary>
    protected virtual void Update()
    {
        // If we're the local player
        if (photonView.IsMine)
            CameraRotation();   // Rotate the camera
    }

    /// <summary>
    /// Handles the camera rotation with the mouse movement
    /// </summary>
    protected void CameraRotation()
    {
        // Change FOV
        GetComponentInChildren<Camera>().fieldOfView = PlayerPrefs.GetInt("GameFOV");

        // Get the rotation ammout for the player
        rotation = new Vector3(0f, Input.GetAxisRaw("Mouse X"), 0f) * xSensitivity;

        // Get the rotation ammount for the camera
        camRotation = Input.GetAxisRaw("Mouse Y") * (invertYAxis ? -ySensitivity : ySensitivity);

        // Method to rotate both player and camera
        Rotate();
    }

    /// <summary>
    /// Rotates the player based on the mouse movement
    /// </summary>
    protected void Rotate()
    {
        if (rotation != Vector3.zero)
        {
            // If my rotation vector is not 0 rotate my rigid body with a MoveRotation
            transform.Rotate(transform.rotation * Quaternion.Euler(rotation).eulerAngles);
        }

        if (cameraPivot != null)
        {
            curCameraRotation -= camRotation;
            // Clamp the camera y rotation
            curCameraRotation = Mathf.Clamp(curCameraRotation, -camRotationLimit, camRotationLimit);

            Recoil *= 50 * Time.deltaTime;
            recoilRot += Recoil * Time.deltaTime;
            recoilRot *= 0.97f;

            // Apply rotation to the camera transform
            cameraPivot.transform.localEulerAngles = new Vector3(curCameraRotation + recoilRot, 0f, 0f);
        }
    }
}
