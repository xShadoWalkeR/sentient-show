﻿using System.Collections;
using System.IO;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Rendering;

public class ProximityScan : Ability {

    [SerializeField] public override float AbilityCooldown { get; } = 15f;         // The ultimate total cooldown
    [SerializeField] private Vector3 minScale = new Vector3(0.2f,0.2f, 0.2f);      // The sphere's minimum scale
    [SerializeField] private Vector3 maxScale = new Vector3(30.0f, 30.0f, 30.0f);  // The sphere's maximum scale
    [SerializeField] private float radiusSpeed = 3.0f;   // The speed on which the sphere will grow
    [SerializeField] private float duration = 4.0f;      // The duration of said growth

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // Support variable so we can only change its values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // The sphere (collider) to be created
    private GameObject sphere;

    // The sniper's Animator
    private Animator anim;

    private void Awake() {

        // Fetch the Animator
        anim = transform.GetChild(1).GetComponentInChildren<Animator>();
    }

    void Start() {

        // Spawn a sphere on the center of the player
        // This sphere's collider will be used to detect enemies with 'OnTriggerEnter'
        if (photonView.IsMine) sphere = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "EnemyDetector"), transform.position, Quaternion.identity);

        // Assing our current cooldown as the given cooldown
        currentCooldown = AbilityCooldown;
    }

    void Update() {

        if (!photonView.IsMine) return;

        // If our ability is on cooldown
        if (IsOnCooldown) {
            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0) {
                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;

                // Reset the 'string' holding player names to be able to detect enemies again
                sphere.GetComponent<EnemyDetector>().ResetEnemyName();
            }
        }
    }

    public override IEnumerator Cast() {
        // Start Cooldown when casting the ability
        isOnCooldown = true;

        // Instantly Scan surroundings
        yield return new WaitForSeconds(0);

        // Call the 'Scan' method through an RPC
        sphere.GetComponent<EnemyDetector>().Scan(transform.position, minScale, maxScale, duration, radiusSpeed);
    }
}
