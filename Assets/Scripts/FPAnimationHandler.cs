﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPAnimationHandler : MonoBehaviour
{
    [SerializeField] private GameObject bow;
    [SerializeField] private GameObject arrow;
    [SerializeField] private GameObject bowString;
    [SerializeField] private GameObject katana;
    [SerializeField] private GameObject holster;
    [SerializeField] private GameObject smokeBomb;
    [SerializeField] private GameObject kunai;

    [Header("Main Animator")]
    [SerializeField] private Animator mainAnim;

    [Header("Object Active Time")]
    [SerializeField] private float kunaiActiveTime;
    [SerializeField] private float smokeBombActiveTime;

    [Header("AudioSource")]
    [SerializeField] private AudioSource sayaSource;
    [SerializeField] private AudioClip[] slaps;
    [SerializeField] private AudioClip[] steps;

    private int doDash;
    private int pullBack;
    private int unDraw;
    private int isClimbing;
    private int throwKunai;
    private int throwSmoke;
    private int hasBow;
    private int hasKatana;
    private int switchToKatana;
    private int switchToBow;
    private int longPress;
    private int startAttack;
    private int fromKatana;
    private int fromBow;
    
    private Animator myAnim;
    

    private void Start()
    {
        doDash = Animator.StringToHash("doDash");
        pullBack = Animator.StringToHash("pullBack");
        unDraw = Animator.StringToHash("unDraw");
        isClimbing = Animator.StringToHash("isClimbing");
        throwKunai = Animator.StringToHash("throwKunai");
        throwSmoke = Animator.StringToHash("throwSmoke");
        hasBow = Animator.StringToHash("hasBow");
        hasKatana = Animator.StringToHash("hasKatana");
        switchToKatana = Animator.StringToHash("switchToKatana");
        switchToBow = Animator.StringToHash("switchToBow");
        longPress = Animator.StringToHash("longPress");
        startAttack = Animator.StringToHash("startAttack");
        fromKatana = Animator.StringToHash("fromKatana");
        fromBow = Animator.StringToHash("fromBow");

        myAnim = GetComponent<Animator>();

        // Please ignore this line i know it's bad
        mainAnim = Camera.main.transform.parent.parent.GetComponent<Player>().TpAnim;
    }

    private void Update()
    {
        myAnim.SetBool(pullBack, mainAnim.GetBool(pullBack));
    }

    protected void StopDash()
    {
        myAnim.SetBool(doDash, false);
    }

    protected void ThrowSmokeBomb()
    {
        smokeBomb.SetActive(true);
        myAnim.SetBool(throwSmoke, false);
        StartCoroutine(LoopAnimationHandle(AnimType.SmokeThrow));
    }

    protected void ThrowKunai()
    {
        kunai.SetActive(true);
        myAnim.SetBool(throwKunai, false);
        StartCoroutine(LoopAnimationHandle(AnimType.KunaiThrow));
    }

    protected void DisableBow()
    {
        bow.SetActive(false);
        arrow.SetActive(false);
        bowString.SetActive(false);

        myAnim.SetBool(hasBow, false);
    }

    protected void EnableBow()
    {
        bow.SetActive(true);
        arrow.SetActive(true);
        bowString.SetActive(true);

        myAnim.SetBool(fromKatana, false);
        myAnim.SetBool(fromBow, true);

        myAnim.SetBool(hasBow, true);
        myAnim.SetBool(switchToBow, false);
    }

    protected void DisableKatana()
    {
        if (myAnim.GetBool(fromBow))
        {
            katana.SetActive(false);
            holster.SetActive(false);

            myAnim.SetBool(hasKatana, false);
        }
    }

    protected void HideKatana()
    {
        katana.SetActive(false);
        holster.SetActive(false);

        myAnim.SetBool(hasKatana, false);
    }

    protected void EnableKatana()
    {
        holster.SetActive(true);
        katana.SetActive(true);

        if (!myAnim.GetBool(doDash))
        {
            myAnim.SetBool(fromKatana, true);
            myAnim.SetBool(fromBow, false);
        }

        myAnim.SetBool(hasKatana, true);
        myAnim.SetBool(switchToKatana, false);
    }

    protected void DisableArrow()
    {
        arrow.SetActive(false);
    }

    protected void EnableArrow()
    {
        arrow.SetActive(true);
    }

    private IEnumerator LoopAnimationHandle(AnimType animType)
    {
        float activeTimer = 0;

        switch(animType)
        {
            case AnimType.KunaiThrow:
                while(activeTimer < kunaiActiveTime)
                {
                    activeTimer += Time.deltaTime;
                    yield return null;
                }

                kunai.SetActive(false);
                break;
            case AnimType.SmokeThrow:
                while (activeTimer < smokeBombActiveTime)
                {
                    activeTimer += Time.deltaTime;
                    yield return null;
                }

                smokeBomb.SetActive(false);
                break;
            case AnimType.UnDraw:

                break;
        }


        yield return null;
    }

    // Audio Stuff
    protected void PlaySound(AudioClip clip)
    {
        sayaSource.PlayOneShot(clip);
    }

    protected void PlaySubSound(AudioClip clip)
    {
        sayaSource.PlayOneShot(clip, 0.2f);
    }

    protected void PlaySlapSound()
    {
        sayaSource.PlayOneShot(slaps[Random.Range(0, slaps.Length)], 0.3f);
        sayaSource.PlayOneShot(steps[Random.Range(0, steps.Length)], 0.05f);
    }
}

public enum AnimType
{
    WallClimb,
    SmokeThrow,
    KunaiThrow,
    UnDraw
}
