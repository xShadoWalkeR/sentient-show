﻿using Photon.Pun;
using UnityEngine;

/// <summary>
/// Sets the character and abilities selected by the player
/// </summary>
public class PlayerInfo : MonoBehaviourPunCallbacks
{
    public string MySelectedCharacter { get; set; }

    public string AttackAbility { get; set; }

    public string DefenseAbility { get; set; }

    public string MyUserName { get => PhotonNetwork.NickName; }

    private string previousCharacter;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        MySelectedCharacter = "Ninja";
        previousCharacter = MySelectedCharacter;

        AttackAbility = "NinjaDash";
        DefenseAbility = "NinjaSmokeBomb";
    }

    private void Update()
    {
        // Updates the player's selected champion
        PlayerPrefs.SetString("MyCharacter", MySelectedCharacter);

        // When the selected champion is changed updates the default abilities
        // to correspond to the selected champion
        if (previousCharacter != MySelectedCharacter)
        {
            previousCharacter = MySelectedCharacter;
            UpdateDefaultAbilities();
        }

        // Updates the player selected abilities
        PlayerPrefs.SetString("AttackAbility", AttackAbility);
        PlayerPrefs.SetString("DefenseAbility", DefenseAbility);
    }

    private void UpdateDefaultAbilities()
    {
        switch(MySelectedCharacter)
        {
            case "Ninja":
                AttackAbility = "NinjaDash";
                DefenseAbility = "NinjaSmokeBomb";
                break;
            case "Sniper":
                AttackAbility = "KineticTraps";
                DefenseAbility = "ProximityScan";
                break;
        }
    }
}
