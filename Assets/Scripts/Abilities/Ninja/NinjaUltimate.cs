﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Controller;

/// <summary>
/// The ninja ultimate
/// </summary>
public class NinjaUltimate : Ability
{
    [SerializeField] public override float AbilityCooldown { get; } = 120f;   // The ultimate total cooldown

    // Support variable so we can only chage it's values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    private float abilityDuration;
    public float AbilityDuration { get => abilityDuration; }

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    private bool isActive;

    // Override the event from our parent
    public override UnityEngine.Events.UnityEvent PlayerCast { get; set; }

    // The player's controller
    private MainController myController;

    private WaitForSeconds duration;

    private Animator myAnimator;

    private Player player;

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        player = GetComponent<Player>();

        myController = GetComponent<NinjaController>();

        PlayerCast = new UnityEngine.Events.UnityEvent();

        myAnimator = player.TpAnim;

        currentCooldown = AbilityCooldown;

        abilityDuration = 7f;

        isActive = false;

        duration = new WaitForSeconds(abilityDuration);
    }

    /// <summary>
    /// Called once per frame
    /// </summary>
    private void Update()
    {
        if (!photonView.IsMine) return;

        if (isActive)
        {
            if (Input.GetMouseButton(0))
            {
                PlayerCast.Invoke();

                myAnimator.SetBool(AnimationTags.LONG_PRESS, true);
                player.FpAnim.SetBool(AnimationTags.LONG_PRESS, true);
                myAnimator.SetInteger(AnimationTags.START_ATTACK, Random.Range(1, 3));
                myAnimator.SetInteger(AnimationTags.ATTACK_TYPE, Random.Range(0, 2));
                player.FpAnim.SetInteger(AnimationTags.START_ATTACK, myAnimator.GetInteger(AnimationTags.START_ATTACK));
            } else
            {
                myAnimator.SetBool(AnimationTags.LONG_PRESS, false);
                player.FpAnim.SetBool(AnimationTags.LONG_PRESS, false);
                myAnimator.SetInteger(AnimationTags.START_ATTACK, 0);
                player.FpAnim.SetInteger(AnimationTags.START_ATTACK, 0);
                myAnimator.SetInteger(AnimationTags.ATTACK_TYPE, 2);
            }
        }

        // If our ability is on cooldown
        if (IsOnCooldown)
        {
            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0)
            {
                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }
    }

    /// <summary>
    /// Cast's the ultimate
    /// </summary>
    /// <returns>The ultimate's duration</returns>
    public override IEnumerator Cast()
    {
        myAnimator.SetBool(AnimationTags.HAS_KATANA, true);
        myAnimator.SetBool(AnimationTags.SWITCH_TO_KATANA, true);

        player.FpAnim.SetBool(AnimationTags.HAS_KATANA, true);
        player.FpAnim.SetBool(AnimationTags.SWITCH_TO_KATANA, true);


        isActive = true;

        yield return duration;

        isOnCooldown = true;

        myAnimator.SetBool(AnimationTags.HAS_KATANA, false);
        myAnimator.SetBool(AnimationTags.SWITCH_TO_BOW, true);

        player.FpAnim.SetBool(AnimationTags.SWITCH_TO_BOW, true);

        isActive = false;
    }
}
