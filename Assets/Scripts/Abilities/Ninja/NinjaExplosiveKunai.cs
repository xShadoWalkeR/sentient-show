﻿using Enums;
using Photon.Pun;
using System.Collections;
using System.IO;
using UnityEngine;

/// <summary>
/// The ninja's explosive kunai ability
/// </summary>
public class NinjaExplosiveKunai : Ability
{
    // The kunai recharge time
    public override float AbilityCooldown { get; } = 4f;

    // Override the event from our parent
    public override UnityEngine.Events.UnityEvent PlayerCast { get; set; }

    private int maxKunais = 3;

    private float kunaiThrowDelay = 0.5f;
    private float currentDelay = 0f;
    private bool delayNext = false;

    private int remainingKunais = 3;

    private float expRadius = 1.5f;
    private float maxExpForce = 3;
    private float expForce = 0;
    private int expDamage = 30;

    // The projectile to be created
    private GameObject projectile;

    // Our camera
    private Transform cam;

    // Colliders detecter by the explosion
    private Collider[] eColliders;

    // Current player affected by explosion
    private Player expPlayer;

    // My Player
    private Player player;

    // Delay to match the animation
    private WaitForSeconds kunaiDelay;

    // Support variable so we can only chage it's values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    private void Start()
    {
        PlayerCast = new UnityEngine.Events.UnityEvent();

        player = GetComponent<Player>();

        kunaiDelay = new WaitForSeconds(0.6f);

        // Get our camera
        cam = Camera.main.transform;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    private void Update()
    {
        if (!photonView.IsMine)
            return;

        // Checks if we have Kunais and manages the cooldown timers
        ManageCooldowns();
    }

    /// <summary>
    /// Called when my projectile hits something
    /// </summary>
    public override void ProjectileHit(Vector3 position)
    {
        // Instanciate The explosion effect across the network
        Destroy(PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "KunaiExplosion"), 
            position, Quaternion.identity), 2.1f);

        // Get all nearby object
        eColliders = Physics.OverlapSphere(position, expRadius);

        // Go through all the collider that where hit
        foreach (Collider nearObj in eColliders)
        {
            // If the current collider is a player
            if (nearObj.tag == Tags.PLAYER_TAG || nearObj.tag == Tags.ENEMY_TAG)
            {
                // Calculate the direction of the knock back
                Vector3 direction = (nearObj.bounds.center - position).normalized;

                // Calculate the force of the explosion based on the distance
                expForce = maxExpForce / Vector3.Distance(position, nearObj.bounds.center);

                // Save a reference to the Player script of the hit player
                expPlayer = nearObj.GetComponent<Player>();

                // Send and RPC for the player to take damage
                expPlayer.photonView.RPC("RPC_TakeDamage", RpcTarget.All, expDamage, gameObject.name);

                // Send an RPC for the player to receive a status effect
                expPlayer.photonView.RPC("RPC_ApplyStatusEffect", RpcTarget.All, 
                    StatusEffectType.SLOW, 0.75f, 2f);

                // Send an RPC to Kockback the player
                nearObj.GetComponent<Controller.MainController>().photonView.RPC("AddForce", 
                    RpcTarget.All, direction, expForce);
            }
        }
    }

    /// <summary>
    /// Checks if we have Kunais and manages the cooldown timers
    /// </summary>
    private void ManageCooldowns()
    {
        // If the amount of kunais we have is less than our max value
        if (remainingKunais < maxKunais)
        {
            // We increasse the current recharge time by Time.deltaTime
            currentCooldown -= Time.deltaTime;

            // If the current recharge time is greater or equal to the time it takes to recharge
            if (currentCooldown <= 0)
            {
                // Increase the amout of Kunais we have
                remainingKunais++;

                // Set the current recharge time to 0
                currentCooldown = AbilityCooldown;
            }

            // If we have no Kunais left
            if (remainingKunais <= 0)
            {
                // We're now on cooldown
                isOnCooldown = true;
            }
            // If not
            else
            {
                // We're not yet on cooldown
                isOnCooldown = false;
            }
        }

        // If we're delaying our next throw
        if (delayNext)
        {
            // Increase the current delay by Time.deltaTime
            currentDelay += Time.deltaTime;

            // If the current delay is greater or equal to the desiered delay
            if (currentDelay >= kunaiThrowDelay)
            {
                // Reset the current delay
                currentDelay = 0;

                // Stop delaying the throw
                delayNext = false;
            }
        }
    }

    /// <summary>
    /// Casts the ability
    /// </summary>
    public override IEnumerator Cast()
    {
        // If for some reason we get to here and we don't have Kunais get out
        if (remainingKunais <= 0 || delayNext)
            yield break;

        player.FpAnim.SetBool(AnimationTags.THROW_KUNAI, true);
        player.TpAnim.SetBool(AnimationTags.THROW_KUNAI, true);

        yield return kunaiDelay;
        
        // Sends an event that the player has casted an ability
        PlayerCast.Invoke();

        // Decrease the amout of Kunais we have
        remainingKunais--;

        // Start the throw delay counter
        delayNext = true;

        // Instanciate a new Kunai to throw
        projectile = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Explosive_Kunai"), 
            cam.transform.position + cam.transform.forward + -(cam.transform.up * 0.3f), 
            cam.transform.rotation);

        // Set this script as being the caster of the projectile
        projectile.GetComponent<Projectile>().CasterOverride(this);

        // Tell the projectile who owns it
        projectile.GetComponent<Projectile>().Owner = GetComponent<Player>();

        // Call the Lose method to throw the Kunai
        projectile.GetComponent<Projectile>().Loose(cam.transform.forward);
    }
}
