﻿using Photon.Pun;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

/// <summary>
/// Responsible for instantiating the player into the game
/// </summary>
public class PhotonPlayer : MonoBehaviourPunCallbacks, IPunObservable
{
    private GameObject myAvatar;

    private GameObject readyDisplay;

    private UiManager myUi;

    [SerializeField]
    private GameObject playerUi;

    private Transform[] mySpawns;

    public int myTeam;

    public string userName;

    private bool avatarSet;

    private GameObject readyParent;

    private ReadyBoxUpdater readyBox;

    private List<ReadyBoxUpdater> enemyBoxDisabler;

    

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        enemyBoxDisabler = new List<ReadyBoxUpdater>(8);

        if (photonView.IsMine)
        {
            readyBox = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Character Select"), transform.position, Quaternion.identity).GetComponent<ReadyBoxUpdater>();

            GetTeam();
        }
    }
    
    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "NewArena" && !avatarSet)
        {
            avatarSet = true;
            Invoke("SetAvatar", 0.5f);
        } else if (SceneManager.GetActiveScene().name != "NewArena" && !avatarSet && photonView.IsMine)
        {
            UpdateReadyBoxes();
        }
    }

    private void UpdateReadyBoxes()
    {
        if (readyParent == null)
        {
            readyParent = readyBox.transform.parent.gameObject;
            return;
        }

        enemyBoxDisabler = readyParent.GetComponentsInChildren<ReadyBoxUpdater>(true).ToList();

        foreach(ReadyBoxUpdater rbu in enemyBoxDisabler)
        {
            if (rbu.MyTeam != myTeam)
            {
                rbu.gameObject.SetActive(false);
            } else
            {
                rbu.gameObject.SetActive(true);
            }
        }
    }

    public void SetAvatar()
    {
        // Set the spawns
        mySpawns = myTeam == 1 ?
            GameSetup.GS.spawnPointsTeamOne :
            GameSetup.GS.spawnPointsTeamTwo;

        // Pick a spawnPoint
        int spawnPicker = Random.Range(0, mySpawns.Length);

        // Spawn the player
        if (photonView.IsMine)
        {
            PhotonNetwork.SendRate = 30;
            PhotonNetwork.SerializationRate = 15;

            myAvatar = PhotonNetwork.Instantiate(
                Path.Combine("PhotonPrefabs", PlayerPrefs.GetString("MyCharacter")),
                mySpawns[spawnPicker].position,
                mySpawns[spawnPicker].rotation, 0);

            // Set the player camera to active
            myAvatar.GetComponent<CameraController>().cameraPivot.GetComponentInChildren<Camera>(true).gameObject.SetActive(true);
            myAvatar.GetComponent<CameraController>().cameraPivot.transform.localPosition = new Vector3(0, 1.677571f, 0.1f);

            // Set the player abilities
            myAvatar.GetComponent<Player>().photonView.RPC("RPC_SetAbilities", RpcTarget.All,
                 new string[] { 
                     PlayerPrefs.GetString("AttackAbility"),
                     PlayerPrefs.GetString("DefenseAbility")
                 });

            // Set the player name
            myAvatar.GetComponent<Player>().photonView.RPC("RPC_SetMyName", RpcTarget.All,
                PhotonNetwork.NickName);

            myUi = Instantiate(playerUi).GetComponent<UiManager>();
            myUi.myPlayerObject = myAvatar;

            myUi.GetComponent<PauseMenu>().photonPlayer = this.gameObject;

            myAvatar.GetComponent<Player>().UI = myUi;

            // Get the weapon the player has
            Transform myWeapon = myAvatar.GetComponent<CameraController>().cameraPivot.transform.GetChild(0);
            // Set it's layer to Weapon
            myWeapon.gameObject.layer = LayerMask.NameToLayer("Weapon");
            // Set all it's childs layers to Weapon
            for (int i = 0; i < myWeapon.transform.childCount; i++)
            {
                myWeapon.transform.GetChild(i).gameObject.layer = LayerMask.NameToLayer("Weapon");
            }

            // Set the player team
            myAvatar.GetComponent<Player>().photonView.RPC("RPC_SetMyTeam", RpcTarget.All,
                myTeam);

            myAvatar.layer = LayerMask.NameToLayer("LocalPlayer");
        }
    }

    /// <summary>
    /// Updates the myTeam variable across the network
    /// </summary>
    private void GetTeam()
    {
        // myTeam = GameSetup.GS.NextPlayerTeam;
        userName = PhotonNetwork.NickName;
        PlayerPrefs.SetInt("MyTeam", myTeam);
        readyBox.MyTeam = myTeam;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(myTeam);
            stream.SendNext(userName);
        }
        else if (stream.IsReading)
        {
            myTeam = (int)stream.ReceiveNext();
            userName = (string)stream.ReceiveNext();
        }
    }
}
