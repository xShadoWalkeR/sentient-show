﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

/// <summary>
/// Manages the player's weapons
/// </summary>
public class WeaponManager : MonoBehaviourPunCallbacks
{
    // Array with all the player's available weapons
    [SerializeField] private WeaponHandler[] weapons;
    public WeaponHandler[] Weapons { get => weapons; }

    // Array for each weapon specific key
    [SerializeField] private KeyCode[] weaponKeys;

    // Selects the first weapon we get to equip by index
    [SerializeField] private int starterWeapon;

    // Checks if we don't want to turn weapons off
    [SerializeField] private bool justChangeIndex;

    // It's used so we can know what weapon we currently have
    private int current_Weapon_Index;

    // The previous weapon index
    public int PreviousWeaponIndex { get; private set; }

    /// <summary>
    /// Use this for initialization
    /// </summary>
    void Start()
    {
        // Assign the first weapon index
        current_Weapon_Index = starterWeapon;
        // Set's the main weapon to be active
        weapons[current_Weapon_Index].gameObject.SetActive(true);
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (!photonView.IsMine) return;

        // If any key was pressed
        if(Input.anyKeyDown)
        {
            // Go trough the Weapon Keys array
            for(int i = 0; i < weaponKeys.Length; i++)
            {
                // Check if any of the keys was pressed
                if (weaponKeys[i] != KeyCode.None && Input.GetKeyDown(weaponKeys[i]))
                {
                    // If so turns on the selected weapon
                    photonView.RPC("RPC_TurnOnSelectedWeapon", RpcTarget.All, i);
                }
            }
        }
    }

    /// <summary>
    /// Returns the currently selected weapon.
    /// </summary>
    /// <returns>Currently selected weapon</returns>
    public WeaponHandler GetCurrentSelectedWeapon() => weapons[current_Weapon_Index];

    /// <summary>
    /// Sets the new current Weapon
    /// </summary>
    /// <param name="index">The current weapon's index</param>
    public void SetCurrentWeapon(int index) { current_Weapon_Index = index; }

    /// <summary>
    /// Turns on the weapon we select.
    /// </summary>
    /// <param name="weaponIndex">The index for the selected weapon</param>
    [PunRPC]
    public void RPC_TurnOnSelectedWeapon(int weaponIndex)
    {
        // If the current weapon is the weapon we already have
        if (current_Weapon_Index == weaponIndex)
            return; // Return...

        // If we don't want to simply change the index
        if (!justChangeIndex) {

            if (!photonView.IsMine)
            {
                // Turn off the current weapon
                weapons[current_Weapon_Index].gameObject.SetActive(false);

                // Turn on the selected weapon
                weapons[weaponIndex].gameObject.SetActive(true);
            }
        }

        PreviousWeaponIndex = current_Weapon_Index;

        // Store the current selected weapon index
        current_Weapon_Index = weaponIndex;
    }
}
