﻿using UnityEngine;
using TMPro;
using Photon.Pun;
using System;

/// <summary>
/// Controls and displays the in game time
/// </summary>
public class GameTimer : MonoBehaviourPunCallbacks, IPunObservable
{
    // How many seconds in a minute
    private const int MINUTE_SECONDS = 60;

    // Seconds offset
    public readonly int OFFSET_SECONDS = 59;

    // The object that displays the timer
    [Header("Timer Text UI")]
    [SerializeField] private TextMeshProUGUI timerText;

    // The time limit for FFA and TDM Game modes
    [Header("Round Duration")]
    [SerializeField] private int ffaTimeLimit;
    [SerializeField] private int tdmTimeLimit;

    // The game mode
    [Header("Game Mode")]
    [SerializeField] private GameMode gameMode;

    // How long has passed since the start
    public double Timer { get; private set; }

    // Holds the time limit
    public int TimeLimit { get; private set; }

    // Other timers
    private double startTime;
    private int timerSeconds;
    private int timerMinutes;

    /// <summary>
    /// Called before the start method
    /// </summary>
    private void Awake()
    {
        // Set the starting time
        startTime = PhotonNetwork.Time - OFFSET_SECONDS;

        // Set the time limit
        TimeLimit = gameMode == GameMode.FFA ? ffaTimeLimit : tdmTimeLimit;
    }

    /// <summary>
    /// Called once every frame
    /// </summary>
    private void Update()
    {
        // If the timer as not yet reached the time limit
        if (!(Timer >= TimeLimit + OFFSET_SECONDS))
        {
            // Set the timer to the current server time minus the starting time
            Timer = PhotonNetwork.Time - startTime;

            // Convert the total time to minutes
            timerMinutes = (TimeLimit / MINUTE_SECONDS) - (int)(Timer / MINUTE_SECONDS);

            // Convert the total time to seconds of the current minute
            timerSeconds = OFFSET_SECONDS - ((int)Timer - (((TimeLimit / MINUTE_SECONDS) - (timerMinutes)) * MINUTE_SECONDS));

            // Update the displayed Timer
            timerText.text = String.Format("{0:00} : {1:00}", timerMinutes, timerSeconds);
        }
    }

    /// <summary>
    /// Serializes and deserializes data to be sent over the network
    /// </summary>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(startTime);
        }
        else if (stream.IsReading)
        {
            startTime = (double)stream.ReceiveNext();
        }
    }

    // Set's the game mode
    public void SetGameMode(GameMode mode)
    {
        gameMode = mode;
    }
}
