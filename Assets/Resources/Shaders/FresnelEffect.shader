﻿Shader "Unlit/FresnelEffect"
{
    Properties
    {
		_MainColor ("MainColor", Color) = (0,0,0,0)
		_SecondaryColor("SecondColor", Color) = (0,0,0,0)
		_MinDistance("MinDistance", float) = 1
		_MaxDistance("MaxDistance", float) = 10

		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_FresnelColor ("Fresnel Color", Color) = (1,1,1,1)
		_FresnelBias ("Fresnel Bias", Float) = 0
		_FresnelScale ("Fresnel Scale", Float) = 1
		_FresnelPower ("Fresnel Power", Float) = 1
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        LOD 100

		Pass
		{
			Blend One OneMinusSrcAlpha
			Cull Back
			ZWrite Off

			ZTest Greater // Always

			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 2.0

            #include "UnityCG.cginc"

			struct appdata
            {
                float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
				float4 worldPos : TEXCOORD2;
				
				half2 uv : TEXCOORD0;
				float fresnel : TEXCOORD1;
            };

			float4 _MainColor;
			float4 _SecondaryColor;
			float _MinDistance;
			float _MaxDistance;
			float4 _DistanceColor;

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			fixed4 _FresnelColor;
			fixed _FresnelBias;
			fixed _FresnelScale;
			fixed _FresnelPower;

			v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

				// ########## Fresnel ##########
				
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				float3 i = normalize(ObjSpaceViewDir(v.vertex));
				o.fresnel = _FresnelBias + _FresnelScale * pow(1 + dot(i, v.normal), _FresnelPower);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float dist = distance(_WorldSpaceCameraPos, i.worldPos);

				if (dist <= _MinDistance)
				{
					_DistanceColor = _MainColor;
				}

				if (dist > _MinDistance && dist < _MaxDistance) 
				{
					half weight = (dist - _MinDistance) / (_MaxDistance - _MinDistance);
					_DistanceColor = lerp(_MainColor, _SecondaryColor, weight);
				}

				if (dist >= _MaxDistance) 
				{
					_DistanceColor = _SecondaryColor;
				}

				// ########## Fresnel ##########
				fixed4 c = tex2D(_MainTex, i.uv) * _Color;
                return lerp(c, _FresnelColor, 1 - i.fresnel) + _DistanceColor;
            }

			ENDCG
		}
    }
}
