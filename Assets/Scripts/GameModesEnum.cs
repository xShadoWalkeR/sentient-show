﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enumerator for the existing game modes
/// </summary>
public enum GameMode
{
    TDM,
    FFA
}
