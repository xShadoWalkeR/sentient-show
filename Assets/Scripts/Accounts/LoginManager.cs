﻿using System;
using System.IO;
using System.Collections;
using UnityEngine;
using TMPro;
using Random = UnityEngine.Random;

public class LoginManager : MonoBehaviour {

    [Header("Directory/File Settings")]
    [SerializeField] private string fileName = "LoginCredentials";
    [SerializeField] private string folderName = "Champions of Tomorrow";

    [Header("Min/Max Character Settings")]
    [SerializeField] private int minUserChars = 4;
    [SerializeField] private int minPassChars = 6;
    [Space]
    [SerializeField] private int maxUserChars = 16;
    [SerializeField] private int maxPassChars = 16;

    [Header("Screens")]
    [SerializeField] private GameObject connectingScreen = null;
    [SerializeField] private GameObject loginScreen = null;
    [Space]
    [SerializeField] private GameObject createAccount = null;
    [SerializeField] private GameObject login = null;

    [Header("Create Account Parameters")]
    [SerializeField] private TMP_InputField createdUsername = null;
    [SerializeField] private TMP_InputField createdPassword = null;
    [SerializeField] private TMP_InputField confirmPassword = null;

    [Header("Login Parameters")]
    [SerializeField] private TMP_InputField loginUsername = null;
    [SerializeField] private TMP_InputField loginPassword = null;

    [Header("Messages/Warnings")]
    [SerializeField] private GameObject connectBackButton = null;
    [Space]
    [SerializeField] private RectTransform connectIcon = null;
    [SerializeField] private float rotationSpeed = 180.0f;
    [Space]
    [SerializeField] private float minDelay = 1.0f;
    [SerializeField] private float maxDelay = 3.0f;
    [Space]
    [SerializeField] private TMP_Text messages = null;
    [Space]
    [SerializeField] private TMP_Text localWarnings = null;
    [SerializeField] private TMP_Text connectWarnings = null;

    private readonly string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
    
    // Value changed from the toggle
    public bool SaveUsername { get; set; } = true;

    private AccountManager am;

    private void Start() {

        am = AccountManager.am;

        if (am.UserName == null) {

            CreateDirectory();

            CreateAccountsFile();

            SetMaxCharacters();

            CreateAdminAccount();

            StartCoroutine(AutoLogin());

        } else {

            gameObject.SetActive(false);
        }
    }

    private void Update() {

        RotateIcon();
    }

    private void RotateIcon() {

        if (connectIcon.gameObject.activeSelf) {

            connectIcon.Rotate(Vector3.forward, -(rotationSpeed * Time.deltaTime));
        }
    }

    private void SetMaxCharacters() {

        createdUsername.characterLimit = maxUserChars;
        createdPassword.characterLimit = maxPassChars;
        confirmPassword.characterLimit = maxPassChars;

        loginUsername.characterLimit = maxUserChars;
        loginPassword.characterLimit = maxPassChars;
    }

    private void CreateDirectory() {

        am.PathString = Path.Combine(documents, folderName);

        if (!Directory.Exists(am.PathString)) {

            Directory.CreateDirectory(am.PathString);

            print("Directory has been created!");

        } else {

            //Directory.Delete(filePath);

            print("Directory already exists!");
        }
    }

    private void CreateAccountsFile() {

        am.PathString = Path.Combine(am.PathString, $"{fileName}.txt");

        if (!File.Exists(am.PathString)) {

            File.Create(am.PathString).Close();

            WriteFirstLine();

            print("File has been created!");

        } else {

            //File.Delete(pathString);

            print("File already exists!");
        }
    }

    private void WriteFirstLine() {

        using (StreamWriter sw = new StreamWriter(am.PathString)) {

            print("Writing the First Line...");

            sw.Write(am.Formatting);

            sw.Close();
        }
    }

    private void CreateAdminAccount() {

        // Check if admin exists
        using (TextReader reader = File.OpenText(am.PathString)) {

            // Skips the first line
            string firstLine = reader.ReadLine();

            // Encrypt outside of while to prevent repetition
            string hexAdmin = am.Encrypt("admin");

            while (reader.Peek() > -1) {

                string[] values = reader.ReadLine().Split(',');

                if (values[0] == hexAdmin) {

                    reader.Close();

                    print("Admin Account has been found!");

                    return;
                }
            }

            reader.Close();
        }

        // Write admin account if it does not exist
        using (StreamWriter sw = new StreamWriter(am.PathString, append: true)) {

            print("Creating Admin Account...");

            sw.Write($"\n{am.Encrypt("admin")},{am.Encrypt("admin1")},40,0,4,false,false,false,false,0,0,0,0");

            sw.Close();
        }
    }

    public void CreateUser() {

        if (VerifyCredentials(false)) {

            StartCoroutine(CheckUsername(false));
        }
    }

    public void LoginUser() {

        if (VerifyCredentials(true)) {

            StartCoroutine(CheckUsername(true));
        }
    }

    private IEnumerator AutoLogin() {

        if (PlayerPrefs.HasKey("NickName") && PlayerPrefs.GetString("NickName") != "") {

            am.UpdateUserSettings(PlayerPrefs.GetString("NickName"));

            ShowMessage(Message.LogIn);

            yield return new WaitForSecondsRealtime(maxDelay);

            ShowMessage(Message.Welcome);
        }
    }

    public void LogOut() {

        print($"{am.UserName} logged out!");

        am.UserName = null;
        am.UserSettings.Clear();

        PlayerPrefs.SetString("NickName", am.UserName);
    }

    // Verifies text present on the input fields
    private bool VerifyCredentials(bool login) {

        string userName;

        if (login) {

            userName = loginUsername.text;

            if (userName == "") {

                ShowWarning(Warning.UserNameUnexistent);
                return false;

            } else if (loginPassword.text == "") {

                ShowWarning(Warning.PassWordUnexistent);
                return false;
            }

        } else {

            userName = createdUsername.text;

            if (userName == "") {

                ShowWarning(Warning.UserNameUnexistent);
                return false;

            } else if (userName.Length < minUserChars) {

                ShowWarning(Warning.UserNameTooSmall);
                return false;

            } else if (createdPassword.text == "") {

                ShowWarning(Warning.PassWordUnexistent);
                return false;

            } else if (createdPassword.text.Length < minPassChars) {

                ShowWarning(Warning.PassWordTooSmall);
                return false;

            } else if (confirmPassword.text == "") {

                ShowWarning(Warning.ConfPassWordUnexistent);
                return false;

            } else if (confirmPassword.text != createdPassword.text) {

                ShowWarning(Warning.ConfPassWordNotEqual);
                return false;
            }
        }

        return true;
    }

    private IEnumerator CheckUsername(bool login) {

        if (login) {

            ShowMessage(Message.SearchUser);

        } else {

            ShowMessage(Message.VerifyAccount);
        }

        float rndDelay = Random.Range(minDelay, maxDelay);

        yield return new WaitForSecondsRealtime(rndDelay);

        using (TextReader reader = File.OpenText(am.PathString)) {

            // Skips the first line
            string firstLine = reader.ReadLine();

            string hexUser;

            // Encrypt outside of while to prevent repetition
            if (login) {

                hexUser = am.Encrypt(loginUsername.text);

            } else {

                hexUser = am.Encrypt(createdUsername.text);
            }

            while (reader.Peek() > -1) {

                string[] values = reader.ReadLine().Split(',');

                if (values[0] == hexUser) {

                    reader.Close();

                    if (login) {

                        StartCoroutine(VerifyPassword(values[1]));

                    } else {

                        ShowWarning(Warning.UserAlreadyExists, true);
                    }

                    yield break;
                }
            }

            reader.Close();
        }

        if (login) {

            ShowWarning(Warning.WrongCredentials, true);

        } else {

            StartCoroutine(WriteUser());
        }
    }

    private IEnumerator WriteUser() {

        ShowMessage(Message.CreateAccount);

        float rndDelay = Random.Range(minDelay, maxDelay);

        yield return new WaitForSecondsRealtime(rndDelay);

        using (StreamWriter sw = new StreamWriter(am.PathString, append: true)) {

            sw.Write($"\n{am.Encrypt(createdUsername.text)},{am.Encrypt(createdPassword.text)},0,0,0,false,false,false,false,0,0,0,0");

            sw.Close();
        }

        am.UpdateUserSettings(createdUsername.text);

        SaveUserInPrefs();

        ShowMessage(Message.Welcome);
    }

    private IEnumerator VerifyPassword(string correctPassword) {

        ShowMessage(Message.VerifyPassWord);

        float rndDelay = Random.Range(minDelay, maxDelay);

        yield return new WaitForSecondsRealtime(rndDelay);

        if (correctPassword == am.Encrypt(loginPassword.text)) {

            am.UpdateUserSettings(loginUsername.text);

            SaveUserInPrefs();

            ShowMessage(Message.Welcome);

        } else {

            ShowWarning(Warning.WrongCredentials, true);
        }
    }

    public void ClearCreatedUser() {

        createdUsername.SetTextWithoutNotify("");
        createdPassword.SetTextWithoutNotify("");
        confirmPassword.SetTextWithoutNotify("");
    }

    public void ClearLoginUser() {

        loginUsername.SetTextWithoutNotify("");
        loginPassword.SetTextWithoutNotify("");
    }

    private void SaveUserInPrefs() {

        if (SaveUsername) {

            PlayerPrefs.SetString("NickName", am.UserName);
        }
    }

    private void ShowWarning(Warning warning, bool connect = false) {

        switch (warning) {

            case Warning.WrongCredentials:
                connectIcon.gameObject.SetActive(false);
                connectWarnings.SetText("Invalid Username/Password!");
                break;

            case Warning.UserAlreadyExists:
                connectIcon.gameObject.SetActive(false);
                connectWarnings.SetText("Username already exists!");
                break;

            case Warning.UserNameTooSmall:
                localWarnings.SetText($"Username needs a minimum of {minUserChars} characters!");
                break;

            case Warning.PassWordTooSmall:
                localWarnings.SetText($"Password needs a minimum of {minPassChars} characters!");
                break;

            case Warning.UserNameUnexistent:
                localWarnings.SetText($"You forgot to write a Username!");
                break;

            case Warning.PassWordUnexistent:
                localWarnings.SetText($"You forgot to write a Password!");
                break;

            case Warning.ConfPassWordUnexistent:
                localWarnings.SetText($"You forgot to confirm your Password!");
                break;

            case Warning.ConfPassWordNotEqual:
                localWarnings.SetText($"Passwords don't match!");
                break;
        }

        messages.gameObject.SetActive(false);

        if (connect) {

            connectWarnings.gameObject.SetActive(true);
            connectBackButton.SetActive(true);

        } else {

            localWarnings.gameObject.SetActive(true);
        }
    }

    private void ShowMessage(Message message) {

        messages.gameObject.SetActive(true);

        SwitchToConnectScreen();

        switch (message) {

            case Message.CreateAccount:
                messages.SetText("Creating Account...");
                break;

            case Message.LogIn:
                messages.SetText("Logging in...");
                break;

            case Message.SearchUser:
                messages.SetText("Searching for User...");
                break;

            case Message.VerifyAccount:
                messages.SetText("Verifying Account...");
                break;

            case Message.VerifyPassWord:
                messages.SetText("Verifying password...");
                break;

            case Message.Welcome:
                connectIcon.gameObject.SetActive(false);
                messages.SetText($"{am.UserName}");

                gameObject.SetActive(false);
                connectingScreen.SetActive(false);

                break;
        }
    }

    private void SwitchToConnectScreen() {

        loginScreen.SetActive(false);
        createAccount.SetActive(false);
        login.SetActive(false);

        connectIcon.gameObject.SetActive(true);
        connectingScreen.SetActive(true);
    }

    private enum Warning {

        WrongCredentials,
        UserAlreadyExists,
        UserNameTooSmall,
        PassWordTooSmall,
        UserNameUnexistent,
        PassWordUnexistent,
        ConfPassWordUnexistent,
        ConfPassWordNotEqual
    }

    private enum Message {

        CreateAccount,
        LogIn,
        SearchUser,
        VerifyAccount,
        VerifyPassWord,
        Welcome
    }
}
