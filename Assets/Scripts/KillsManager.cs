﻿using UnityEngine;
using Photon.Pun;
using TMPro;

/// <summary>
/// Manages and syncronizes both team's kills
/// </summary>
public class KillsManager : MonoBehaviourPunCallbacks, IPunObservable
{
    // Singleton
    public static KillsManager km;

    // The text objects for the ally and enemy kills
    [SerializeField] private TextMeshProUGUI allyKillsText;
    [SerializeField] private TextMeshProUGUI enemyKillsText;

    // The number of kills from team one
    public int TeamOneKills { get; private set; }

    // The number of kills from team two
    public int TeamTwoKills { get; private set; }

    // The local player's team
    private int mainTeam;
    
    /// <summary>
    /// Called before the start method
    /// </summary>
    private void Awake()
    {
        // Set up our singleton
        if (KillsManager.km == null)
        {
            KillsManager.km = this;
        }
        else
        {
            if (KillsManager.km != this)
            {
                Destroy(KillsManager.km.gameObject);
                KillsManager.km = this;
            }
        }
    }

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        // Set the mainTeam to 0
        mainTeam = 0;

        // If it's running on the local player
        if (photonView.IsMine)
        {
            // Reset both team kills to 0
            TeamOneKills = 0;
            TeamTwoKills = 0;
        }
    }

    /// <summary>
    /// Runs once each frame
    /// </summary>
    private void Update()
    {
        // Updates the kill counters
        UpdateCounters();
    }

    /// <summary>
    /// Update the kill counters
    /// </summary>
    private void UpdateCounters()
    {
        // If the mainTeam is not 0
        if (mainTeam != 0)
        {
            // Update both kill counters
            allyKillsText.text = string.Format("{0:00}", mainTeam == 1 ? TeamOneKills : TeamTwoKills);
            enemyKillsText.text = string.Format("{0:00}", mainTeam == 1 ? TeamTwoKills : TeamOneKills);
        }
    }

    /// <summary>
    /// RPC to increasse the kills all across the network
    /// </summary>
    /// <param name="team">The team that got a kill</param>
    [PunRPC]
    public void RPC_SetKill(int team)
    {
        // Increase the team kills depending on what team was given
        switch (team)
        {
            case 1:
                TeamTwoKills++;
                break;
            case 2:
                TeamOneKills++;
                break;
        }
    }

    /// <summary>
    /// Set's the main team (The team of the local player)
    /// </summary>
    /// <param name="team">Int representing the main player's team</param>
    public void SetMainTeam(int team)
    {
        mainTeam = team;
    }

    /// <summary>
    /// Serializes and deserializes data to be sent over the network
    /// </summary>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(TeamOneKills);
            stream.SendNext(TeamTwoKills);
        }
        else if (stream.IsReading)
        {
            TeamOneKills = (int)stream.ReceiveNext();
            TeamTwoKills = (int)stream.ReceiveNext();
        }
    }
}
