﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BowHandler : MonoBehaviourPunCallbacks
{
    // The katana game object
    [SerializeField] private GameObject bow;
    public GameObject Bow { get => bow; }

    // The bow's projectile 
    [SerializeField] private GameObject arrow;
    public GameObject Arrow { get => arrow; }

    // The player's animator
    [SerializeField] private Animator bowAnim;

    // The player's animator
    private Animator myAnim;

    private int pullBack;
    private int aim;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize the animator
        myAnim = GetComponent<Animator>();

        pullBack = Animator.StringToHash("pullBack");
        aim = Animator.StringToHash("Aim");
    }

    private void Update()
    {
        myAnim.SetBool(pullBack, bowAnim.GetBool(aim));
    }

    /// <summary>
    /// Enebles the Bow
    /// </summary>
    public void EnableBow()
    {
        if (photonView.IsMine)
            photonView.RPC("RPC_EnableBow", RpcTarget.Others);
    }

    /// <summary>
    /// RPC to enable the Bow
    /// </summary>
    [PunRPC]
    private void RPC_EnableBow()
    {
        bow.SetActive(true);
    }

    /// <summary>
    /// Disables the Bow
    /// </summary>
    public void DisableBow()
    {
        if (photonView.IsMine)
            photonView.RPC("RPC_DisableBow", RpcTarget.Others);
    }

    /// <summary>
    /// RPC to disable the Bow
    /// </summary>
    [PunRPC]
    private void RPC_DisableBow()
    {
        bow.SetActive(false);
    }

    /// <summary>
    /// Disables the Arrow
    /// </summary>
    public void DisableArrow()
    {
        if (photonView.IsMine)
            photonView.RPC("RPC_DisableArrow", RpcTarget.Others);
    }

    /// <summary>
    /// RPC to disable the Arrow
    /// </summary>
    [PunRPC]
    private void RPC_DisableArrow()
    {
        arrow.SetActive(false);
    }

    /// <summary>
    /// Enebles the Arrow
    /// </summary>
    public void EnableArrow()
    {
        if (photonView.IsMine && myAnim.GetBool(AnimationTags.HAS_BOW))
            photonView.RPC("RPC_EnableArrow", RpcTarget.Others);
    }

    /// <summary>
    /// RPC to enable the Arrow
    /// </summary>
    [PunRPC]
    private void RPC_EnableArrow()
    {
        arrow.SetActive(true);
    }
}
