﻿using Enums;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPacks : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] private float healthpack;

    [SerializeField] private float respawnTimer;
    private WaitForSeconds timer;

    private MeshRenderer myRender;
    private SphereCollider myColy;

    private bool enableRenderer;
    private bool enableCollider;

    private Vector3 startpos;
    private bool movingUp;

    private void Start()
    {
        // Get info for sphere collider and mesh render
        myRender = GetComponent<MeshRenderer>();
        myColy = GetComponent<SphereCollider>();
        timer = new WaitForSeconds(respawnTimer);
        enableRenderer = true;
        enableCollider = true;

        startpos = transform.localPosition;
    }

    private void Update()
    {
        if (healthpack == 50)
        {
            transform.Rotate(0, 0, -(Time.deltaTime * 35));
        } else
        {
            transform.Rotate(0, Time.deltaTime * 35, 0);
        }

        if (Vector3.Distance(transform.localPosition, startpos) < 0.005f)
        {
            movingUp = true;
        } else if (Vector3.Distance(transform.localPosition, startpos) > (Vector3.up / 8).y)
        {
            movingUp = false;
        }

        if (movingUp)
        {
            transform.localPosition = Vector3.Lerp(transform.position, transform.localPosition + (Vector3.up / 8), 0.001f);
        } else
        {
            transform.localPosition = Vector3.Lerp(transform.position, transform.localPosition - (Vector3.up / 8), 0.001f);
        }
        


        if (enableRenderer != myRender.enabled)
        {
            myRender.enabled = enableRenderer;
        }
        if (enableCollider != myColy.enabled)
        {
            myColy.enabled = enableCollider;
        }
    }

    /// <summary>
    /// Heal the player that collides with us
    /// </summary>
    /// <param name="other">Collider of the player</param>
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.PLAYER_TAG))
        {
            Player player = other.GetComponent<Player>();

            if (player.CurrentHealth < player.MaxHealth) {

                photonView.TransferOwnership(player.photonView.Owner);

                player.photonView.RPC("RPC_ApplyStatusEffect",
                    RpcTarget.All, StatusEffectType.HEAL, healthpack, null);
                enableRenderer = false;
                enableCollider = false;
                StartCoroutine(RespawnHealthPack());
            }
        }
    }

    /// <summary>
    /// Wait a while to respawn the heath pack
    /// </summary>
    /// <returns>Wait for seconds</returns>
    private IEnumerator RespawnHealthPack()
    {
        yield return timer;
        enableRenderer = true;
        enableCollider = true;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(enableRenderer);
            stream.SendNext(enableCollider);
        }
        else if (stream.IsReading)
        {
            enableRenderer = (bool)stream.ReceiveNext();
            enableCollider = (bool)stream.ReceiveNext();
        }
    }
}