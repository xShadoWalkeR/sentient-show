﻿using UnityEngine;

/// <summary>
/// Set's the tags for all ingame players acording to their teams
/// </summary>
public class TeamsManager : MonoBehaviour
{
    // Array of players
    private GameObject[] toBeSet;

    // The instance of our player
    private Player playerInstance;

    // The team of our player
    private int? thisPlayerTeam;

    /// <summary>
    /// Runs at the start
    /// </summary>
    private void Start()
    {
        // Set this player team as null
        thisPlayerTeam = null;
    }

    private void LateUpdate()
    {
        // Runs once to get the local player team
        if (playerInstance == null)
        {
            // If the playerInstance is null tries to get the player component
            playerInstance = GameObject.FindGameObjectWithTag(Tags.PLAYER_TAG)?.GetComponent<Player>();
        }
        else if (thisPlayerTeam == null || (thisPlayerTeam != 1 && thisPlayerTeam != 2))
        {
            // If thisPlayerTeam is null or is different than 1 or 2 set it to the value found in the player
            thisPlayerTeam = playerInstance.MyTeam;
        } else
        {
            // Very bad performance change later
            toBeSet = GameObject.FindGameObjectsWithTag(Tags.TO_BE_SET);

            // If to be set is not null
            if (toBeSet != null)
            {
                // Goes through every game object
                for (int i = toBeSet.Length - 1; i >= 0; i--)
                {
                    // If the current object is not null
                    if (toBeSet[i] != null)
                    {
                        // If the current player's team is the same as the player
                        if (toBeSet[i].GetComponent<Player>().MyTeam == thisPlayerTeam)
                        {
                            // Set the tag to ally
                            toBeSet[i].tag = Tags.ALLY_TAG;
                        }
                        else
                        {
                            // Else set the tag to enemy
                            toBeSet[i].tag = Tags.ENEMY_TAG;
                        }

                        // Empty the array space
                        toBeSet[i] = null;
                    }
                }
            }
        }
    }
}
