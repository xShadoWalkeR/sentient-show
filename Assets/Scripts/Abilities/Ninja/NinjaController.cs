﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controller {
    /// <summary>
    /// The ninja controller overriding the main controller
    /// </summary>
    public class NinjaController : MainController
    {
        [Header("Climbing")]
        [SerializeField] private float climbJumpForce;  // Small force applyed wen we reach the top
        [SerializeField] private float climbSpeed;  // Our climbing speed

        public bool IsFalling { get; private set; }

        private float rayCastDistance = 1;
        private float climbTimer;
        private float climbMaxDuration = 1f;
        private int jumpsLeft = 2;  // How many times we can jump
        private int isFalling;  // If the character is falling
        private bool isClimbing = false;    // If we're currently climbing
        private bool checkForWall = false;    // If we where on the ground before wall climbing

        private bool onCooldown = false;    // If the climbing is currently on cooldown
        private bool secondJump = false;    // If the ninja can jump from the wal

        private Vector2 mouse;
        private float swaySmooth = 0.123f;


        [Header("Animators")]
        [SerializeField] private Animator tpAnim;
        [SerializeField] private Animator fpAnim;

        private RaycastHit hit; // Saves some memory on raycasts

        protected override void Start()
        {
            base.Start();

            IsFalling = false;
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        protected override void Update()
        {
            // if this character doesn't belong to me we stop and go back
            if (!photonView.IsMine)
                return;

            // If we're not climbing
            if (!isClimbing)
            {
                if (charController.isGrounded)
                {
                    climbTimer = 0;
                    onCooldown = false;
                    secondJump = false;
                }

                // Run the base class update
                base.Update();
            } else if (isClimbing)
            {
                climbTimer += Time.deltaTime;

                if (climbTimer >= climbMaxDuration)
                {
                    onCooldown = true;
                    climbTimer = 0;
                }
            }

            // Sway arms with mouse movement
            mouse = new Vector2(
                Mathf.Lerp(mouse.x, Input.GetAxis("Mouse X"), swaySmooth * swaySmooth * (3.0f - 2.0f * swaySmooth)), 
                Mathf.Lerp(mouse.y, Input.GetAxis("Mouse Y"), swaySmooth * swaySmooth * (3.0f - 2.0f * swaySmooth)));

            fpAnim.SetFloat(AnimationTags.MOUSEX, mouse.x / 1.5f);
            fpAnim.SetFloat(AnimationTags.MOUSEY, mouse.y / 1.5f);


            // If we're currently checking for a wall
            if (checkForWall)
            {
                if (!onCooldown)
                {
                    // We Check for a wall
                    CheckForClimbableWall();
                }
            }

            // Update the locomotion state
            UpdateLocomotionState();
        }

        /// <summary>
        /// FixedUpdate is called once per physics update
        /// </summary>
        protected override void FixedUpdate()
        {
            // if this character doesn't belong to me we stop and go back
            if (!photonView.IsMine)
                return;

            // If we're not climbing
            if (!isClimbing)
            {
                // Run the base class FixedUpdate
                base.FixedUpdate();
            }
        }

        /// <summary>
        /// Basic double jump (Just testing stuff)
        /// </summary>
        protected override void JumpCheck()
        {
            if (Input.GetButtonDown("Jump"))
            {
                // If we jumped start seaching for a climbable wall
                checkForWall = true;


                if (secondJump)
                {
                    secondJump = false;

                    tpAnim.SetBool(AnimationTags.DO_JUMP, true);

                    yVelocity = 0;

                    AddForce(Vector3.up, jumpForce * 1.5f);
                }

                // If we ever want a double jump it's already done xD //
                //    if (jumpsLeft < 2)
                //    {
                //        if (onGround)
                //        {
                //            jumpsLeft = 2;
                //        }
                //    }
                //    if (jumpsLeft == 2 || jumpsLeft == 1)
                //    {
                //        ResetImpact("Y");
                //        AddForce(Vector3.up, jumpForce);
                //        jumpsLeft--;
                //    }
            }

            // If we're on the ground and press to jump
            if (onGround && Input.GetButtonDown("Jump"))
            {
                tpAnim.SetBool(AnimationTags.DO_JUMP, true);

                // Adds a force making the player jump
                AddForce(Vector3.up, jumpForce);
            }
        }

        /// <summary>
        /// Checks for a climbable wall
        /// </summary>
        private void CheckForClimbableWall()
        {
            // Raycast in the forward direction to check if we're close to a wall
            if (Physics.Raycast(transform.position, transform.forward, out hit, rayCastDistance))
            {
                // If what we hit is Climbable
                if (hit.collider.GetComponent<Climable>() != null)
                {
                    // We start the climbing coroutine
                    StartCoroutine(Climb(hit.collider));
                    // We're no longing checking if there's a wall so..
                    checkForWall = false;
                }
            }
        }

        /// <summary>
        /// Coroutine to run so we can climb
        /// </summary>
        /// <param name="climableCollider">Collider bellonging to the wall we want to climb</param>
        /// <returns></returns>
        private IEnumerator Climb(Collider climableCollider)
        {
            // Set isClimbing to true
            isClimbing = true;

            // While we're pressing the jump key
            while (Input.GetButton("Jump") && !onCooldown)
            {
                Debug.DrawRay(transform.position, transform.forward * rayCastDistance, Color.green, 0.5f);

                // Ray cast forwards to check if we're hiting a wall
                if (Physics.Raycast(transform.position, transform.forward, out hit, rayCastDistance))
                {
                    // If it's still the same collider we first hit
                    if (hit.collider == climableCollider)
                    {
                        // Move our controller upwards to climb the wall
                        charController.Move((Vector3.up * climbSpeed) * Time.deltaTime);

                        secondJump = true;

                        yVelocity = 0;
                        // Delay to run the while each frame
                        yield return null;
                    } else
                    {
                        // Else break out of the while
                        break;
                    }
                } else
                {
                    break;
                }
            }

            // Prevent us from acomulating downwards velocity while climbing
            ResetImpact("Y");

            // Makes sure where wallclimbing
            if (!checkForWall)
            {
                // Adds a force to the player at the end of the climb
                AddForce(Vector3.up, climbJumpForce);
            }
            
            // Set isClimbing to false
            isClimbing = false;
        }

        // Pre written stuff so we can add animations later with less work
        protected override void UpdateLocomotionState()
        {
            if (!isClimbing)
            {
                if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
                {
                    tpAnim.SetBool(AnimationTags.IS_WALKING, true);
                } else
                {
                    tpAnim.SetBool(AnimationTags.IS_WALKING, false);
                }

                if (yVelocity < (-1.5f + gravity))
                {
                    IsFalling = true;

                    RaycastHit hit;
                    if (Physics.Raycast(transform.position, Vector3.down * 4, out hit))
                    {
                        if (Vector3.Distance(transform.position, hit.point) < 2f)
                        {
                            tpAnim.SetBool(AnimationTags.IS_NEAR_GROUND, true);
                        }
                    }
                } else if (onGround)
                {
                    tpAnim.SetBool(AnimationTags.IS_NEAR_GROUND, false);
                    IsFalling = false;
                } else
                {
                    IsFalling = false;
                }
            } else
            {
                tpAnim.SetBool(AnimationTags.IS_WALKING, false);
            }

            tpAnim.SetBool(AnimationTags.IS_CLIMBING, isClimbing);
            fpAnim.SetBool(AnimationTags.IS_CLIMBING, isClimbing);

        }
    }
}
