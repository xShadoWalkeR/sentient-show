﻿using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using UnityEngine;
using Photon.Pun;

public class KineticTraps : Ability {
    /// <summary>
    /// Class that holds mutable cooldown values for each trap
    /// </summary>
    private class TrapTime {

        // The trap's cooldown
        public float Time { get; set; }

        /// <summary>
        /// Assigns a cooldown to a trap
        /// </summary>
        /// <param name="t">The cooldown to assign</param>
        public TrapTime(float t) { Time = t; }
    }

    // Ability Cooldown [NOT USED]
    public override float AbilityCooldown { get; } = 0.0f;
    public override bool IsOnCooldown => false;

    private int trapDamage = 10;

    private float maxRange = 7.0f;   // The maximum Raycast range

    private float maxSlope = 0.5f;  // The maximum hit.normal slope value
    private float minSlope = -0.5f;  // The minimum hit.normal slope value

    private int maxTraps = 3;  // The max amount of traps
    private float trapCooldown = 10.0f; // Individual trap cooldown

    private float placeDelay = 1.0f; // Delay for placing multiple traps
    private float currentDelay = 0.0f; // The current delay

    // Time manager for spawned traps
    private Dictionary<int, TrapTime> traps;

    // The traps list
    private List<TrapPlayer> trapsList;

    // The current trap to reduce cooldown
    private int currentTrap = 0;

    // Our camera
    private Transform cam;

    // Start is called before the first frame update
    void Start() {

        // Get the player's camera
        if (photonView.IsMine) cam = Camera.main.transform;

        // Create the Dictionary to hold the traps' indexes (and their respective cooldowns)
        traps = new Dictionary<int, TrapTime>();

        // Create the List to hold the traps
        trapsList = new List<TrapPlayer>();
    }

    void Update() {

        if (photonView.IsMine) ManageCooldowns();
    }

    /// <summary>
    /// Removes a trap from the Dictionary
    /// and destroys it across the Network
    /// </summary>
    /// <param name="trapID"></param>
    public void RemoveTrap(int trapID) {

        int trapIndex = 0;

        // PLAY SOUNDS / EFFECTS / ANIMATIONS

        foreach (KeyValuePair<int, TrapTime> t in traps.OrderBy(pair => pair.Key)) {

            if (t.Key == trapID) {

                trapsList.RemoveAt(trapIndex);
                break;
            }

            trapIndex++;
        }

        // Remove the trap from the Dictionary
        traps.Remove(trapID);

        // Destroy the trap on all game instances
        PhotonNetwork.Destroy(PhotonView.Find(trapID).gameObject);
    }

    /// <summary>
    /// Manages all cooldowns
    /// </summary>
    private void ManageCooldowns() {

        // Iterate through each trap (in order of PhotonID)
        foreach (KeyValuePair<int, TrapTime> t in traps.OrderBy(pair => pair.Key)) {

            // If an ID is different from the previous one
            if (t.Key != currentTrap && t.Value.Time > 0) {

                // Save the new ID
                currentTrap = t.Key;
                break;

            } else {  // If the ID is the same

                // Reduce its cooldown
                if (t.Value.Time > 0) {

                    t.Value.Time -= Time.deltaTime;
                    break;
                }
            }
        }

        // Reduce the delay to place traps
        if (currentDelay > 0) currentDelay -= Time.deltaTime;
    }

    public override IEnumerator Cast() {

        // No cooldown derived from here
        yield return new WaitForSeconds(0);

        // Make a Raycast towards where the player is looking
        if (Physics.Raycast(cam.position, cam.forward, out RaycastHit hit, maxRange)) {

            // Ignore all players (including this one)
            if (!hit.transform.CompareTag(Tags.PLAYER_TAG) &&
                !hit.transform.CompareTag(Tags.ENEMY_TAG)) {

                // Check if the ground is acceptable
                if (hit.normal.x < maxSlope && hit.normal.x > minSlope &&
                    hit.normal.z < maxSlope && hit.normal.z > minSlope) {

                    // Place our trap
                    if (currentDelay <= 0.0f) PlaceTrap(hit.point);
                }
            }
        }
    }

    /// <summary>
    /// Places a Trap at a desired location
    /// </summary>
    /// <param name="position">The location to place the Trap</param>
    private void PlaceTrap(Vector3 position) {

        GameObject trap = null;

        // If the number of traps is less than the maximum amount...
        if (traps.Count < maxTraps) {

            // ...Spawn a trap...
            trap = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Trap"),
                position, Quaternion.identity);

            // ...Add it to the Dictionary (and List)...
            traps.Add(trap.GetPhotonView().ViewID, new TrapTime(trapCooldown));
            trapsList.Add(trap.GetComponent<TrapPlayer>());

            // ...Set this Ability as the spawner and the trap damage...
            trap.GetPhotonView().RPC("RPC_GetAbility", RpcTarget.AllBuffered, gameObject.name);
            //trap.GetComponent<TrapPlayer>().GetAbility(this);
            trap.GetComponent<TrapPlayer>().TrapDamage = trapDamage;

            // ...And reset the delay
            currentDelay = placeDelay;

        } // If the number of traps is the same as the maximum amount... 
        else if (traps.Count == maxTraps) {

            // The wanted ID
            int k = 0;

            // ...Iterate through each trap (in order of PhotonID)...
            foreach (KeyValuePair<int, TrapTime> t in traps.OrderBy(pair => pair.Key)) {

                // ...And if any trap has no cooldown, save that ID
                if (t.Value.Time <= 0) {

                    k = t.Key;
                    break;
                }
            }

            // If and ID was saved...
            if (k != 0) {

                // ...Removes the trap from the Dictionary
                // and Destroys it across the network
                RemoveTrap(k);

                // ...Instantiate a new trap...
                trap = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Trap"),
                position, Quaternion.identity);

                // ...Add it to the Dictionary (and List)...
                traps.Add(trap.GetPhotonView().ViewID, new TrapTime(trapCooldown));
                trapsList.Add(trap.GetComponent<TrapPlayer>());

                trap.GetPhotonView().RPC("RPC_GetAbility", RpcTarget.AllBuffered, gameObject.name);
                trap.GetComponent<TrapPlayer>().TrapDamage = trapDamage;

                // ...And reset the delay
                currentDelay = placeDelay;
            }
        }
    }

    /// <summary>
    /// Changes damage based on a multiplier
    /// </summary>
    /// <param name="increase">True if it will increase the damage</param>
    /// <param name="multiplier">The damage multiplier</param>
    public void ChangeDamage(bool increase, float multiplier) {

        if (increase) {

            trapDamage *= (int)multiplier;

            foreach (TrapPlayer trap in trapsList) {

                trap.TrapDamage *= (int)multiplier;
            }

        } else {

            trapDamage /= (int)multiplier;

            foreach (TrapPlayer trap in trapsList) {

                trap.TrapDamage /= (int)multiplier;
            }
        }
    }
}
