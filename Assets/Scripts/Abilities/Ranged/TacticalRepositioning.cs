﻿using System.Collections;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Controller;
using Cinemachine;

public class TacticalRepositioning : Ability {

    [SerializeField] public override float AbilityCooldown { get; } = 13f;   // The ultimate total cooldown
    [SerializeField] private float maxRange = 40.0f;        // The maximum Raycast range
    [SerializeField] public float minRange = 6.0f;          // The minimum Raycast range
    [SerializeField] private float tpDuration = 1.1f;       // The duration of the teleport
    [SerializeField] private float maxSlope = 0.5f;         // The maximum hit.normal slope value
    [SerializeField] private float minSlope = -0.5f;        // The minimum hit.normal slope value
    [SerializeField] private float angle = 3;               // The angle of the vector to raycast to, when the player is looking up
    [SerializeField] private float distanceToPoint = 1.4f;  // The minimum distance between the raycast's hitpoint and the upper position
    [SerializeField] private float ledgeOffset = 0.2f;      // The offset of the teleporting location

    // Support variable so we can only change its values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // The Ground Raycast hit info
    private RaycastHit hit = default;

    // The layer to hit
    private int layerMask;

    // Our desired location to teleport
    private Vector3 desiredLocation;

    /// <summary>
    /// This ability requires dual casting
    /// </summary>
    public override bool IsDualCast => true;

    // The Camera's Transform
    private Transform cam;

    // The Camera's pivot
    private Transform camPivot;

    // The 3rd Person Cam
    private CinemachineFreeLook thirdPersonCam;

    // Teleport effect at the point of the Raycast
    private GameObject tpEffect;

    // The Sniper's animator
    private Animator anim;

    // Animator and Handler of the second avatar
    private Animator cloneAnim;
    private SniperCloneHandler cloneHandler;

    // Our player component
    private Player player;

    // S.U.R.G.E.'s main controller script
    private SniperController sniperController;

    private void Awake() {

        // Fetch the animator
        anim = transform.GetChild(1).GetComponentInChildren<Animator>();

        // Get the third person camera
        thirdPersonCam = transform.GetChild(transform.childCount - 2).GetComponent<CinemachineFreeLook>();

        // Instantiate tpEffect locally
        tpEffect = Instantiate(Resources.Load<GameObject>("LocalPrefabs/TP_Effect"));
        tpEffect.SetActive(false);

        if (photonView.IsMine) {

            // Instantiate the second avatar through the network
            cloneAnim = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "SniperClone"), Vector3.zero, Quaternion.identity).
                GetComponent<Animator>();

            cloneAnim.GetComponent<PhotonView>().RPC("RPC_SetParent", RpcTarget.All, gameObject.name);

            cloneHandler = cloneAnim.GetComponent<SniperCloneHandler>();
        }
    }

    void Start() {

        // Assing our current cooldown as the given cooldown
        currentCooldown = AbilityCooldown + tpDuration;

        // Get the 'Default' layer
        layerMask |= 1 << LayerMask.NameToLayer("Default");

        // Get the player's camera
        if (photonView.IsMine) {

            cam = Camera.main.transform;
            camPivot = cam.parent;
        }

        // Get our player Component
        player = GetComponent<Player>();

        // Get our Sniper Controller
        sniperController = GetComponent<SniperController>();
    }

    void Update() {

        if (!photonView.IsMine) return;

        // If our ability is on cooldown
        if (IsOnCooldown) {

            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0) {

                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown + tpDuration;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }
    }

    public override bool FirstCast() {

        // If cooldown is active leave the method
        if (IsOnCooldown || !sniperController.IsGrounded) {

            desiredLocation = Vector3.zero;
            DisableEffects();
            return false;
        }

        // Verify if player is looking up
        if (cam.forward.y > 0) {

            // Calculate a vector slightly below the cam.forward vector
            Vector3 rotVector = Quaternion.AngleAxis(angle, cam.right) * cam.forward;

            // Raycast using the previous vector to try and find ledges while looking above them
            if (Physics.Raycast(cam.position, rotVector, out RaycastHit hit, maxRange, layerMask)) {

                // Calculate the highest point on the object's collider, and a small ledge offset
                Vector3 offsetVector = hit.normal * ledgeOffset;

                Vector3 vectorInside = hit.point;
                vectorInside -= offsetVector;

                Vector3 highestPoint = vectorInside + Vector3.up * (distanceToPoint * 2.0f);

                if (Physics.Raycast(highestPoint, Vector3.down, out RaycastHit upperHit, distanceToPoint * 3.0f, layerMask)) {

                    // Certify that we're looking above the edge
                    if (Vector3.Distance(hit.point, upperHit.point) < distanceToPoint) {

                        // Set our desired teleporting location
                        desiredLocation = upperHit.point;

                        // Change the effects position to the hitpoint 
                        // and warn this method's caller that he's hitting something
                        tpEffect.transform.position = desiredLocation;
                        tpEffect.transform.rotation = transform.rotation;
                        tpEffect.SetActive(true);
                        return true;
                    }
                }
            }

        } else {

            // Rasycast towards regular surfaces with colliders
            if (Physics.Raycast(cam.position, cam.forward, out hit, maxRange, layerMask)) {

                // Verify if the distance isn't too close
                if (hit.distance > minRange) {

                    // Check if the surface we're hitting is too vertical
                    if (hit.normal.x > maxSlope || hit.normal.x < minSlope ||
                        hit.normal.z > maxSlope || hit.normal.z < minSlope) {

                        desiredLocation = Vector3.zero;
                        DisableEffects();
                        return false;
                    }

                    // Set the hitpoint as out desired teleporting location
                    desiredLocation = hit.point;

                    // Change the effects position to the hitpoint 
                    // and warn this method's caller that he's hitting something
                    tpEffect.transform.position = desiredLocation;
                    tpEffect.transform.rotation = transform.rotation;
                    tpEffect.SetActive(true);
                    return true;
                }
            }
        }

        // Reset the hitpoint and disable effects
        desiredLocation = Vector3.zero;
        DisableEffects();
        return false;
    }

    public override void DisableEffects() {

        // Disable the effect
        tpEffect.SetActive(false);
    }

    public override IEnumerator Cast() {

        // Checks if our hitpoint is not reset
        if (desiredLocation != Vector3.zero) {

            // Starts cooldown after teleport is complete
            isOnCooldown = true;

            // Disables the tpEffect
            DisableEffects();

            // Start the teleporting animation(s)
            anim.SetBool("Teleport", true);
            cloneAnim.SetBool("Teleport", true);

            StartCoroutine(Activate3RDPerson(true));

            // Blend the second layer's Weight so that he stops looking at where the player points
            StartCoroutine(anim.GetComponent<SniperIKHandler>().BlendWeight(0.5f, 1.5f));

            // Disable the CC to be able to everride our position
            GetComponent<CharacterController>().enabled = false;
            GetComponent<CameraController>().enabled = false;

            // Waits for the duration of the teleport
            yield return new WaitForSeconds(tpDuration);

            // Teleports the player after timer
            transform.position = desiredLocation;

            // Waits for animation to end
            yield return new WaitForSeconds(tpDuration);

            // Set 'Teleport' to false on both anims
            anim.SetBool("Teleport", false);
            cloneAnim.SetBool("Teleport", false);

            Vector3 rotEuler = cam.eulerAngles;
            StartCoroutine(Activate3RDPerson(false));

            // Re-Enable the CC
            GetComponent<CharacterController>().enabled = true;
            GetComponent<CameraController>().enabled = true;

            GetComponent<CameraController>().CurCameraRot = rotEuler.x;

            // Tell the player it's no longer Casting
            player.IsCasting = false;
        }
    }

    private IEnumerator Activate3RDPerson(bool activate) {

        if (activate) {

            thirdPersonCam.m_YAxis.Value = 0.7f;
            thirdPersonCam.m_XAxis.Value = 0.0f;

            thirdPersonCam.Priority = 11;

            yield return new WaitForSeconds(0.05f);

            sniperController.SwitchTo3rdPerson(true);
            cloneHandler.ActivateRenderers(true);

        } else {

            thirdPersonCam.Priority = 9;

            // Apply rotations that mimic 3rd person
            Vector3 rotEuler = cam.eulerAngles;
            GetComponent<CameraController>().CurCameraRot = 0.0f;

            rotEuler.x = 0;
            rotEuler.z = 0;
            transform.eulerAngles = rotEuler;
            //-------------------------------------

            yield return new WaitForSeconds(0.3f);

            sniperController.SwitchTo3rdPerson(false);
            cloneHandler.ActivateRenderers(false);
        }
    }
}
