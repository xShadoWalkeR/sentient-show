﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Controller;

/// <summary>
/// Handles the pause menu
/// </summary>
public class PauseMenu : MonoBehaviour
{
    private static bool GamePaused;
    [SerializeField] private GameObject pauseUI;

    private Player player;
    private MainController controller;
    private CameraController camController;
    private PlayerShoot playerShoot;

    [HideInInspector]
    public GameObject photonPlayer;

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = PlayerPrefs.GetInt("GameFPSLock");

        player = GetComponent<UiManager>().myPlayerObject.GetComponent<Player>();

        controller = player.GetComponent<MainController>();

        camController = player.GetComponent<CameraController>();

        playerShoot = player.GetComponent<PlayerShoot>();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GamePaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    /// <summary>
    /// Gets back to game
    /// </summary>
    public void Resume()
    {
        pauseUI.SetActive(false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        player.CanCast = true;
        controller.EnableLocomotion = true;
        camController.enabled = true;
        playerShoot.enabled = true;

        GamePaused = false;
    }

    /// <summary>
    /// Displays the pause menu, stopping all player movement
    /// </summary>
    public void Pause()
    {
        pauseUI.SetActive(true);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        player.CanCast = false;
        controller.EnableLocomotion = false;
        camController.enabled = false;
        playerShoot.enabled = false;

        GamePaused = true;
    }

    /// <summary>
    /// Goes back to the main menu disconnecting from the lobby
    /// </summary>
    public void Loaded()
    {
        PhotonNetwork.DestroyPlayerObjects(player.photonView.OwnerActorNr);
        //PhotonNetwork.Destroy(player.gameObject);
        //PhotonNetwork.Destroy(photonPlayer);
        //Destroy(gameObject, 1f);
        Destroy(PhotonRoom.room.gameObject);
        PhotonNetwork.LeaveRoom();
        pauseUI.SetActive(false);
        GamePaused = false;
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// Closes the game
    /// </summary>
    public void Quit()
    {
        Debug.Log("Quitting game");
        Application.Quit();
    }
}