﻿using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

/// <summary>
/// Handles the photon lobby
/// </summary>
public class PhotonLobby : MonoBehaviourPunCallbacks
{
    public static PhotonLobby lobby;

    public GameObject playButton;
    public GameObject offLineButton;
    public GameObject cancelButton;
    

    public bool RoomCreated { get; private set; }

    /// <summary>
    /// Called before the start
    /// </summary>
    private void Awake()
    {
        // Create a singleton
        lobby = this;

        RoomCreated = false;
    }

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        // Connect to the master photon server.
        if (!PhotonNetwork.IsConnected)
            PhotonNetwork.ConnectUsingSettings();
    }

    /// <summary>
    /// When the player is connected to the master
    /// </summary>
    public override void OnConnectedToMaster()
    {
        print("Player has connected to the Photon master server.");
        PhotonNetwork.AutomaticallySyncScene = true;
        playButton.SetActive(true);
        offLineButton.SetActive(false);
    }

    /// <summary>
    /// When the play button is clicked
    /// </summary>
    public void OnPlayClicked()
    {
        playButton.SetActive(false);
        cancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }

    /// <summary>
    /// When we fail to join a random room
    /// </summary>
    /// <param name="returnCode">The return code</param>
    /// <param name="message">The message</param>
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        print("Tried to join a game but failed. There must be no open games available.");
        CreateRoom();
    }

    /// <summary>
    /// Creates a new room
    /// </summary>
    private void CreateRoom()
    {
        print("Trying to create a room");
        RoomCreated = true;
        int randomRoomName = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)MultiplayerSettings.mpSettings.MaxPlayer };
        PhotonNetwork.CreateRoom("Room" + randomRoomName, roomOps);
    }

    /// <summary>
    /// When the player joins a room
    /// </summary>
    public override void OnJoinedRoom()
    {
        print("Joined a room!");
    }

    /// <summary>
    /// When the player fails to create a room
    /// </summary>
    /// <param name="returnCode">The return code</param>
    /// <param name="message">The message</param>
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        print("Tried to create a new room but failed, there must already be a room with the same name.");
        CreateRoom();
    }

    /// <summary>
    /// When the palyer clicks the cancel button
    /// </summary>
    public void OnCancelButton()
    {
        cancelButton.SetActive(false);
        playButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}
