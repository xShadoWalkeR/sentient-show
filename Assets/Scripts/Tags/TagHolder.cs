﻿
using UnityEngine;
/// 
/// Drop here any "Tags" you'll need to use in diferent scripts
/// 
public class AnimationTags
{
    // Not really setup properly yeat cause we don't really have any animations
    // So use these "premade" string when setting up the animation parameters

    public const string CAN_SHOOT = "CanShoot";
    public const string RELOAD = "Reload";
    public const string SHOOT_TRIGGER = "Shoot";
    public const string AIM_PARAMETER = "Aim";
    public const string CYCLE_OFFSET = "CycleOffset";

    public const string WALK_PARAMETER = "Walk";
    public const string RUN_PARAMETER = "Run";
    public const string ATTACK_TRIGGER = "Attack";
    public const string DEAD_TRIGGER = "Dead";

    // Ninja specific
    public static int DO_DASH = Animator.StringToHash("doDash");
    public static int HAS_BOW = Animator.StringToHash("hasBow");
    public static int LONG_PRESS = Animator.StringToHash("longPress");
    public static int IS_DASHING = Animator.StringToHash("isDashing");
    public static int HAS_KATANA = Animator.StringToHash("hasKatana");
    public static int ATTACK_TYPE = Animator.StringToHash("attackType");
    public static int IS_CLIMBING = Animator.StringToHash("isClimbing");
    public static int THROW_SMOKE = Animator.StringToHash("throwSmoke");
    public static int THROW_KUNAI = Animator.StringToHash("throwKunai");
    public static int IS_ATTACKING = Animator.StringToHash("isAttacking");
    public static int START_ATTACK = Animator.StringToHash("startAttack");
    public static int SWITCH_TO_BOW = Animator.StringToHash("switchToBow");
    public static int SWITCH_TO_KATANA = Animator.StringToHash("switchToKatana");

    // Sniper specific
    public static int ZOOM = Animator.StringToHash("Zoom");
    public static int INPUT = Animator.StringToHash("Input");
    public static int INPUT_X = Animator.StringToHash("InputX");
    public static int INPUT_Y = Animator.StringToHash("InputY");
    public static int IS_GROUNDED = Animator.StringToHash("IsGrounded");
    public static int SPEED_MULTI = Animator.StringToHash("SpeedMultiplier");
    public static int DASH_BACK = Animator.StringToHash("DashBack");
    public static int SHOOT_LEFT = Animator.StringToHash("ShootLeft");
    public static int SHOOT_RIGHT = Animator.StringToHash("ShootRight");
    public static int CHARGE = Animator.StringToHash("Charge");
    public static int OVERLOAD = Animator.StringToHash("Overload");

    // Any Char
    public static int DO_JUMP = Animator.StringToHash("doJump");
    public static int IS_SLOWED = Animator.StringToHash("isSlowed");
    public static int IS_ROOTED = Animator.StringToHash("isRooted");
    public static int IS_FALLING = Animator.StringToHash("isFalling");
    public static int IS_WALKING = Animator.StringToHash("isWalking");
    public static int IS_STUNNED = Animator.StringToHash("isStunned");
    public static int IS_NEAR_GROUND = Animator.StringToHash("isNearGround");
    public static int MOUSEX = Animator.StringToHash("MouseX");
    public static int TIME_ON_AIR = Animator.StringToHash("airTime");
    public static int MOUSEY = Animator.StringToHash("MouseY");
}

public class Tags
{
    // Other strings
    public const string CAM_PIVOT = "CameraPivot";
    public const string WEAPON_CAM = "WeaponCamera";
    public const string MAIN_CAM = "MainCamera";
    public const string CROSSHAIR = "Crosshair";

    // Player Strings
    public const string PLAYER_TAG = "Player";
    public const string TO_BE_SET = "ToBeSet";
    public const string ENEMY_TAG = "Enemy";
    public const string ALLY_TAG = "Ally";
    
    // Ability strings
    public const string FIRST_ABILITY = "Ability 1";
    public const string SECOND_ABILITY = "Ability 2";
    public const string ULTIMATE_ABILITY = "Ultimate";
}