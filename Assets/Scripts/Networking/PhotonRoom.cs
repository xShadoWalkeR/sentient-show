﻿using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

/// <summary>
/// Creates and handles the photon rooms
/// </summary>
public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    // Room info
    public static PhotonRoom room;

    [SerializeField] TMP_Text timer;

    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip clip;

    private float pulseTimer;

    public bool isGameLoaded;
    public int currentScene;

    // Player info
    private Photon.Realtime.Player[] photonPlayers;
    public int playersInRoom;
    public int myNumberInRoom;
    public int playerInGame;

    //Delayed start
    private bool readyToCount;
    private bool readyToStart;
    public float startingTime;
    private float lessThanMaxPlayers;
    private float atMaxPlayers;
    private float timeToStart;

    private TimeSync startTimer;
    private bool masterClient;

    /// <summary>
    /// Called before the start
    /// </summary>
    private void Awake()
    {
        // Set up our singleton
        if(PhotonRoom.room == null)
        {
            PhotonRoom.room = this;
        } else
        {
            if (PhotonRoom.room != this)
            {
                Destroy(PhotonRoom.room.gameObject);
                PhotonRoom.room = this;
            }
        }

        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Called when the object is enabled
    /// </summary>
    public override void OnEnable()
    {
        // Subscribe to functions
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    /// <summary>
    /// Called when the object is disabled
    /// </summary>
    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }

    /// <summary>
    /// Initialization
    /// </summary>
    void Start()
    {
        readyToCount = false;
        readyToStart = false;
        masterClient = false;
        lessThanMaxPlayers = startingTime;
        atMaxPlayers = startingTime;
        timeToStart = startingTime;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (!isGameLoaded && myNumberInRoom > 1 && startTimer == null)
        {
            startTimer = GameObject.FindGameObjectWithTag("ReadyTimer")?.GetComponent<TimeSync>();
            masterClient = false;
        }

        if(MultiplayerSettings.mpSettings.DelayStart)
        {
            if(playersInRoom == 0)
            {
                RestartTimer();
            }

            if(!isGameLoaded)
            {
                if(readyToStart)
                {
                    if (masterClient)
                    {
                        atMaxPlayers -= Time.deltaTime;
                        lessThanMaxPlayers = atMaxPlayers;
                        timeToStart = atMaxPlayers;

                        startTimer.TimeToStart = timeToStart;
                    } else
                    {
                        timeToStart = startTimer.TimeToStart;
                    }

                } else if(readyToCount)
                {
                    if (masterClient)
                    {
                        lessThanMaxPlayers -= Time.deltaTime;
                        timeToStart = lessThanMaxPlayers;

                        startTimer.TimeToStart = timeToStart;
                    } else
                    {
                        timeToStart = startTimer.TimeToStart;
                    }
                }

                // Animate text scale when the timer it's 5 seconds

                if (timeToStart <= 6f)
                {
                    if (pulseTimer == 0)
                    {
                        audioSource.PlayOneShot(clip, 2f);
                    }

                    timer.color = Color.red;

                    if (pulseTimer <= 0.333f)
                    {
                        timer.fontSize += Time.deltaTime * 100;
                    } else
                    {
                        timer.fontSize -= Time.deltaTime * 50;
                    }

                    pulseTimer += Time.deltaTime;

                    if (pulseTimer >= 0.999f)
                    {
                        pulseTimer = 0;
                    }
                }

                timer.text = ((int)timeToStart).ToString();
                
                
                if(timeToStart <= 1.2f)
                {
                    StartGame();
                }
            }
        }
    }

    /// <summary>
    /// When the player joins a room
    /// </summary>
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        print("Joined a room!");

        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom = photonPlayers.Length;
        myNumberInRoom = playersInRoom;

        if (myNumberInRoom == 1)
        {
            startTimer = PhotonNetwork.InstantiateSceneObject(Path.Combine("PhotonPrefabs", "TimeSetup"), transform.position, Quaternion.identity, 0).GetComponent<TimeSync>();
            masterClient = true;
        }
        
        GameObject tmp = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonNetworkPlayer"), transform.position, Quaternion.identity, 0);

        tmp.GetComponent<PhotonPlayer>().myTeam = playersInRoom % 2 == 0 ? 2 : 1;

        if (!CheckDelayedStart())
        {
            StartGame();
        }
    }

    /// <summary>
    /// When a player enters a room
    /// </summary>
    /// <param name="newPlayer">The photon player</param>
    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        print("A new player has joined the room");
        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom++;
        CheckDelayedStart();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        PhotonRoom.room.playersInRoom--;
        base.OnPlayerLeftRoom(otherPlayer);
    }

    /// <summary>
    /// Check if we want a delayed start
    /// </summary>
    /// <returns></returns>
    private bool CheckDelayedStart()
    {
        if (MultiplayerSettings.mpSettings.DelayStart)
        {
            print($"Displayer players in room out of max players possible ({playersInRoom} : {MultiplayerSettings.mpSettings.MaxPlayer})");
            if (playersInRoom > 1)
            {
                readyToCount = true;
            }
            if (playersInRoom == MultiplayerSettings.mpSettings.MaxPlayer)
            {
                readyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return true;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
            return true;
        }
        return false;
    }

    /// <summary>
    /// Starts the game
    /// </summary>
    private void StartGame()
    {
        isGameLoaded = true;
        if (!PhotonNetwork.IsMasterClient)
            return;
        if(MultiplayerSettings.mpSettings.DelayStart)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }

        PhotonNetwork.LoadLevel(MultiplayerSettings.mpSettings.MultiplayerScene);
    }

    /// <summary>
    /// Restarts the timer
    /// </summary>
    public void RestartTimer()
    {
        lessThanMaxPlayers = startingTime;
        timeToStart = startingTime;
        atMaxPlayers = startingTime;
        readyToCount = false;
        readyToStart = false;
    }

    /// <summary>
    /// Called when the multiplayer scene is loaded
    /// </summary>
    /// <param name="scene">Scene to switch to.</param>
    /// <param name="mode">Load mode for the scene</param>
    private void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;
        if(currentScene == MultiplayerSettings.mpSettings.MultiplayerScene)
        {
            isGameLoaded = true;

            if(MultiplayerSettings.mpSettings.DelayStart)
            {
                photonView.RPC("RPC_LoadedGameScene", RpcTarget.MasterClient);
            } else
            {
                //RPC_CreatePlayer();
            }
        }
    }

    /// <summary>
    /// Called as an RPC when the scene finished loading
    /// </summary>
    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        playerInGame++;
        if(playerInGame == PhotonNetwork.PlayerList.Length)
        {
            //RPC_CreatePlayer();
            //photonView.RPC("RPC_CreatePlayer", RpcTarget.All);
        }
    }

    /// <summary>
    /// RPC to create the player
    /// </summary>
    [PunRPC]
    private void RPC_CreatePlayer()
    {
        if (PhotonLobby.lobby.RoomCreated)
        {
            PhotonNetwork.InstantiateSceneObject(Path.Combine("PhotonPrefabs", "GameModeManager"), transform.position, Quaternion.identity, 0);
        }
    }
}
