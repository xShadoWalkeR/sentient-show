﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[ExecuteInEditMode]
public class InvisibleWallsGenerator : MonoBehaviour {

    [SerializeField] private bool createWalls = false;
    [SerializeField] private bool destroyWalls = false;
    [SerializeField] private GameObject spherePrefab;

    private List<Transform> spheres;

    private void Update() {

        GenerateWalls();

        DestroyWalls();
    }

    private void GenerateWalls() {

        int i = 0;

        spheres = new List<Transform>();

        if (createWalls) {

            Mesh mesh = GetComponent<MeshFilter>().sharedMesh;

            foreach (Vector3 v1 in mesh.vertices) {

                foreach (Vector3 v2 in mesh.vertices) {

                    if ((v1.x == v2.x) && (v1.y > v2.y) && (v1.z == v2.z)) {

                        GameObject sphere = Instantiate(spherePrefab, transform.TransformPoint(v1), Quaternion.identity, transform);
                        sphere.transform.localRotation = Quaternion.identity;
                        sphere.name = $"Sphere{i}";

                        spheres.Add(sphere.transform);
                    }
                }

                i++;
            }

            DestroyDupes();

            createWalls = false;
        }
    }

    private void DestroyDupes() {

        List<Transform> tempList = transform.Cast<Transform>().ToList();

        List<Transform> dupes = tempList.GroupBy(c => c.localPosition).Select(c => c.First()).ToList();

        foreach (Transform child in tempList) {

            if (!dupes.Contains(child)) {

                DestroyImmediate(child.gameObject);
            }
        }

        tempList = dupes;

        OrganizePoints(tempList);
    }

    private void OrganizePoints(List<Transform> list) {

        List<Transform> open = new List<Transform>();
        List<Transform> closed = new List<Transform>();

        open.Add(list[0]);

        //while (open.Count > 0) {


        //}

        print("Dot: " + Vector3.Dot(list[1].position - list[0].position, list[0].forward));
    }

    private void DestroyWalls() {

        if (destroyWalls) {

            List<Transform> tempList = transform.Cast<Transform>().ToList();

            foreach (Transform child in tempList) {

                DestroyImmediate(child.gameObject);
            }

            destroyWalls = false;
        }
    }

    private void OnDrawGizmos() {

        Gizmos.color = Color.red;

        //Mesh mesh = GetComponent<MeshFilter>().sharedMesh;

        //foreach (Vector3 vertice in mesh.vertices) {

        //    if (Physics.Raycast(vertice, Vector3.down, out hit, raycastDistance, gameObject.layer)) {

        //        Gizmos.DrawSphere(vertice, 0.2f);
        //    }
        //}

        // ####################################

        //MeshCollider m = GetComponent<MeshCollider>();

        //Gizmos.color = Color.red;

        //Vector3 A = transform.TransformPoint(new Vector3(m.bounds.size.x, m.bounds.size.y, m.bounds.size.z) * 0.5f);
        //Vector3 B = transform.TransformPoint(new Vector3(-m.bounds.size.x, m.bounds.size.y, m.bounds.size.z) * 0.5f);
        //Vector3 C = transform.TransformPoint(new Vector3(-m.bounds.size.x, m.bounds.size.y, -m.bounds.size.z) * 0.5f);
        //Vector3 D = transform.TransformPoint(new Vector3(m.bounds.size.x, m.bounds.size.y, -m.bounds.size.z) * 0.5f);

        //Gizmos.DrawSphere(A, 0.5f);
        //Gizmos.DrawSphere(B, 0.5f);
        //Gizmos.DrawSphere(C, 0.5f);
        //Gizmos.DrawSphere(D, 0.5f);
    }
}
