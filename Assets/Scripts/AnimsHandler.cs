﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimsHandler : MonoBehaviour {

    [SerializeField] private AudioSource source;

    private Animator anim;

    private void Awake() {

        anim = GetComponent<Animator>();
    }

    public void JumpStarted() {

        anim.SetBool(AnimationTags.DO_JUMP, false);
    }

    public void Dashed() {

        anim.SetBool(AnimationTags.DASH_BACK, false);
    }

    public void Shot() {

        anim.SetBool(AnimationTags.SHOOT_TRIGGER, false);
    }

    public void ShotLeft() {

        anim.SetBool(AnimationTags.SHOOT_LEFT, false);
    }

    public void ShotRight() {

        anim.SetBool(AnimationTags.SHOOT_RIGHT, false);
    }

    // ---------- AUDIO ----------

    protected void PlaySound(AudioClip clip) {

        if (source.enabled) {

            source.clip = clip;
            source.Play();
        }
    }
}
