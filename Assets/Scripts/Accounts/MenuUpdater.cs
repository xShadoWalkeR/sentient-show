﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuUpdater : MonoBehaviour {

    [Header("Main Camera")]
    [SerializeField] private Camera mainCam;
    [SerializeField] private Transform secondaryPosition;

    [Header("Welcome Message")]
    [SerializeField] private TMP_Text welcomeMessage = null;

    [Header("Buy Menu")]
    [SerializeField] private GameObject buyMenu = null;
    [SerializeField] private GameObject[] lockedBuyAbilities = null;
    [SerializeField] private TMP_Text[] tokenAmounts = null;

    [Header("Selection Menu")]
    [SerializeField] private GameObject[] lockedSelectAbilities = null;

    [Header("Progression")]
    [SerializeField] private TMP_Text levelDisplay = null;
    [SerializeField] private TMP_Text tokenDisplay = null;
    [SerializeField] private TMP_Text xpDisplay = null;
    [SerializeField] private Slider xpSlider = null;

    [Header("Audio")]
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip[] clips;

    [Header("Options")]
    [SerializeField] private GameObject optionsMenu;

    public string AbilityToUnlock { get; set; }

    private AccountManager am;

    private string currentUser;

    private Coroutine redRoutine;

    private void Start() {

        am = AccountManager.am;

        QualitySettings.vSyncCount = 1;

        Invoke("LateDisableOptions", 0.03f);
    }

    private void Update() {

        UpdateUIElements();
    }

    private void LateDisableOptions()
    {
        optionsMenu.SetActive(false);
    }

    private void UpdateUIElements() {

        // Detect when user has been set
        if (currentUser != am.UserName) {

            UpdateWelcomeMessage();

            if (currentUser != null) {

                UpdateLockedAbilities();
                UpdateProgressDisplay();
            }
        }
    }

    private void UpdateWelcomeMessage() {

        currentUser = am.UserName;
        welcomeMessage.SetText($"{currentUser}");
    }

    // Change to 1 'for' later
    private void UpdateLockedAbilities() {

        // Name format for 'Locked' images and buttons:
        // Locked - NameOfAbility

        foreach (GameObject g in lockedBuyAbilities) {

            string[] abilityName = g.name.Split('-');

            if (am.AbilityUnlocked(abilityName[1])) {

                g.SetActive(false);

            } else {

                g.SetActive(true);
            }
        }

        foreach (GameObject g in lockedSelectAbilities) {

            string[] abilityName = g.name.Split('-');

            if (am.AbilityUnlocked(abilityName[1])) {

                g.SetActive(false);

            } else {

                g.SetActive(true);
            }
        }

        UpdateTokensDisplay();
    }

    // Also updates the display on the progress
    private void UpdateTokensDisplay() {

        foreach (TMP_Text t in tokenAmounts) {

            t.SetText($"{am.GetAccountValue("tokens")}");
        }

        tokenDisplay.SetText($"{am.GetAccountValue("tokens")}");
    }

    private void UpdateProgressDisplay() {

        int myLevel = am.GetAccountValue("accountLevel");

        levelDisplay.SetText($"Level: {myLevel}");
        
        // Verify MaxLevel
        if (myLevel >= 40) {

            xpSlider.value = xpSlider.maxValue;
            xpDisplay.SetText("Max Level");

        } else {

            int xpAmount = am.GetAccountValue("levelXP");

            xpSlider.value = xpAmount;
            xpDisplay.SetText($"{xpAmount} XP");
        }
    }

    public void VerifyTokens() {

        int myTokens = am.GetAccountValue("tokens");

        if (myTokens > 0) {

            buyMenu.SetActive(true);

        } else {

            ShowTokenWarning();

            PlayLockedSound();
        }
    }

    public void MoveCamera()
    {
        mainCam.transform.position = secondaryPosition.position;
        mainCam.transform.rotation = secondaryPosition.rotation;
    }

    public void UnlockAbility() {

        am.UnlockAbility(AbilityToUnlock);
        UpdateLockedAbilities();
    }

    private void ShowTokenWarning() {

        if (redRoutine != null) StopCoroutine(redRoutine);

        redRoutine = StartCoroutine(ShowRed());
    }

    private void PlayLockedSound()
    {
        for (int i = 0; i < clips.Length; i++)
        {
            if (clips[i].name == "Locked")
            {
                audioSource.clip = clips[i];
            }
        }

        audioSource.Play();
    }

    private IEnumerator ShowRed() {

        foreach (TMP_Text t in tokenAmounts) {

            t.color = Color.red;

            yield return new WaitForSeconds(0.07f);

            t.color = Color.white;

            yield return new WaitForSeconds(0.07f);

            t.color = Color.red;

            yield return new WaitForSeconds(0.07f);

            t.color = Color.white;
        }

        redRoutine = null;
    }
}
