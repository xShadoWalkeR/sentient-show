﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

/// <summary>
/// Set's the game to balance the teams
/// </summary>
public class GameSetup : MonoBehaviourPunCallbacks
{
    // Singleton
    public static GameSetup GS;

    // The spawnpoints for team one
    public Transform[] spawnPointsTeamOne;
    
    // The spawnpoints for team two
    public Transform[] spawnPointsTeamTwo;

    /// <summary>
    /// When the object is enabled
    /// </summary>
    private void OnEnable()
    {
        // Set the singleton
        if(GameSetup.GS == null)
        {
            GameSetup.GS = this;
        }
    }
}
