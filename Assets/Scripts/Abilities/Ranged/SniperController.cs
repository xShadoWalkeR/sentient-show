﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Controller {

    public class SniperController : MainController {

        // The sniper's Animator
        [SerializeField] private Animator anim;

        [SerializeField] private Animator firstPerson;

        [SerializeField] private GameObject[] cm_Cameras;

        // The sniper's model(s)
        [SerializeField] private Renderer[] models;
        public Renderer[] Models { get => models; }

        [SerializeField] private Renderer[] fP_Models;

        [SerializeField] private AudioSource[] tp_audioSources;

        [SerializeField] private AudioSource[] fp_audioSources;

        [SerializeField] private AudioSource ultiSource;
        public AudioSource UltiSource { get => ultiSource; }

        public bool IsGrounded { get => onGround; }
        private float timeOnAir;

        protected override void Start() {

            base.Start();

            // Check if the PhotonView belongs to the player
            if (photonView.IsMine) {

                // Deactivate all local models
                foreach (Renderer r in models) {

                    r.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
                }

                // Activate my Cinemachine cams
                foreach (GameObject g in cm_Cameras) {

                    g.SetActive(true);
                }

                foreach (AudioSource a in tp_audioSources) {

                    a.enabled = false;
                }

            } else {

                foreach (AudioSource a in fp_audioSources) {

                    a.enabled = false;
                }

                firstPerson.gameObject.SetActive(false);
            }
        }

        protected override void Update() {

            base.Update();

            // Check if the PhotonView belongs to the player
            if (photonView.IsMine) {

                ManageAnimations();
            }
        }

        protected override void FixedUpdate() {

            base.FixedUpdate();
        }

        private void ManageAnimations() {

            // Apply our Input to the Animator so that it blends through animations
            anim.SetFloat(AnimationTags.INPUT, new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).sqrMagnitude);

            firstPerson.SetFloat(AnimationTags.INPUT, new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).sqrMagnitude);

            anim.SetFloat(AnimationTags.INPUT_X, Input.GetAxisRaw("Horizontal"), 0.4f, 12.0f * Time.deltaTime);
            anim.SetFloat(AnimationTags.INPUT_Y, Input.GetAxisRaw("Vertical"), 0.4f, 12.0f * Time.deltaTime);

            firstPerson.SetFloat(AnimationTags.INPUT_X, Input.GetAxisRaw("Horizontal"), 0.5f, 2.0f * Time.deltaTime);
            firstPerson.SetFloat(AnimationTags.INPUT_Y, Input.GetAxisRaw("Vertical"), 0.5f, 2.0f * Time.deltaTime);

            anim.SetBool(AnimationTags.IS_GROUNDED, onGround);

            firstPerson.SetBool(AnimationTags.IS_GROUNDED, onGround);
            firstPerson.SetFloat(AnimationTags.TIME_ON_AIR, timeOnAir);

            timeOnAir = onGround ? 0.0f : timeOnAir + Time.deltaTime;

            if (anim.GetBool(AnimationTags.IS_SLOWED)) {

                anim.SetFloat(AnimationTags.SPEED_MULTI, 0.5f);

            } else {

                anim.SetFloat(AnimationTags.SPEED_MULTI, 1.0f);
            }

            // If we're on the ground and press to jump
            if (onGround && Input.GetButtonDown("Jump")) {

                anim.SetBool(AnimationTags.DO_JUMP, true);

                firstPerson.SetTrigger(AnimationTags.DO_JUMP);

                timeOnAir = 0.0f;
            }
        }

        public void SwitchTo3rdPerson(bool value) {

            if (value) {

                foreach (Renderer r in models) {

                    r.shadowCastingMode = ShadowCastingMode.On;
                }

                foreach (Renderer r in fP_Models) {

                    r.enabled = false;
                }

            } else {

                foreach (Renderer r in fP_Models) {

                    r.enabled = true;
                }

                foreach (Renderer r in models) {

                    r.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
                }
            }
        }
    }
}
