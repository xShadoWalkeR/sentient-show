﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    [SerializeField] private float blendTime;

    private int switchToBow;
    private int switchToKatana;
    private int doDash;
    private int doJump;
    private int hasBow;
    private int hasKatana;
    private int isClimbing;
    private int isDashing;
    private int wallClimbFromBow;
    private int wallClimbFromKatana;
    private int attackType;
    private int pullBack;
    private int throwSmoke;
    private int throwKunai;
    private int fromBow;
    private int fromKatana;
    private int katanaFromWallClimb;

    private bool isRotated;
    private bool isEnabeled;
    private bool lockLayer;

    private Animator myAnim;

    [SerializeField] private GameObject katana;
    [SerializeField] private GameObject arrow;
    [SerializeField] private AudioSource sayaSource;
    [SerializeField] private AudioClip[] footSteps;
    [SerializeField] private GameObject ninjaArms;

    private void Start()
    {
        myAnim = GetComponent<Animator>();
        switchToBow = Animator.StringToHash("switchToBow");
        switchToKatana = Animator.StringToHash("switchToKatana");
        doDash = Animator.StringToHash("doDash");
        doJump = Animator.StringToHash("doJump");
        hasBow = Animator.StringToHash("hasBow");
        hasKatana = Animator.StringToHash("hasKatana");
        isClimbing = Animator.StringToHash("isClimbing");
        isDashing = Animator.StringToHash("isDashing");
        wallClimbFromBow = Animator.StringToHash("wallClimbFromBow");
        wallClimbFromKatana = Animator.StringToHash("wallClimbFromKatana");
        attackType = Animator.StringToHash("attackType");
        pullBack = Animator.StringToHash("pullBack");
        throwSmoke = Animator.StringToHash("throwSmoke");
        throwKunai = Animator.StringToHash("throwKunai");
        fromBow = Animator.StringToHash("fromBow");
        fromKatana = Animator.StringToHash("fromKatana");
        katanaFromWallClimb = Animator.StringToHash("katanaFromWallClimb");

        isEnabeled = true;
        isRotated = false;
        lockLayer = false;
    }

    private void Update()
    {
        if (myAnim.GetBool(isClimbing) && isEnabeled && !lockLayer)
        {
            DisableLayer();

            if (myAnim.GetBool(hasBow)) {
                myAnim.SetBool(wallClimbFromBow, true);
            } else
            {
                myAnim.SetBool(wallClimbFromKatana, true);
            }

            lockLayer = true;
        } else if (!myAnim.GetBool(isClimbing) && !isEnabeled && !myAnim.GetBool(isDashing) && !lockLayer)
        {
            myAnim.SetBool(wallClimbFromBow, false);
            myAnim.SetBool(wallClimbFromKatana, false);

            EnableLayer();
            lockLayer = true;
        }

        if (!myAnim.GetBool(pullBack) && transform.localEulerAngles.y != 0)
        {
            UnRotate();
        }

        if (myAnim.GetBool(pullBack) && !isRotated)
        {
            if (transform.localEulerAngles.y < 70)
            {
                transform.Rotate(0, Time.deltaTime * 225, 0);
            } else
            {
                transform.localEulerAngles = new Vector3(0, 70, 0);
                isRotated = true;
            }
        }
    }

    public void UnRotate()
    {
        StartCoroutine(UndoRotation());
    }

    private IEnumerator UndoRotation()
    {
        do
        {
            if (transform.localEulerAngles.y > 10)
            {
                transform.Rotate(0, -Time.deltaTime * 135, 0);
            }
            else
            {
                transform.localEulerAngles = new Vector3(0, 0, 0);
                isRotated = false;
            }

            yield return null;
        } while (isRotated);
    }

    public void HideKatana()
    {
        if (myAnim.GetBool(fromKatana))
            katana.SetActive(false);
    }

    public void HideArrow()
    {
        if (myAnim.GetBool(fromBow))
            arrow.SetActive(false);
    }

    public void StopSmokeThrow()
    {
        myAnim.SetBool(throwSmoke, false);
    }

    public void StopKunaiThrow()
    {
        myAnim.SetBool(throwKunai, false);
    }

    public void KatanaSwitchComplete()
    {
        myAnim.SetBool(switchToKatana, false);
        myAnim.SetBool(switchToBow, false);
        myAnim.SetBool(fromKatana, true);
        myAnim.SetBool(fromBow, false);
    }

    public void HasKatana()
    {
        myAnim.SetBool(switchToBow, false);
        myAnim.SetBool(hasKatana, true);
        myAnim.SetBool(hasBow, false);
    }

    public void BowSwitchComplete()
    {
        myAnim.SetBool(switchToBow, false);

        if (!myAnim.GetBool(isClimbing))
        {
            myAnim.SetBool(fromBow, true);
            myAnim.SetBool(fromKatana, false);
        }
    }

    public void HasBow()
    {
        myAnim.SetBool(hasBow, true);
        myAnim.SetBool(hasKatana, false);
    }

    public void ResetAttackType()
    {
        myAnim.SetInteger(attackType, 2);
    }

    public void ResetKunai()
    {
        myAnim.SetBool(throwKunai, false);
    }

    public void DashIsStarted()
    {
        myAnim.SetBool(isDashing, true);
        myAnim.SetBool(doDash, false);

        DisableLayer();
    }

    public void IsNotDashing()
    {
        myAnim.SetBool(isDashing, false);
    }

    public void JumpIsStarted()
    {
        myAnim.SetBool(doJump, false);
    }

    public void DisableLayer()
    {
        StopAllCoroutines();
        StartCoroutine(SmoothWeightBlend());
    }

    public void EnableLayer()
    {
        StopAllCoroutines();
        StartCoroutine(SmoothWeightBlend());
    }

    private IEnumerator SmoothWeightBlend()
    {
        float weightValue;
        byte switchComplete = 0;

        if (!isEnabeled)
        {
            weightValue = 0;
            do
            {
                weightValue += Time.deltaTime / blendTime;

                if (weightValue > 0.5 && switchComplete == 0)
                {
                    SwitchMidBlend(ref switchComplete);
                }

                myAnim.SetLayerWeight(1, Mathf.Clamp(weightValue, 0, 1));

                yield return null;
            } while (myAnim.GetLayerWeight(1) != 1);

            lockLayer = false;
            isEnabeled = true;
        } else
        {
            if (myAnim.GetBool(isDashing))
            {
                myAnim.SetLayerWeight(1, 0);
            } else
            {
                weightValue = 1;
                do
                {
                    weightValue -= Time.deltaTime / (blendTime / 4);

                    myAnim.SetLayerWeight(1, Mathf.Clamp(weightValue, 0, 1));

                    yield return null;
                } while (myAnim.GetLayerWeight(1) != 0);
            }

            lockLayer = false;
            isEnabeled = false;
        }
    }

    private void SwitchMidBlend(ref byte switchComplete)
    {
        switchComplete = 1;

        if (myAnim.GetBool(hasBow))
        {
            myAnim.SetBool(switchToBow, true);
        }
        else if (myAnim.GetBool(hasKatana))
        {
            myAnim.SetBool(switchToKatana, true);
        }
    }


    // Audio Stuff
    protected void PlaySound(AudioClip clip)
    {
        sayaSource.PlayOneShot(clip);
    }

    protected void PlaySoundTp(AudioClip clip)
    {
        if (!ninjaArms.activeSelf)
            sayaSource.PlayOneShot(clip);
    }

    protected void PlayStepsSound()
    {
        sayaSource.PlayOneShot(footSteps[UnityEngine.Random.Range(0, footSteps.Length)], 0.1f);
    }


}
