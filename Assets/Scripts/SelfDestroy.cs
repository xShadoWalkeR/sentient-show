﻿using UnityEngine;
using Photon.Pun;

/// <summary>
/// This script is used on objects that need to self destruct
/// </summary>
public class SelfDestroy : MonoBehaviourPunCallbacks {

    // Self explanatory...
    [SerializeField] private float timeToDestroy;

    /// <summary>
    /// Runs at the start
    /// </summary>
    void Start() {

        // Start a timer to destroy the object
        Invoke("DestroySelf", timeToDestroy);
    }

    /// <summary>
    /// Destroys the object in all network instances
    /// </summary>
    private void DestroySelf() {

        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }
}
