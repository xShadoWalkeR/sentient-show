﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// This one is for when the player GETS HIT
public class HitManager : MonoBehaviour {

    [SerializeField] private float fadeDuration = 1.0f;

    [SerializeField] private Image hitImg = null;

    public Transform OtherPlayer { get; set; }

    private float currFadeTime;

    private Transform myCamera;

    // Only gets set once
    private Vector3 playerPos;

    void Start() {

        playerPos = OtherPlayer.position;
        myCamera = Camera.main.transform;

        StartCoroutine(FadeImage());
    }

    void Update() {

        Rotate();
    }

    // Rotates this UI object on 'z' according to the attacker's position (when shot)
    private void Rotate() {

        playerPos.y = 0.0f;
        Vector3 camPos = myCamera.position;
        camPos.y = 0.0f;
        Vector3 playerDir = (playerPos - camPos).normalized;

        Vector3 camForward = myCamera.forward;
        camForward.y = 0;

        float angle = Vector3.Angle(camForward, playerDir);

        Vector3 cross = Vector3.Cross(camForward, playerDir);

        if (cross.y > 0) {

            angle = -angle;
        }

        Quaternion rot = Quaternion.Euler(0, 0, angle);
        transform.localRotation = rot;
    }

    // Fade Image according to duration
    private IEnumerator FadeImage() {

        while (currFadeTime < fadeDuration) {

            currFadeTime += Time.deltaTime;

            hitImg.CrossFadeAlpha(0.0f, fadeDuration, false);

            yield return null;
        }

        // Destroy when duration is surpassed
        Destroy(gameObject);
    }
}
