﻿using UnityEngine.Rendering;
using System.Collections;
using UnityEngine;
using Photon.Pun;
using Controller;

public class SystemOverload : Ability {

    [SerializeField] public override float AbilityCooldown { get; } = 150f;   // The ultimate total cooldown

    private float abilityDuration = 8.0f;

    private float dmgMultiplier = 2.0f;

    private Animator scope;
    private ParticleSystem electricBolts;


    private Renderer[] models;

    // Support variable so we can only chage it's values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // Variables for all Damage Type Abilities and Weapons
    private WeaponHandler sniper;
    private ShotgunBarrel shotgun1;
    private ShotgunBarrel shotgun2;
    private KineticTraps traps;
    private ShockTherapy shock;

    private Color startVisorColor;
    private Color alternateVisorColor;

    private AudioSource ultiSource;

    private void Awake() {

        scope = transform.GetChild(transform.childCount - 4).GetChild(0).GetComponent<Animator>();

        // Get the last child!
        electricBolts = transform.GetChild(transform.childCount - 1).GetComponent<ParticleSystem>();

        // Get all models
        models = gameObject.GetComponent<SniperController>().Models;

        // Get ulti audio source
        ultiSource = GetComponent<SniperController>().UltiSource;

        // Get S.U.R.G.E.'s visor color
        startVisorColor = models[5].materials[1].GetColor("_EmissColor");
        alternateVisorColor = startVisorColor;

        alternateVisorColor.r = 255;
        alternateVisorColor.b = 0;
    }

    void Start() {

        SearchForDamageComponents(gameObject);

        // Starts without a cooldown to show on the presentation!
        currentCooldown = AbilityCooldown;
        //isOnCooldown = true;
    }

    // Searches for all damage based components (in all children)
    private void SearchForDamageComponents(GameObject obj) {

        // If no object was passed, leave the method
        if (obj == null) {

            return;
        }

        if (sniper == null) obj.TryGetComponent(out sniper);
        if (shotgun1 == null) obj.TryGetComponent(out shotgun1);
        if (shotgun2 == null || 
            shotgun2 == shotgun1) obj.TryGetComponent(out shotgun2);
        if (traps == null) obj.TryGetComponent(out traps);
        if (shock == null) obj.TryGetComponent(out shock);

        // Iterate through all children
        foreach (Transform child in obj.transform) {

            // If no child was found go to the next iteration
            if (child == null) {

                continue;
            }

            // The method calls itself with the new child
            SearchForDamageComponents(child.gameObject);
        }
    }

    private void Update() {

        if (!photonView.IsMine) return;

        // If our ability is on cooldown
        if (IsOnCooldown) {

            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0) {

                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }
    }

    public override IEnumerator Cast() {

        if (isOnCooldown) yield break;

        isOnCooldown = true;

        IncreaseDamage(true);

        ultiSource.Play();

        // Play/Activate all vfx for 3rd and 1st person
        photonView.RPC("Electrify", RpcTarget.All, true);

        // Enable vignette
        scope.SetBool(AnimationTags.OVERLOAD, true);

        yield return new WaitForSeconds(abilityDuration);

        IncreaseDamage(false);

        photonView.RPC("Electrify", RpcTarget.All, false);

        // Disable vignette
        scope.SetBool(AnimationTags.OVERLOAD, false);
    }

    /// <summary>
    /// Change all damage based Components damage by a multiplier
    /// </summary>
    /// <param name="increase">True to increase damage</param>
    private void IncreaseDamage(bool increase) {

        if (sniper != null) sniper.ChangeDamage(increase, dmgMultiplier);
        if (shotgun1 != null) shotgun1.ChangeDamage(increase, dmgMultiplier);
        if (shotgun2 != null) shotgun2.ChangeDamage(increase, dmgMultiplier);
        if (traps != null) traps.ChangeDamage(increase, dmgMultiplier);
        if (shock != null) shock.ChangeDamage(increase, dmgMultiplier);
    }

    /// <summary>
    /// Makes all models appear electrified
    /// </summary>
    /// <param name="electrify">True to show electrified</param>
    [PunRPC]
    private void Electrify(bool electrify) {

        if (electrify) {

            // Electricity Particle System
            electricBolts.Play();

            // Set new visor color
            models[5].materials[1].SetColor("_EmissColor", alternateVisorColor);

            // Activate electricity on all materials
            foreach (Renderer r in models) {

                r.materials[0].SetInt("_Electrified", 1);
            }

        } else {

            // Set original visor color
            models[5].materials[1].SetColor("_EmissColor", startVisorColor);

            // Deactivate electricity on all materials
            foreach (Renderer r in models) {

                r.materials[0].SetInt("_Electrified", 0);
            }
        }
    }
}
