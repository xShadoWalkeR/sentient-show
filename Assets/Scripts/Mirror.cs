﻿using UnityEngine;

[ExecuteInEditMode]
public class Mirror : MonoBehaviour {

    [SerializeField] private bool mirror = false;

    [SerializeField] private Transform origObject = null;
    [SerializeField] private Transform objectToMirror = null;

    [SerializeField] private Vector3 mirrorPlane = new Vector3(-1.0f, 1.0f, 1.0f);

    void Update() {

        if (mirror) {

            mirror = false;

            if (origObject != null || objectToMirror != null) {

                objectToMirror.localRotation = new Quaternion(origObject.localRotation.x * mirrorPlane.x,
                                            origObject.localRotation.y * mirrorPlane.y,
                                            origObject.localRotation.z * mirrorPlane.z,
                                            origObject.localRotation.w * -1.0f);

            } else {

                print("Make sure you have two objects!");
            }
        }
    }
}
