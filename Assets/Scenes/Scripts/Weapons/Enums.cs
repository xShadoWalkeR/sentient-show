﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enums
{
    /// <summary>
    /// Enumerator for the type of aim the wepon has
    /// </summary>
    public enum WeaponAim
    {
        NONE,
        SELF_AIM, // For the Bow and Arrow
        AIM // For rifles
    }

    /// <summary>
    /// Enumerator for the type of fire the wepon has
    /// </summary>
    public enum WeaponFireType
    {
        SINGLE,
        MULTIPLE,
        AKIMBO
    }

    /// <summary>
    /// Enumerator for the type of bullet in the wepon
    /// </summary>
    public enum WeaponBulletType
    {
        BULLET,
        PELLET,
        ARROW,
        NONE
    }

    /// <summary>
    /// Struct to give support to the StatusEffectType Enum
    /// </summary>
    public struct StatusEffects
    {
        private readonly StatusEffectType effectType;
        private readonly float value;
        private readonly float? time;

        public StatusEffects(StatusEffectType effectType, float value, float? time)
            : this()
        {
            this.effectType = effectType;
            this.value = value;
            this.time = time;
        }

        public StatusEffectType EffectType { get => effectType; }
        public float Value { get => value; }
        public float? Time { get => time; }
    }

    /// <summary>
    /// Enumerator for the type of Effect affecting the player
    /// </summary>
    public enum StatusEffectType
    {
        NONE,
        SLOW,
        ROOT,
        STUN,
        HEAL,
        SHIELD,
        SPEED
    }

    /// <summary>
    /// Enumerator that determines the XP amounts
    /// </summary>
    public enum XPGainType 
    {
        ELIM,
        WIN,
        LOSS
    }
}
