﻿/// <summary>
/// Enumeratior for the possible winner
/// </summary>
public enum Winner
{
    NONE,
    DRAW,
    TEAM_ONE,
    TEAM_TWO
}
