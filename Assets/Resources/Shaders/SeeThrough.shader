﻿Shader "Unlit/SeeThrough"
{
    Properties
    {
		_MainColor ("MainColor", Color) = (0,0,0,0)
		_SecondaryColor("SecondColor", Color) = (0,0,0,0)
		_MinDistance("MinDistance", float) = 1
		_MaxDistance("MaxDistance", float) = 10
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        LOD 100

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Back
			ZWrite Off

			ZTest Always // Greater

			CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

			struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
				float4 worldPos : TEXCOORD2;
            };

			float4 _MainColor;
			float4 _SecondaryColor;
			float _MinDistance;
			float _MaxDistance;
			float4 _DistanceColor;

			v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float dist = distance(_WorldSpaceCameraPos, i.worldPos);

				if (dist <= _MinDistance)
				{
					_DistanceColor = _MainColor;
				}

				if (dist > _MinDistance && dist < _MaxDistance) 
				{
					half weight = (dist - _MinDistance) / (_MaxDistance - _MinDistance);
					_DistanceColor = lerp(_MainColor, _SecondaryColor, weight);
				}

				if (dist >= _MaxDistance) 
				{
					_DistanceColor = _SecondaryColor;
				}

                // Return a color
                return _DistanceColor;
            }

			ENDCG
		}
    }
}
