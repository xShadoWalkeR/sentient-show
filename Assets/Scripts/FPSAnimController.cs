﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSAnimController : MonoBehaviour
{
    public Animator bowAnim;

    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            anim.SetInteger("ItemStateIndex", 1);
            anim.SetInteger("ItemSubstateIndex", 1);
            bowAnim.SetInteger("ItemStateIndex", 1);
            bowAnim.SetInteger("ItemSubstateIndex", 1);

        } else if (Input.GetMouseButtonUp(1))
        {
            anim.SetInteger("ItemStateIndex", 0);
            anim.SetInteger("ItemSubstateIndex", 0);
            bowAnim.SetInteger("ItemStateIndex", 0);
            bowAnim.SetInteger("ItemSubstateIndex", 0);
        }

        if (Input.GetMouseButtonDown(0)) {
            anim.SetTrigger("ItemStateIndexChange");
            anim.SetInteger("ItemStateIndex", 2);
            anim.SetInteger("ItemSubstateIndex", 2);
            bowAnim.SetTrigger("ItemStateIndexChange");
            bowAnim.SetInteger("ItemStateIndex", 2);
            bowAnim.SetInteger("ItemSubstateIndex", 2);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            anim.SetInteger("ItemSubstateIndex", 12);
            bowAnim.SetInteger("ItemSubstateIndex", 12);
        }
    }

    public void Reload()
    {
        anim.SetInteger("ItemSubstateIndex", 3);
        bowAnim.SetInteger("ItemSubstateIndex", 3);
    }
    public void ReloadEnd()
    {
        anim.SetInteger("ItemStateIndex", 0);
        anim.SetInteger("ItemSubstateIndex", 0);
        bowAnim.SetInteger("ItemStateIndex", 0);
        bowAnim.SetInteger("ItemSubstateIndex", 0);
    }
}
