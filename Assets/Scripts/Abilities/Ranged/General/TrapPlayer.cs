﻿using UnityEngine;
using Photon.Pun;
using Enums;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class TrapPlayer : MonoBehaviourPunCallbacks {

    // The damage dealt by the trap
    public int TrapDamage { get; set; }

    // The trap's life span and root duration
    [SerializeField] private float trapDuration = 2.0f;

    // The Material that allows seeing through walls
    [SerializeField] private Material seeThrough = null;

    [SerializeField] private float glitchChance = 0.1f;
    [SerializeField] private float glitchWait = 0.1f;

    [SerializeField] private float alphaSpeed = 1.0f;

    // Verifies if a player was already trapped
    private bool activated;

    // The trap's root effect
    private StatusEffectType root = StatusEffectType.ROOT;

    // The reference to the ability that spawned this object
    private KineticTraps kT;

    private Renderer currentRenderer = null;

    private Transform healthPack;
    private Transform hologram;

    private Renderer holoFountain;
    private Renderer firstHolo;

    private int childNumber;

    private Vector3 startpos;
    private bool movingUp;

    private Renderer hPRend;
    private Renderer holoRenderer;
    private WaitForSeconds glitchLoopWait;
    private WaitForSeconds glitchLength = new WaitForSeconds(0.1f);

    private IEnumerator Start() {

        SelectHealthPack();

        glitchLoopWait = new WaitForSeconds(glitchWait);

        float alpha = holoFountain.material.GetFloat("_AlphaCut");

        while (alpha < 0.0f) {

            alpha += Time.deltaTime * alphaSpeed;

            holoFountain.material.SetFloat("_AlphaCut", alpha);
            firstHolo.material.SetFloat("_AlphaCut", alpha);

            yield return null;
        }

        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);

        holoFountain.gameObject.SetActive(false);
        firstHolo.transform.parent.gameObject.SetActive(false);

        while (true) {

            float glitchTest = Random.Range(0.0f, 1.0f);

            if (glitchTest <= glitchChance) {

                StartCoroutine(Glitch());
            }

            yield return glitchLoopWait;
        }
    }

    private void Update() {

        LevitateHealthPack();
    }

    /// <summary>
    /// Choose either a big or small hp
    /// </summary>
    private void SelectHealthPack() {

        if (photonView.IsMine) {

            int rnd = Random.Range(0, 2);

            photonView.RPC("RPC_AssignHealthPack", RpcTarget.All, rnd);
        }
    }

    /// <summary>
    /// Levitates the holograms
    /// </summary>
    private void LevitateHealthPack() {

        // SAME BEHAVIOUR AS HEALTHPACKS!!!

        if (healthPack != null) {

            if (childNumber == 0) {
                healthPack.Rotate(0, 0, -(Time.deltaTime * 35));
            } else {
                healthPack.Rotate(0, Time.deltaTime * 35, 0);
            }

            if (Vector3.Distance(healthPack.localPosition, startpos) < 0.005f) {
                movingUp = true;
            } else if (Vector3.Distance(healthPack.localPosition, startpos) > (Vector3.up / 8).y) {
                movingUp = false;
            }

            if (movingUp) {
                healthPack.localPosition = Vector3.Lerp(healthPack.localPosition, healthPack.localPosition + (Vector3.up / 8), 0.001f);
            } else {
                healthPack.localPosition = Vector3.Lerp(healthPack.localPosition, healthPack.localPosition - (Vector3.up / 8), 0.001f);
            }
        }
    }

    private IEnumerator Glitch() {

        glitchLength = new WaitForSeconds(Random.Range(0.05f, 0.2f));

        holoRenderer.enabled = true;
        hPRend.enabled = false;

        holoRenderer.material.SetFloat("_Amplitude", Random.Range(20.0f, 50.0f));
        holoRenderer.material.SetFloat("_Speed", Random.Range(4.0f, 10.0f));

        yield return glitchLength;

        hPRend.enabled = true;
        holoRenderer.enabled = false;
    }

    /// <summary>
    /// Assigns the hologram to activate
    /// </summary>
    /// <param name="childNumber">The child number</param>
    [PunRPC]
    private void RPC_AssignHealthPack(int childNumber) {

        this.childNumber = childNumber;

        healthPack = transform.GetChild(1).GetChild(childNumber);
        hologram = healthPack.GetChild(0);

        holoFountain = transform.GetChild(2).GetComponent<Renderer>();
        firstHolo = transform.GetChild(3).GetChild(childNumber).GetComponent<Renderer>();

        hPRend = healthPack.GetComponent<Renderer>();
        holoRenderer = hologram.GetComponent<Renderer>();

        healthPack.gameObject.SetActive(true);

        startpos = healthPack.localPosition;
    }

    /// <summary>
    /// Fetches the ability that spawned this trap
    /// </summary>
    /// <param name="ability">The ability</param>
    [PunRPC]
    public void RPC_GetAbility(string name) {
        
        kT = GameObject.Find(name).GetComponent<KineticTraps>();
    }

    private void OnTriggerEnter(Collider other) {

        // If a player was trapped, return
        if (activated) return;

        // Check if the object that hit is a Player
        if (other.TryGetComponent(out Player player)) {

            // Verify if the Player isn't either an owner nor from the owner's team
            if (kT.photonView.ViewID != player.photonView.ViewID &&
                kT.GetComponent<Player>().MyTeam != player.MyTeam) {

                // Verify is the trap 'isMine'
                if (photonView.IsMine) {

                    // Destroys itself after a duration
                    Invoke("DestroyTrap", trapDuration);

                    //
                    // BELOW IS THE LOGIC TO SEE THE ENEMY THROUGH WALLS
                    //

                    // Fetch the enemy's Transform and Animator
                    Transform enemyTransform = player.transform;
                    Animator enemyAnim = enemyTransform.GetComponentInChildren<Animator>();

                    // Creates and empty object that will be the enemy's silhouette
                    Transform silhouette = new GameObject("Silhouette").transform;
                    silhouette.position = enemyTransform.position;

                    // Add an Animator to the silhouette 
                    // and assign the same values as the one from the enemy
                    Animator anim = silhouette.gameObject.AddComponent<Animator>();
                    anim.runtimeAnimatorController = enemyAnim.runtimeAnimatorController;
                    anim.avatar = enemyAnim.avatar;

                    GetChildRecursive(player.transform.GetChild(1).GetChild(0).gameObject, silhouette);

                } else {

                    // Applies the Root effect to the enemy
                    player.RPC_ApplyStatusEffect(root, 0.0f, trapDuration);

                    // Deals damage to the enemy
                    player.RPC_TakeDamage(TrapDamage, PhotonNetwork.NickName);
                }
            }
        }
    }

    /// <summary>
    /// Passes the photonView ID to the ability 
    /// in order to remove this trap from the Dictionary
    /// </summary>
    private void DestroyTrap() {

        kT.RemoveTrap(photonView.ViewID);
    }

    /// <summary>
    /// Recursevly gets all the childs of this object
    /// </summary>
    /// <param name="obj"></param>
    private void GetChildRecursive(GameObject obj, Transform silhouette) {

        if (obj == null || !obj.activeSelf)
            return;

        // For each child this has 
        foreach (Transform child in obj.transform)
        {

            // If it's null, skip
            if (child == null || !child.gameObject.activeSelf || child.name == "Holder")
                continue;

            // If this object has a renderer
            if (child.TryGetComponent(out currentRenderer))
            {

                if (child.TryGetComponent(out ParticleSystem _)) continue;

                // Create a copy of each child
                GameObject g = Instantiate(child, silhouette, true).gameObject;

                // Change all materials from a GameObject
                Material[] mats = new Material[g.GetComponent<Renderer>().materials.Length];

                for (int i = 0; i < mats.Length; i++)
                {

                    mats[i] = seeThrough;
                }

                g.GetComponent<Renderer>().materials = mats;

                g.AddComponent<AlphaController>().Duration = trapDuration;
            }

            // Get all the childs of the current object
            GetChildRecursive(child.gameObject, silhouette);
        }
    }
}
