﻿using System.Collections;
using Photon.Pun;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Controller;
using Cinemachine;
using Enums;

/// <summary>
/// Handles all the player shooting weapon independent
/// </summary>
public class PlayerShoot : MonoBehaviourPunCallbacks
{
    // Scope game object
    [SerializeField] private Animator scope;
    // Starting position to shoot the arrow
    [SerializeField] private Transform arrowStartPosition;
    // The layer mask for the raycast shots
    [SerializeField] private LayerMask layerMask;
    // The virtual camera for 1st Person
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    // An alternate first person animation
    [SerializeField] private Animator alternateFirstPersonAnim;
    // Where the shooting trails spawn
    [SerializeField] private Transform[] trailsSpawn;

    // Our main camera
    private Camera mainCam;

    // How long till we can fir the next shoot
    private float nextTimeToFire;
    // The current time in order to shoot
    private float currentTimeToFire;
    // Reload timer
    private float reloadTimer;
    // If we're currently Aiming
    private bool isAiming;
    // If we can shoot
    private bool canShoot;

    // Time to charge weapon
    private readonly float timeToCharge = 0.33f;
    // Current charge time
    private float currentCharge;
    // Current Scope Charge
    private float scopeCharge;
    // The coroutine that controls scoping in/out
    private Coroutine zoomCoroutine;

    // CycleOffSet for the Bow and Arrow
    private float cycleOffSet;
    // Damage multiplyer for the Bow andArrow
    private float damageMultiplier;

    // Create a new event to be invoked when i shoot
    public UnityEngine.Events.UnityEvent playerShoot;

    // Our Camera Controller
    private CameraController cameraController;

    // Our Controller Component
    private MainController controller;

    // Our Player Component
    private Player player;

    // Our Weapon Manager
    private WeaponManager weaponManager;

    // Current wepon we're holding
    private WeaponHandler currentWeapon;
    public WeaponHandler CurrentWeapon { get => currentWeapon; }

    // Our projectile (In case we need it)
    private GameObject projectile;

    // The instance of our hitmarker
    private HitMarkerBehaviour hitMarker;

    // The fov of the main camera at the start
    private float baseFov;

    /// <summary>
    /// Awake method called on load
    /// </summary>
    void Awake()
    {
        // Get the Camera Controller
        cameraController = GetComponent<CameraController>();

        // Get the Controller component
        controller = GetComponent<MainController>();

        // Get the WeaponManager Component
        weaponManager = GetComponent<WeaponManager>();

        // Get the Player Component
        player = GetComponent<Player>();
    }

    /// <summary>
    /// Start method called at the start
    /// </summary>
    private void Start()
    {
        if (photonView.IsMine)
        {
            mainCam = Camera.main;
            if (virtualCamera) baseFov = virtualCamera.m_Lens.FieldOfView;

            if (scope) scope.gameObject.SetActive(true);
        }

        hitMarker = HitMarkerBehaviour.Instance;

        canShoot = true;
        reloadTimer = 0;
        cycleOffSet = 0;
    }

    /// <summary>
    /// Update method called once per frame
    /// </summary>
    void Update()
    {
        // If this doesn't belong to us, return
        if (!photonView.IsMine) return;

        // If we're casting an ability, return
        // Currently only used for Dual Casting
        if (player.IsCasting) return;

        // Update what weapon we have selected
        currentWeapon = weaponManager.GetCurrentSelectedWeapon();

        // Call the ZoomInAndOut method
        ZoomInAndOut();

        // Call the WeaponShoot method
        WeaponShoot();

        // Reload();
        WeaponReload();
    }

    private void WeaponReload()
    {
        // LOGIC FOR AKIMBO WEAPON RELOADS
        if (currentWeapon.FireType == WeaponFireType.AKIMBO) {

            if (canShoot) {

                // Get 'R' key press
                if (Input.GetKeyDown(KeyCode.R)) {

                    // Check if player can reload
                    // And reload if it can
                    StartCoroutine(ReloadAkimbo(0));

                // Get Current (Total) Akimbo Ammo
                } else if (AkimboNoAmmo()) {

                    // Reload with delay
                    canShoot = false;
                    StartCoroutine(ReloadAkimbo(0.5f));
                }
            }
            
            return;
        }
        
        if (!canShoot && currentWeapon.BulletType != WeaponBulletType.NONE)
        {
            reloadTimer += Time.deltaTime;

            if (reloadTimer >= currentWeapon.ReloadSpeed)
            {
                reloadTimer = 0;
                canShoot = true;
                currentWeapon.CanShoot(canShoot);
            }
        }
    }

    /// <summary>
    /// Checks what type of weapon we have and when we can shoot it
    /// </summary>
    void WeaponShoot()
    {
        // If we have an Automatic Rifle
        if (currentWeapon.FireType == WeaponFireType.MULTIPLE)
        {

            // If we press and hold left mouse click AND
            // And if Time is greater than the nextTimeToFire
            if (Input.GetMouseButton(0) && Time.time > nextTimeToFire)
            {
                // Here we use Time.time so we don't need to use and exterior counter
                // Time.time is the time that has passed since the start of the game

                // Calculates how long it takes till we can fire again
                nextTimeToFire = Time.time + (1f / currentWeapon.FireRate);

                // Play the shooting animation for the weapon
                currentWeapon.ShootAnimation();

                // Shoot the weapon
                Shoot();
            }
        } // If our weapon is single shot
        else if (currentWeapon.FireType == WeaponFireType.SINGLE) {

            if (Input.GetMouseButtonDown(0))
            {

                if (currentWeapon.BulletType == WeaponBulletType.BULLET && isAiming)
                {
                    // Play the shooting animation for the weapon
                    currentWeapon.ShootAnimation();

                    // Shoot the weapon
                    Shoot();
                }
                else if (currentWeapon.BulletType == WeaponBulletType.PELLET)
                {
                    // Play the shooting animation for the weapon
                    currentWeapon.ShootAnimation();

                    // Shoot the weapon
                    ShotGunShoot();
                }
            }

            if (canShoot && currentWeapon.BulletType == WeaponBulletType.ARROW
                && Input.GetMouseButtonUp(0))
            {
                // Invoke the event
                playerShoot.Invoke();

                // Instanciate a new arrow to shoot
                projectile = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Arrow"), arrowStartPosition.position, arrowStartPosition.rotation);

                // Set the settings for the projectile
                projectile.GetComponent<Projectile>().OverrideProjectileSettings(damageMultiplier);

                // Set the projectile's owner
                projectile.GetComponent<Projectile>().Owner = GetComponent<Player>();

                // Call the Shoot method from the BowAndArrow
                projectile.GetComponent<Projectile>().Loose(mainCam.transform.forward);

                // Delay the shooting
                canShoot = false;

                // Restart the damage multiplier
                damageMultiplier = 0;
            }
        }// If our weapon is akimbo (dual wield)
        else if (currentWeapon.FireType == WeaponFireType.AKIMBO) 
        {
            // Increase the time to fire so we can shoot again
            currentTimeToFire += Time.deltaTime;

            // If we're pressing the specified button
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) {

                // Verify the bulley type of the current weapon
                if (currentWeapon.BulletType == WeaponBulletType.PELLET && canShoot) {

                    // Try to switch to the other akimbo weapon
                    if (SwitchWeapon()) {

                        // Play character related animation based on which weapon is shooting
                        if (currentWeapon == weaponManager.Weapons[1]) {

                            player.TpAnim.SetBool(AnimationTags.SHOOT_RIGHT, true);

                        } else {

                            player.TpAnim.SetBool(AnimationTags.SHOOT_LEFT, true);
                        }

                        // Play the shooting animation for the weapon
                        currentWeapon.ShootAnimation();

                        // Decrease its ammo
                        currentWeapon.Ammo--;

                        // Shoot the weapon
                        ShotGunShoot();
                    }
                }

            } else if (Input.GetMouseButtonUp(0)) {

                // Reset the Shooting animation's trigger
                // (If it's mid-animation)
                currentWeapon.Anim.ResetTrigger("Shoot");
            }
        }
    }

    /// <summary>
    /// Plays an animation for aiming downsights or for Drawing the Bow.
    /// Depending on wich weapon you're holding.
    /// </summary>
    void ZoomInAndOut()
    {
        switch(currentWeapon.WeaponAim)
        {
            case WeaponAim.AIM:

                if (!canShoot) return;

                // if we press the right mouse button
                if (Input.GetMouseButtonDown(1))
                {
                    // Play scoping animation
                    currentWeapon.Zoom(1.0f, 0.85f);

                    // Have our character aiming
                    player.TpAnim.SetBool(AnimationTags.ZOOM, true);

                    // Enables the scope
                    if (zoomCoroutine != null) StopCoroutine(zoomCoroutine);
                    zoomCoroutine = StartCoroutine(BlendZoom(1.0f, 0.85f));

                    // Let the script know we're aiming
                    isAiming = true;

                    // Lower our sensitivity
                    cameraController.XSense = cameraController.BaseXSense * 0.6f;
                    cameraController.YSense = cameraController.BaseYSense * 0.6f;

                    // Slow our player down by 60%
                    controller.MoveSpeed -= controller.BaseMoveSpeed * 0.4f;

                    // Deactivate shotguns (ONLY RENDERERS IN THE FUTURE)
                    alternateFirstPersonAnim.SetBool(AnimationTags.AIM_PARAMETER, true);
                }

                // if we hold the right mouse button
                if (Input.GetMouseButton(1) && isAiming) {

                    currentCharge += Time.deltaTime;
                    // Zoom Camera In
                    virtualCamera.m_Lens.FieldOfView -= Time.deltaTime * 200;
                    if (virtualCamera.m_Lens.FieldOfView < baseFov / 3)
                        virtualCamera.m_Lens.FieldOfView = baseFov / 3;

                    if (currentCharge >= timeToCharge) {

                        // 0 to 1 in 0.75s
                        scopeCharge += Time.deltaTime / 0.75f;
                        scopeCharge = Mathf.Clamp01(scopeCharge);

                        scope.SetFloat(AnimationTags.CHARGE, scopeCharge);

                        // Increase the weapon's damage over time (when scoped)
                        currentWeapon.Damage += currentWeapon.ChargeRate * Time.deltaTime;   // 12 to 120 in 0.75s
                        currentWeapon.Damage = Mathf.Clamp(currentWeapon.Damage, 0, currentWeapon.MaxDamage);

                        //if (currentWeapon.Damage == 120) print((int)currentWeapon.Damage);
                    }
                }

                // if we release the right mouse button
                if (Input.GetMouseButtonUp(1) && isAiming)
                {
                    // Reactivate shotguns (ONLY RENDERERS IN THE FUTURE)

                    alternateFirstPersonAnim.SetBool(AnimationTags.AIM_PARAMETER, false);

                    // Play unscoping animation
                    currentWeapon.Zoom(0.0f, 0.85f);

                    // Have our character stop aiming
                    player.TpAnim.SetBool(AnimationTags.ZOOM, false);

                    // Disables the scope
                    if (zoomCoroutine != null) StopCoroutine(zoomCoroutine);
                    zoomCoroutine = StartCoroutine(BlendZoom(0.0f, 5f));

                    // Let the script know we're aiming no more
                    isAiming = false;

                    // Zoom Camera Out
                    StartCoroutine(Unscope());

                    // Reset our sensitivity
                    cameraController.XSense = cameraController.BaseXSense;
                    cameraController.YSense = cameraController.BaseYSense;

                    // Reset our Move Speed
                    controller.MoveSpeed += controller.BaseMoveSpeed * 0.4f;

                    // Reset the weapon's damage
                    currentWeapon.Damage = currentWeapon.BaseDamage;

                    // Reset the current charge time
                    currentCharge = 0.0f;

                    // Reset the UI animation
                    scopeCharge = 0.0f;
                    scope.SetFloat(AnimationTags.CHARGE, 0.0f);

                    // Reset the current time to fire
                    currentTimeToFire = 100.0f;

                    // Switch to Akimbo weapons (CHANGE THIS LATER)
                    if (weaponManager.PreviousWeaponIndex == 1) {

                        weaponManager.SetCurrentWeapon(1);

                    } else {

                        weaponManager.SetCurrentWeapon(2);
                    }
                }
                break;
            case WeaponAim.SELF_AIM:
                // If we're pressing down the MouseButton 0
                if (Input.GetMouseButtonDown(0))
                {
                    // Set the isAiming to true
                    isAiming = true;

                    // Slow our player down by 60%
                    controller.MoveSpeed -= controller.BaseMoveSpeed * 0.4f;

                    cycleOffSet = 0;

                    // Set the aiming to true so we can ads (aim down sights)
                    currentWeapon.Aim(isAiming);
                }

                if (Input.GetMouseButton(0))
                {
                    if (cycleOffSet <= 0.7f)
                    {
                        cycleOffSet += Time.deltaTime;
                        damageMultiplier += Time.deltaTime;

                        // Clamp the value to 0.75f
                        if (damageMultiplier > 1f)
                            damageMultiplier = 1f;
                    }
                }

                // If we released the MouseButton 0
                if (Input.GetMouseButtonUp(0))
                {
                    // Set the isAiming to false
                    isAiming = false;

                    // Remove the slow
                    controller.MoveSpeed += controller.BaseMoveSpeed * 0.4f;

                    // Offset for the release animation
                    cycleOffSet = 0.8f;

                    // Set the aiming to false so we stop ads (aim down sights)
                    currentWeapon.Aim(isAiming);
                }
                break;
        }
    }

    /// <summary>
    /// Raycast forward to check if the shot hit something
    /// </summary>
    private void Shoot()
    {
        // Invoke the event
        playerShoot.Invoke();

        cameraController.Recoil = -700;

        // Declare a new Raycasthit
        RaycastHit hit;

        // If we hit something
        if (Physics.Raycast(mainCam.transform.position, mainCam.transform.forward, out hit, 500, layerMask))
        {
            // Spawn a trail
            photonView.RPC("SpawnTrail", RpcTarget.All, true, mainCam.transform.position, Vector3.zero, hit.point);

            // Create a bullet on the hit point of the raycast
            GameObject bullet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Pellet"), hit.point, Quaternion.identity);
            bullet.transform.up = hit.point + hit.normal;

            // If what we hit was an enemy
            if (hit.transform.tag == Tags.ENEMY_TAG)
            {

                // Check for headshots
                if (Vector3.Distance(hit.point, hit.transform.position) > 1.55f) {

                    // Call a headshot hit marker
                    hitMarker.Hit(true);

                    // Call an RPC Method to deal damage to that player
                    hit.transform.GetComponent<Player>().photonView.RPC("RPC_TakeDamage", RpcTarget.All, (int)currentWeapon.Damage * 2, gameObject.name);
                }
                else {

                    // Call a regular hit marker
                    hitMarker.Hit(false);

                    // Call an RPC Method to deal damage to that player
                    hit.transform.GetComponent<Player>().photonView.RPC("RPC_TakeDamage", RpcTarget.All, (int)currentWeapon.Damage, gameObject.name);
                }
            }

        } else {

            // Spawn a trail
            photonView.RPC("SpawnTrail", RpcTarget.All, false, mainCam.transform.position, mainCam.transform.forward, Vector3.zero);
        }

        // Reset the weapon's damage
        currentWeapon.Damage = currentWeapon.BaseDamage;

        // Reset the current charge time
        currentCharge = 0.0f;

        // Reset the UI animation
        scopeCharge = 0.0f;
        scope.SetFloat(AnimationTags.CHARGE, 0.0f);
    }

    /// <summary>
    /// Increases FOV to simulate unscoping
    /// </summary>
    /// <returns></returns>
    private IEnumerator Unscope() {

        while (virtualCamera.m_Lens.FieldOfView < baseFov) {

            // Zoom Camera Out
            virtualCamera.m_Lens.FieldOfView += Time.deltaTime * 400.0f;

            yield return null;
        }

        virtualCamera.m_Lens.FieldOfView = baseFov;
    }

    /// <summary>
    /// Reload regular weapons
    /// </summary>
    /// <returns>Waits for reload time</returns>
    private IEnumerator Reload() {

        canShoot = false;

        currentWeapon.Reload(true);

        yield return new WaitForSeconds(currentWeapon.ReloadSpeed);

        currentWeapon.Ammo = currentWeapon.BaseAmmo;

        canShoot = true;
    }

    /// <summary>
    /// Check if both akimbo weapons have no ammo
    /// </summary>
    /// <returns>True if both have no ammo</returns>
    private bool AkimboNoAmmo() {

        if (weaponManager.Weapons[1].Ammo == 0 &&
            weaponManager.Weapons[2].Ammo == 0) {

            return true;
        }

        return false;
    }

    /// <summary>
    /// Reloads akimbo weapons
    /// </summary>
    /// <param name="delay">Reload delay</param>
    /// <returns></returns>
    private IEnumerator ReloadAkimbo(float delay) {

        // Delay, so if both akimbo weapons' ammo drops to zero
        // they won't start reloading immediately
        yield return new WaitForSeconds(delay);

        // Count of akimbos to reload
        int akimboToReload = 0;

        // Search which akimbos need reloading
        for (int i = 0; i < weaponManager.Weapons.Length; i++) {

            if (weaponManager.Weapons[i].FireType == WeaponFireType.AKIMBO &&
                weaponManager.Weapons[i].Ammo < weaponManager.Weapons[i].BaseAmmo) {

                // Reload an akimbo and increase the previous count
                weaponManager.Weapons[i].Reload(true);
                akimboToReload++;
            }
        }

        // If no akimbos need reloading, leave the method
        if (akimboToReload == 0) yield break;

        // Player can't shoot anymore
        canShoot = false;

        // Wait for animation to end (linked to the reload speed)
        yield return new WaitForSeconds(weaponManager.Weapons[1].ReloadSpeed);

        // Reset the reload bool on the animators
        weaponManager.Weapons[1].Reload(false);
        weaponManager.Weapons[2].Reload(false);

        // Search all akimbos
        for (int i = 0; i < weaponManager.Weapons.Length; i++) {

            if (weaponManager.Weapons[i].FireType == WeaponFireType.AKIMBO &&
                weaponManager.Weapons[i].Ammo < weaponManager.Weapons[i].BaseAmmo) {

                // Reset their ammo
                weaponManager.Weapons[i].Ammo = weaponManager.Weapons[i].BaseAmmo;
            }
        }

        // Set the first akimbo as the current ammo
        weaponManager.SetCurrentWeapon(1);

        // Player can shoot now
        canShoot = true;
    }

    /// <summary>
    /// Switches between akimbo (dual wielded) weapons
    /// </summary>
    /// <returns>True if enough time has passed</returns>
    private bool SwitchWeapon()
    {
        // If enough time has passed
        if (currentTimeToFire >= currentWeapon.FireRate) {

            // Search for the other Akimbo weapon
            for (int i = 0; i < weaponManager.Weapons.Length; i++) {

                if (weaponManager.Weapons[i].FireType == WeaponFireType.AKIMBO &&
                    weaponManager.Weapons[i] != currentWeapon) {

                    // Set the weapon found as the current one
                    weaponManager.SetCurrentWeapon(i);

                    // Reset the timer
                    currentTimeToFire = 0.0f;

                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Logic for shooting pellet based weapons
    /// </summary>
    private void ShotGunShoot() {

        // Invoke the event
        playerShoot.Invoke();

        // Get the current shotgun barrel
        ShotgunBarrel barrel = currentWeapon.GetComponent<ShotgunBarrel>();

        // Create an array for the pellets origins 
        // with the size of the number of pellets
        Vector3[] pelletsOrigin = new Vector3[barrel.Pellets];

        // Stores the Raycast's hit info
        RaycastHit hit;

        // Iterates through the number of pellets
        for (int i = 0; i < pelletsOrigin.Length; i++) {

            // Assign the current pellet's position to a random
            pelletsOrigin[i] = barrel.GetRandomPoint();

            // Throw a raycast from the camera towards the Vector 
            // based on the pellet's position and the back of the barrel
            if (Physics.Raycast(mainCam.transform.position, 
                pelletsOrigin[i] - barrel.RaycastOrigin.position, out hit, barrel.MaxRange, layerMask)) {

                // Create a pellet on the hit point of the raycast
                GameObject pellet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Pellet"), hit.point, Quaternion.identity);
                pellet.transform.up = hit.point + hit.normal;

                // If what we hit was an enemy
                if (hit.transform.CompareTag(Tags.ENEMY_TAG)) {

                    // Calculate its damage based on the distance
                    int pelletDamage = barrel.CalculateFaloffDamage(hit.distance);

                    // Check for headshots
                    if (Vector3.Distance(hit.point, hit.transform.position) > 1.55f) {

                        // Call a headshot hit marker
                        hitMarker.Hit(true);

                        // Call an RPC Method to deal damage to that player
                        hit.transform.GetComponent<Player>().photonView.RPC("RPC_TakeDamage", RpcTarget.All, pelletDamage * 2, gameObject.name);

                    } else {

                        // Call a regular hit marker
                        hitMarker.Hit(false);

                        // Call an RPC Method to deal damage to that player
                        hit.transform.GetComponent<Player>().photonView.RPC("RPC_TakeDamage", RpcTarget.All, pelletDamage, gameObject.name);
                    }
                }
            }
        }
    }

    // Spawn trails for shooting guns
    [PunRPC]
    private void SpawnTrail(bool hit, Vector3 camPos, Vector3 camForward, Vector3 hitPos) {

        LineRenderer bulletTrace = Instantiate(Resources.Load<GameObject>("LocalPrefabs/SniperTrail"), Vector3.zero, Quaternion.identity).GetComponent<LineRenderer>();

        bulletTrace.SetPosition(0, trailsSpawn[photonView.IsMine ? 0 : 1].position);
        bulletTrace.SetPosition(1, hit ? hitPos : camPos + camForward * 500);
    }

    // Blends zooming in/out
    private IEnumerator BlendZoom(float targetBlend, float animTime) {

        while (Mathf.FloorToInt(scope.GetFloat(AnimationTags.ZOOM)) != 1) {

            scope.SetFloat(AnimationTags.ZOOM, targetBlend, 0.1f, animTime * Time.deltaTime);

            yield return null;
        }
    }
}
