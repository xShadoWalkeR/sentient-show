﻿using System.Collections;
using System.IO;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

/// <summary>
/// The ninja smoke bomb ability
/// </summary>
public class NinjaSmokeBomb : Ability
{
    // Support variable so we can only chage it's values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    private bool startCooldown;

    // Smoke Bomb total cooldown
    [SerializeField] public override float AbilityCooldown { get; } = 10f;

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // CloakingBehaviour
    private CloakingBehaviour cloaking;
    public bool IsCloaked { get => cloaking.IsCloaked; }

    private Player player;

    private WaitForSeconds smokeDelay;

    // My Instantiation Event Code
    private byte instantiationEventCode;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        instantiationEventCode = (byte)photonView.OwnerActorNr;

        player = GetComponent<Player>();

        smokeDelay = new WaitForSeconds(0.75f);

        // CloakingBehaviour
        cloaking = gameObject.AddComponent<CloakingBehaviour>();

        // Setting the cloaking duration to 0 will mean we have permenent invisibility (Untill we shoot)
        cloaking.CloakingDuration = 0f;

        // Set the ability current cooldown
        currentCooldown = AbilityCooldown;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (!photonView.IsMine)
            return;

        // If our ability is on cooldown
        if (startCooldown && !IsCloaked)
        {
            isOnCooldown = true;

            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0)
            {
                // Stop cooldown
                startCooldown = false;

                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }
    }

    /// <summary>
    /// Casts the Ability
    /// </summary>
    /// <returns>Waits for a certain ammount of time</returns>
    public override IEnumerator Cast()
    {
        player.TpAnim.SetBool(AnimationTags.THROW_SMOKE, true);
        player.FpAnim.SetBool(AnimationTags.THROW_SMOKE, true);

        // Set the start cooldown to true
        startCooldown = true;

        Vector3 previousPosition = transform.position;

        yield return smokeDelay;

        // Instanciate the Smoke Bomb particle effects
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "SmokeBomb"),
            previousPosition + (transform.forward / 2), Quaternion.Euler(-90, 0, 0));

        // Raise a Cloaking event to cloak all the player instances across the network
        PhotonNetwork.RaiseEvent(instantiationEventCode, null,
            new RaiseEventOptions()
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            }, SendOptions.SendReliable);

        // Waits for the frame to be rendered before continuing
        yield return null;
    }
}
