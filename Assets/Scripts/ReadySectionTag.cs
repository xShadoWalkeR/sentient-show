﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadySectionTag : MonoBehaviour
{
    public bool Display { get; set; }

    [SerializeField] private Sprite sayaSprite;
    [SerializeField] private Sprite surgeSprite;

    public bool CanUpdate { get; set; }

    private ReadyBoxUpdater[] boxUpdaters;
}
