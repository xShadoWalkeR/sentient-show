﻿using UnityEngine;

public class AlphaReducer : MonoBehaviour {

    [SerializeField] private float duration;

    // The current time (for lerping the alpha)
    private float currentTime;

    private LineRenderer line;

    void Start() {

        line = GetComponent<LineRenderer>();
    }

    void Update() {

        line.material.SetFloat("_Alpha", 0.4f - GetCurrentTime());

        if (line.material.GetFloat("_Alpha") <= 0) {

            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Updates the alpha 'time' percentage
    /// </summary>
    /// <returns>The percentage (0 - 1)</returns>
    private float GetCurrentTime() {

        // Increases the current time
        currentTime += Time.deltaTime;

        // Returns said percentage
        return currentTime / duration;
    }

    private void OnDisable() {

        line.material.SetFloat("_Alpha", 1);
    }
}
