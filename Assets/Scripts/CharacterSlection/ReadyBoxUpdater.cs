﻿using TMPro;
using UnityEngine.UI;
using UnityEngine;
using Photon.Pun;

public class ReadyBoxUpdater : MonoBehaviourPunCallbacks, IPunObservable
{
    private bool ready;
    public bool IsReady {
        get => ready;
        set {
            ready = value;
            isReady.SetActive(value);
            picking.SetActive(!value);
        }
    }
    private string nickname;
    public string NickName {
        get => nickname;
        set {
            nickname = value;
            nickName.text = nickname;
        }
    }
    private string championname;
    public string ChampionName {
        get => championname;
        set {
            championname = value;
            championName.text = value;
        }
    }
    private Sprite championsprite;
    public Sprite ChampionSprite {
        get => championsprite;
        set {
            championsprite = value;
            championSprite.sprite = value;
        }
    }

    [SerializeField] private TextMeshProUGUI nickName;
    [SerializeField] private TextMeshProUGUI championName;
    [SerializeField] private Image championSprite;
    [SerializeField] private GameObject isReady;
    [SerializeField] private GameObject picking;

    [Header("Sprites")]
    [SerializeField] private Sprite sayaSprite;
    [SerializeField] private Sprite surgeSprite;

    public int MyTeam { get; set; }

    private PlayerInfo myInfo;
    private ReadySectionTag readySection;

    private void Awake()
    {
        transform.SetParent(Resources.FindObjectsOfTypeAll<ReadySectionTag>()[0].transform, false);
        readySection = GetComponentInParent<ReadySectionTag>();
    }

    private void Start()
    {
        NickName = PhotonNetwork.NickName;
        if (photonView.IsMine)
        {
            myInfo = GameObject.FindGameObjectWithTag("Canvas").GetComponent<PlayerInfo>();
        }
    }

    // This should disable the object so it only apears team members!
    private void Update()
    {
        if (photonView.IsMine)
        {
            if (readySection.CanUpdate)
            {
                switch (myInfo.MySelectedCharacter)
                {
                    case "Ninja":
                        ChampionName = "SAYA";
                        break;
                    case "Sniper":
                        ChampionName = "S.U.R.G.E.";
                        break;
                }
            }

            IsReady = transform.GetComponentInParent<ReadySectionTag>().Display;
        }

        if (readySection.CanUpdate)
        {
            switch (ChampionName)
            {
                case "SAYA":
                    ChampionSprite = sayaSprite;
                    break;
                case "S.U.R.G.E.":
                    ChampionSprite = surgeSprite;
                    break;
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(MyTeam);
            stream.SendNext(ready);
            stream.SendNext(nickname);
            stream.SendNext(ChampionName);
        }
        else if (stream.IsReading)
        {
            MyTeam = (int)stream.ReceiveNext();
            IsReady = (bool)stream.ReceiveNext();
            NickName = (string)stream.ReceiveNext();
            ChampionName = (string)stream.ReceiveNext();
        }
    }
}
