﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Controller;
using Enums;
using System;
using Random = UnityEngine.Random;

/// <summary>
/// The main player script
/// </summary>
public class Player : MonoBehaviourPunCallbacks, IPunObservable
{
    // Player's max health
    [SerializeField] private int maxHealth;
    public int MaxHealth { get => maxHealth; }  // Player's max health

    // Player's Ultimate ability
    [SerializeField] private string ultimateAbility;
    public Ability[] Abilities { get; private set; } // An array of all our abilities

    [Header("Animators")]

    // Player's Animator
    [SerializeField] private Animator fpAnim;
    public Animator FpAnim { get => fpAnim; }

    // Player's Animator
    [SerializeField] private Animator tpAnim;
    public Animator TpAnim { get => tpAnim; }

    // Our current healt
    private int currentHealth;
    public int CurrentHealth { get => currentHealth; }  // Our current healt

    // Our current shield
    private int currentShield;
    public int CurrentShield { get => CurrentShield; }  // Our current shield

    // This players team
    private int? myTeam;
    public int? MyTeam { get => myTeam; }

    // This player's name
    private string myName;
    public string MyName { get => myName; }

    // If the player is dead
    public bool IsDead { get; set; }

    // If we can cast abilities
    public bool CanCast { get; set; }

    // If we are casting an ability
    public bool IsCasting { get; set; }

    // The player's controller
    private MainController myController;

    // The last player that damaged me
    private Player lastDamaged;

    // The player stats
    private PlayerStats playerStats;

    // Shouldn't do this, but there's no other way :/
    public UiManager UI { get; set; }

    // The AccountManager script
    private AccountManager am;

    // Bools for animations
    private bool isSlowed;
    private bool isStunned;
    private bool isRooted;

    // List with all the status effects affecting the player
    public List<StatusEffects> statusEffects { get; private set; }

    private void Awake()
    {
        // Initialize the status effect Collection
        statusEffects = new List<StatusEffects>();
    }

    private void Start()
    {
        // Initialize the canCast variable
        CanCast = true;

        // Start as our selfs for suicide
        lastDamaged = this;

        // Get my controller script
        myController = GetComponent<MainController>();

        // Get the player stats
        playerStats = GetComponent<PlayerStats>();

        // Get the AccountManager script reference
        am = AccountManager.am;

        // Set the current healt
        currentHealth = maxHealth;

        // Set the current shield
        currentShield = 0;

        // Set our that to be player or enemy
        tag = photonView.IsMine ? Tags.PLAYER_TAG : Tags.TO_BE_SET;
    }

    /// <summary>
    /// Set's the player's starting abilities using reflection
    /// </summary>
    /// <param name="abilities">Array with the abilities names</param>
    [PunRPC]
    public void RPC_SetAbilities(string[] abilities)
    {
        Abilities = new Ability[3];

        Abilities[0] = (Ability)gameObject.AddComponent(Type.GetType(ultimateAbility));

        for (int i = 0; i < abilities.Length; i++)
        {
            Abilities[i + 1] = (Ability)gameObject.AddComponent(Type.GetType(abilities[i]));
        }
    }

    /// <summary>
    /// Set the player's team
    /// </summary>
    /// <param name="team">The team</param>
    [PunRPC]
    public void RPC_SetMyTeam(int team)
    {
        myTeam = team;

        if (photonView.IsMine)
        {
            Invoke("SetMainTeam", 1f);
        }
    }

    private void SetMainTeam()
    {
        KillsManager.km.SetMainTeam(myTeam.Value);
    }

    /// <summary>
    /// Sets the player's name
    /// </summary>
    /// <param name="name">The name for the player</param>
    [PunRPC]
    public void RPC_SetMyName(string name)
    {
        myName = name;
        gameObject.name = name;
    }

    /// <summary>
    /// Called once every frame
    /// </summary>
    private void Update()
    {
        // If our health goes bellow zero we die
        if (currentHealth <= 0)
        {
            currentHealth = 0; // So no negative numbers appear on the screen
            Die();
        }

        if (!photonView.IsMine)
            return;

        if (CanCast && Abilities != null)
        {
            // Checks if we're trying to cast the first ability
            CheckAbilityCast(Tags.FIRST_ABILITY, Abilities[1]);

            // Checks if we're trying to cast the second ability
            CheckAbilityCast(Tags.SECOND_ABILITY, Abilities[2]);

            // Checks if we're trying to cast the second ability
            CheckAbilityCast(Tags.ULTIMATE_ABILITY, Abilities[0]);
        }

        UpdateStatusEffects();
        UpdateAnimations();
    }
    
    /// <summary>
    /// Check if the ability we pass is being casted
    /// </summary>
    /// <param name="button">What button needs to be pressed</param>
    /// <param name="castAbility">What ability we are checking</param>
    private void CheckAbilityCast(string button, Ability castAbility)
    {
        if (!castAbility.IsDualCast) {

            // If we're pressing the button for our 1st ability and if that ability isn't on cooldown
            if (Input.GetButton(button) && !castAbility.IsOnCooldown) {
                // We check if the ability is not a charging ability and if the button was pressed down
                // We do this because `GetButton` is true while we keep the button pressed
                // And `GetButtonDown` is only true at the moment the button is pressed down
                // So we don't just keep recasting the ability
                if (Input.GetButtonDown(button) && !castAbility.IsCharged) {
                    // If true we start the `Cast()` Coroutine
                    StartCoroutine(castAbility.Cast());
                }
                else {
                    // If not that means it's a charging ability so we call the ChargeAbility method
                    // This method will keep being called each update as long as we keep pressing the button
                    castAbility.ChargeAbility();
                }
            }
            else if (Input.GetButtonUp(button) && castAbility.IsCharged && !castAbility.IsOnCooldown) {
                // If we stopped pressing the button reset the values for the next charge
                castAbility.ResetCharge();
            }

        } else {

            if (Input.GetButton(button) && !castAbility.IsOnCooldown) {

                // This enters here in 1 frame because the coroutine below skips one frame...
                IsCasting = true;

                if (castAbility.FirstCast() && Input.GetMouseButtonDown(0)) {

                    StartCoroutine(castAbility.Cast());
                }

            } else if (Input.GetButtonUp(button) && !castAbility.IsOnCooldown) {

                castAbility.DisableEffects();

                IsCasting = false;
            }
        }
    }

    /// <summary>
    /// Increases XP "on script"
    /// </summary>
    /// <param name="xpType">The type of XP</param>
    [PunRPC]
    private void RPC_GainXP(XPGainType xpType) {

        am.GainXP(xpType);
    }

    /// <summary>
    /// Updates the kills stats
    /// </summary>
    [PunRPC]
    private void RPC_UpdateKillStats()
    {
        if (photonView.IsMine)
        {
            playerStats.IncrementKills(this);
        }
    }

    /// <summary>
    /// Updates the deaths stats
    /// </summary>
    [PunRPC]
    private void RPC_UpdateDeathStats()
    {
        if (photonView.IsMine)
        {
            playerStats.IncrementDeaths(this);
        }
    }

    /// <summary>
    /// Deals damage to this player
    /// </summary>
    /// <param name="_amount">The ammount of damage</param>
    [PunRPC]
    public void RPC_TakeDamage(int _amount, string player)
    {
        if (!photonView.IsMine) return;

        // If we're dead we can't take damage
        if (IsDead)
            return;

        lastDamaged = GameObject.Find(player).GetComponent<Player>();

        UI.CreateHit(lastDamaged);

        // If we have no shield
        if (currentShield <= 0)
        {
            // Decrease the health by the amount of damage taken
            currentHealth = Mathf.Clamp(currentHealth - _amount, 0, currentHealth);

        } else // If we do have shield
        {
            // Decrease the health by the amount of damage taken
            currentHealth -= Mathf.Clamp(TookShieldDamage(_amount), 0, currentHealth);
        }
        
        // Print to console our current health
        print(transform.name + " now has " + currentHealth + "health.");
    }

    /// <summary>
    /// Apply a status effect to our player
    /// </summary>
    /// <param name="effectType">The type of effect</param>
    /// <param name="value">The value for said effect</param>
    /// <param name="time">How long the effect lasts</param>
    [PunRPC]
    public void RPC_ApplyStatusEffect(StatusEffectType effectType, float value, float? time = null)
    {
        switch (effectType)
        {
            case StatusEffectType.HEAL:
                // If we're dead we can't take damage
                if (!photonView.IsMine || IsDead) return;

                // Heal the player clamping the value to not go over the maxHp
                currentHealth = Mathf.Clamp(currentHealth + (int)value, 0, maxHealth);
                break;
            case StatusEffectType.ROOT:
                // Root the player
                myController.EnableLocomotion = false;
                CanCast = false;
                isRooted = true;
                break;
            case StatusEffectType.STUN:
                // Lock all player movement
                GetComponent<CameraController>().enabled = false;
                myController.EnableLocomotion = false;
                CanCast = false;
                isStunned = false;
                break;
            case StatusEffectType.SLOW:
                // Slow the player
                myController.MoveSpeed -= myController.BaseMoveSpeed * (1 - value);
                isSlowed = true;
                break;
            case StatusEffectType.SPEED:
                // Increase the movement speed
                myController.MoveSpeed += myController.BaseMoveSpeed * value;
                break;
        }

        if (effectType != StatusEffectType.HEAL)
        {
            statusEffects.Add(new StatusEffects(effectType, value, time));
        }
    }

    /// <summary>
    /// Update All the statusEffects on our player
    /// </summary>
    private void UpdateStatusEffects()
    {
        // Acumulate all the shields we have to set it as our current shield value
        int shieldValue = 0;

        // Go through all the StatusEffects on the Player
        for (int i = 0; i < statusEffects.Count; i++)
        {
            // First we update variables for the animations
            switch (statusEffects[i].EffectType)
            {
                case StatusEffectType.SLOW:
                    isSlowed = true;
                    break;
                case StatusEffectType.STUN:
                    isStunned = true;
                    break;
                case StatusEffectType.ROOT:
                    isRooted = true;
                    break;
            }

            // If the Effect has run out of time
            if ((statusEffects[i].Time ?? 1) <= 0)
            {
                // Check what effect it was
                switch (statusEffects[i].EffectType)
                {
                    case StatusEffectType.ROOT:
                        // Remove Root
                        myController.EnableLocomotion = true;
                        CanCast = true;
                        isRooted = false;
                        break;
                    case StatusEffectType.STUN:
                        // Removes the stun
                        GetComponent<CameraController>().enabled = true;
                        myController.EnableLocomotion = true;
                        CanCast = true;
                        isStunned = false;
                        break;
                    case StatusEffectType.SLOW:
                        // Remove slow
                        myController.MoveSpeed += myController.BaseMoveSpeed * (1 - statusEffects[i].Value);
                        isSlowed = false;
                        break;
                    case StatusEffectType.SPEED:
                        // Remove the speed boost
                        myController.MoveSpeed -= myController.BaseMoveSpeed * statusEffects[i].Value;
                        break;
                }

                statusEffects.Remove(statusEffects[i]);

                if (i + 1 == statusEffects.Count)
                {
                    return;
                }
                else
                {
                    i--;
                    continue;
                }

            }
            else if (statusEffects[i].Time != null)
            {
                statusEffects[i] = new StatusEffects(
                statusEffects[i].EffectType,
                statusEffects[i].Value,
                statusEffects[i].Time - Time.deltaTime);
            }

            // Calculate the current total value of our shield
            if (statusEffects[i].EffectType == StatusEffectType.SHIELD)
            {
                shieldValue += (int)statusEffects[i].Value;
            }
        }

        currentShield = shieldValue;
    }

    /// <summary>
    /// Update the state of all animations referent to player debufs
    /// </summary>
    private void UpdateAnimations()
    {
        if (tpAnim != null)
        {
            tpAnim.SetBool(AnimationTags.IS_SLOWED, isSlowed);
            tpAnim.SetBool(AnimationTags.IS_ROOTED, isRooted);
            tpAnim.SetBool(AnimationTags.IS_STUNNED, isStunned);
        }
    }

    /// <summary>
    /// If we took any shield Damage run this
    /// </summary>
    /// <returns>The extra damage if the shield was destroyed</returns>
    private int TookShieldDamage(int _amount)
    {
        // Go through all the StatusEffects on the Player
        for (int i = 0; i < statusEffects.Count; i++)
        {
            // Check if the Effect is of Type Shield
            if (statusEffects[i].EffectType == StatusEffectType.SHIELD)
            {
                // If our shield can support all the damage
                if (statusEffects[i].Value - _amount > 0) {
                    // Decrease the ammout of shield it provides
                    statusEffects[i] = new StatusEffects(
                        statusEffects[i].EffectType,
                        statusEffects[i].Value - _amount,
                        statusEffects[i].Time);

                    // Returns with the value 0 cause we don't need to check anything else
                    return 0;
                } else if (statusEffects[i].Value - _amount == 0)
                {
                    // Removes the shield from the list
                    statusEffects.Remove(statusEffects[i]);

                    // Returns with the value 0 cause we don't need to check anything else
                    return 0;
                } else if (statusEffects[i].Value - _amount < 0)
                {
                    // Removes the shield from the list
                    statusEffects.Remove(statusEffects[i]);

                    // Reduce the amount of damage so other possible shields don't take damage
                    _amount = Mathf.Clamp(_amount - (int)statusEffects[i].Value, 0, int.MaxValue);
                }
            }
        }

        // Return the excess amout to damage the player Hp
        return _amount;
    }

    /// <summary>
    /// The player dies
    /// </summary>
    private void Die()
    {
        // Set IsDead to true
        IsDead = true;

        KillsManager.km.photonView.RPC("RPC_SetKill", RpcTarget.All, MyTeam);
        lastDamaged.photonView.RPC("RPC_UpdateKillStats", RpcTarget.All);
        photonView.RPC("RPC_UpdateDeathStats", RpcTarget.All);

        // Call our killer's 'GainXP'
        if (photonView.IsMine) photonView.RPC("RPC_GainXP", lastDamaged.photonView.Owner, XPGainType.ELIM);

        KillsManager.km.GetComponent<KillFeedManager>().photonView.RPC("AddToFeed", RpcTarget.All, lastDamaged.MyName, myName, lastDamaged.tag);

        // Disable the coolider when we die
        Collider _col = GetComponent<Collider>();
        if (_col != null)
        {
            _col.enabled = false;
        }

        MeshRenderer mesh = GetComponent<MeshRenderer>();
        if (mesh != null)
            mesh.enabled = false;

        // Reset our hp
        currentHealth = maxHealth;

        // Print the this player is dead on the console
        print(transform.name + " is dead!");

        if (photonView.IsMine)
        {
            Transform respawn = SpawnSystem.spawn.RespawnPlayer(this);
            transform.position = respawn.position;
            transform.rotation = respawn.rotation;
        }

        // Starts a Coroutine to respawn the player
        StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(1);

        // Set IsDead to true
        IsDead = false;

        MeshRenderer mesh = GetComponent<MeshRenderer>();
        if (mesh != null)
            mesh.enabled = true;

        // Enable the coolider when we die
        Collider _col = GetComponent<Collider>();
        if (_col != null)
        {
            _col.enabled = true;
        }

        print(transform.name + " respawned!");
    }

    /// <summary>
    /// Serializes and deserializes data to be sent over the network
    /// </summary>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(myTeam);
            stream.SendNext(myName);
            stream.SendNext(currentHealth);
            stream.SendNext(currentShield);
            stream.SendNext(statusEffects.Count);
            for (int i = 0; i < statusEffects.Count; i++) {

                stream.SendNext(statusEffects[i].EffectType);
                stream.SendNext(statusEffects[i].Value);
                stream.SendNext(statusEffects[i].Time);
            }
        } else if (stream.IsReading)
        {
            if (statusEffects != null) statusEffects.Clear();
            myTeam = (int)stream.ReceiveNext();
            gameObject.name = (string)stream.ReceiveNext();
            myName = gameObject.name;
            currentHealth = (int)stream.ReceiveNext();
            currentShield = (int)stream.ReceiveNext();
            int n = (int)stream.ReceiveNext();
            for (int i = 0; i < n; i++) {

                statusEffects.Add(
                    new StatusEffects(
                    (StatusEffectType)stream.ReceiveNext(), 
                    (float)stream.ReceiveNext(), 
                    (float?)stream.ReceiveNext()));
            }
        }
    }
}
