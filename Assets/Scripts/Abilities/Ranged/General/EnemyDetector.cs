﻿using System.Collections;
using UnityEngine;
using Photon.Pun;

public class EnemyDetector : MonoBehaviourPunCallbacks {

    [SerializeField] private string enemyTag = null;   // The tag to search for enemies

    [SerializeField] private Material seeThrough = null;   // The Material that allows seeing through walls

    [SerializeField] private float seeDuration = 5.0f;

    private string enemyName = "";   // The enemy name is saved so we don't check multiple 'OnTriggers'

    private Renderer currentRenderer = null;

    /// <summary>
    /// Resets the saved name [KEEP AN EYE ON THIS WHEN WE HAVE MORE THAN 1/2 PLAYERS]
    /// </summary>
    public void ResetEnemyName() {

        enemyName = "";
    }

    // Called by the 'ProximityScan' class through an RPC
    public void Scan(Vector3 desiredPosition, Vector3 minScale, Vector3 maxScale, float duration, float radiusSpeed) {

        photonView.RPC("RPC_LerpSphere", RpcTarget.All, desiredPosition, minScale, maxScale, duration, radiusSpeed);
    }

    // RPC method
    [PunRPC]
    private void RPC_LerpSphere(Vector3 desiredPosition, Vector3 minScale, Vector3 maxScale, float duration, float radiusSpeed) {

        // Acrivate the sphere
        gameObject.SetActive(true);

        // Start the Coroutine
        StartCoroutine(LerpSphere(desiredPosition, minScale, maxScale, duration, radiusSpeed));
    }

    // Positions, Lerps the scale and disables the sphere
    private IEnumerator LerpSphere(Vector3 desiredPosition, Vector3 minScale, Vector3 maxScale, float duration, float radiusSpeed) {

        // Change the position of the sphere
        transform.position = desiredPosition;

        // Apply the minimum scale to the sphere
        transform.localScale = minScale;

        // Local variables that keep track of our timer
        float perc = 0.0f;
        float rate = (1.0f / duration) * radiusSpeed;

        // While our percentage of time is smaller than 100%...
        while (perc < 1.0f) {

            // ...Keep adding it up and applying it to the Lerp method
            perc += Time.deltaTime * rate;
            transform.localScale = Vector3.Lerp(minScale, maxScale, perc);

            // Return null (Remember that this is an IEnumerator method 
            // running inside a coroutine, meaning that this 'while' won't run
            // on a single frame but rather on how many in needs!)
            yield return null;
        }

        // Deactive the sphere
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Detects collision with an enemy
    /// </summary>
    /// <param name="enemy">The enemy we are colliding with</param>
    private void OnTriggerEnter(Collider enemy) {

        if (!photonView.IsMine) return;

        // Verifies the enemy's tag and name
        if (enemy.CompareTag(enemyTag) && enemy.name != enemyName) {

            // Fetch the enemy's Transform and Animator
            Transform enemyTransform = enemy.transform;
            Animator enemyAnim = enemyTransform.GetComponentInChildren<Animator>();

            // Creates and empty object that will be the enemy's silhouette
            Transform silhouette = new GameObject("Silhouette").transform;
            silhouette.position = enemyTransform.position;

            // Add an Animator to the silhouette 
            // and assign the same values as the one from the enemy
            Animator anim = silhouette.gameObject.AddComponent<Animator>();
            anim.runtimeAnimatorController = enemyAnim.runtimeAnimatorController;
            anim.avatar = enemyAnim.avatar;

            GetChildRecursive(enemy.transform.GetChild(1).GetChild(0).gameObject, silhouette);
        }
    }

    /// <summary>
    /// Recursevly gets all the childs of this object
    /// </summary>
    /// <param name="obj"></param>
    private void GetChildRecursive(GameObject obj, Transform silhouette) {

        if (obj == null || !obj.activeSelf)
            return;

        // For each child this has 
        foreach (Transform child in obj.transform) {

            // If it's null, skip
            if (child == null || !child.gameObject.activeSelf || child.name == "Holder")
                continue;

            // If this object has a renderer
            if (child.TryGetComponent(out currentRenderer)) {

                if (child.TryGetComponent(out ParticleSystem _)) continue;

                // Create a copy of each child
                GameObject g = Instantiate(child, silhouette, true).gameObject;

                // Change all materials from a GameObject
                Material[] mats = new Material[g.GetComponent<Renderer>().materials.Length];

                for (int i = 0; i < mats.Length; i++) {

                    mats[i] = seeThrough;
                }

                g.GetComponent<Renderer>().materials = mats;

                g.AddComponent<AlphaController>().Duration = seeDuration;
            }

            // Get all the childs of the current object
            GetChildRecursive(child.gameObject, silhouette);
        }
    }
}
