﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Handles the player's katana
/// </summary>
public class KatanaHandler : MonoBehaviourPunCallbacks
{
    // The player instance
    [SerializeField] private Player player;

    // The katana game object
    [SerializeField] private GameObject katana;
    public GameObject Katana { get => katana; }

    // The katana in the holder game object
    [SerializeField] private GameObject katanaInHolder;
    public GameObject KatanaInHolder { get => katanaInHolder; }

    // Referece to our weapon manager
    [SerializeField] private WeaponManager myManager;

    // The player's animator
    private Animator myAnimator;
    public Animator MyAnimator { get => myAnimator; }

    // Our camera
    private Transform cam;

    // The katana's projectile 
    private GameObject projectile;

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        // Initialize the animator
        myAnimator = GetComponent<Animator>();

        // If this script is running on the local player
        if (photonView.IsMine)
        {
            // Set the camera
            cam = Camera.main.transform;
        }
    }

    /// <summary>
    /// Enebles the Katana
    /// </summary>
    public void EnableKatana()
    {
        if (photonView.IsMine)
            photonView.RPC("RPC_EnableKatana", RpcTarget.All);
    }

    /// <summary>
    /// RPC to enable the katana
    /// </summary>
    [PunRPC]
    private void RPC_EnableKatana()
    {
        if (!photonView.IsMine)
        {
            katanaInHolder.SetActive(false);
            katana.SetActive(true);
        }
        
        myManager.photonView.RPC("RPC_TurnOnSelectedWeapon", RpcTarget.All, 1);
    }

    /// <summary>
    /// Disables the Katana
    /// </summary>
    public void DisableKatana()
    {
        if (myAnimator.GetBool("isDashing") && myAnimator.GetBool("fromKatana")) return;

        if (photonView.IsMine)
            photonView.RPC("RPC_DisableKatana", RpcTarget.All);
    }

    /// <summary>
    /// RPC to disable the katana
    /// </summary>
    [PunRPC]
    private void RPC_DisableKatana()
    {
        if (!photonView.IsMine)
        {
            katanaInHolder.SetActive(true);
            katana.SetActive(false);
        }

        myManager.photonView.RPC("RPC_TurnOnSelectedWeapon", RpcTarget.All, 0);
    }

    /// <summary>
    /// Turns on the attack point for the katana
    /// </summary>
    public void TurnOnAttackPoint()
    {
        // Turn on the attackPoint
        myManager.Weapons[1].TurnOnAttackPoint();
    }

    /// <summary>
    /// Disables the attack point
    /// </summary>
    public void TurnOffAttackPoint()
    {
        // Turn off the attackPoint
        myManager.Weapons[1].TurnOffAttackPoint();
    }

    /// <summary>
    /// Throws the katana's projectile
    /// </summary>
    public void ThrowProjectile()
    {
        if (player.photonView.IsMine)
        {
            projectile = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "KatanaSlashProjectile"),
                cam.position + cam.forward + -(cam.up * 0.3f),
                cam.rotation * Quaternion.Euler(1f, 1f, Random.Range(1f, 360f)));

            Physics.IgnoreCollision(projectile.GetComponent<BoxCollider>(), 
                myManager.Weapons[1].AttackPoint.GetComponent<CapsuleCollider>());

            // Set the projectile's owner
            projectile.GetComponent<Projectile>().Owner = player;

            // Set the projectile loose
            projectile.GetComponent<Projectile>().Loose(cam.forward);
        }
    }
}
