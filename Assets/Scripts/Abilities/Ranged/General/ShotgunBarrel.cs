﻿using UnityEngine;
using Photon.Pun;

public class ShotgunBarrel : MonoBehaviourPunCallbacks {

    // The minimum amount of damage per pellet
    // Used for short distances
    [SerializeField] private float minDamage;

    // The maximum amount of damage per pellet
    // Used for long distances
    [SerializeField] private float maxDamage;

    // The pellets' minimum hit distance
    // Used only for calculating falloff damage
    [SerializeField] private float minRange;

    // The pellets' maximum hit distance
    [SerializeField] private float maxRange;
    public float MaxRange { get => maxRange; }

    // The number of pellets to shoot
    [SerializeField] private int pellets;
    public int Pellets { get => pellets; }

    // The point to assign to the raycast of a pellet
    [SerializeField] private Transform raycastOrigin;
    public Transform RaycastOrigin { get => raycastOrigin; }

    // The list of borders present on the barrel
    [SerializeField] private Transform[] borders;

    private Animator anim;

    private void Awake() {

        anim = GetComponent<Animator>();
    }

    // This is here because there's nowhere else to put it 
    // without creating a new script :/
    private void LateUpdate() {

        anim.SetFloat(AnimationTags.INPUT_X, Input.GetAxis("Mouse X"), 1.0f, 4.0f * Time.deltaTime);
        anim.SetFloat(AnimationTags.INPUT_Y, Input.GetAxis("Mouse Y"), 1.0f, 4.0f * Time.deltaTime);
    }

    /// <summary>
    /// Get a random Vector3 within the boundaries of the barrel
    /// </summary>
    /// <returns>The random Vector3</returns>
    public Vector3 GetRandomPoint() {

        // Create an empty Vector3
        Vector3 point;

        // Randomize its coordinates based on the (2) borders of the barrel
        point.x = Random.Range(borders[0].position.x, borders[1].position.x);
        point.y = Random.Range(borders[0].position.y, borders[1].position.y);
        point.z = Random.Range(borders[0].position.z, borders[1].position.z);

        // Return said Vector3
        return point;
    }

    /// <summary>
    /// Scales damage based on the distance to the target
    /// </summary>
    /// <param name="distance">The distance to the target</param>
    /// <returns>The scaled damage</returns>
    public int CalculateFaloffDamage(float distance) {

        // If the distance is too close
        if (distance <= minRange) {

            // Deal full damage
            return (int)maxDamage;

        // If the distance is too far
        } else if (distance >= maxRange) {

            // Deal minimal damage
            return (int)minDamage;

        // If the distance is intermediate
        } else {

            // Deal damage based on the function below
            return (int)((-2 * distance) / 5 + 7);
        }
    }

    /// <summary>
    /// Changes damage based on a multiplier
    /// </summary>
    /// <param name="increase">True if it will increase the damage</param>
    /// <param name="multiplier">The damage multiplier</param>
    public void ChangeDamage(bool increase, float multiplier) {

        if (increase) {

            minDamage *= multiplier;
            maxDamage *= multiplier;

        } else {

            minDamage /= multiplier;
            maxDamage /= multiplier;
        }
    }
}
