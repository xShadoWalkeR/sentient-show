/*
Texture2D tex,
SamplerState texSS, 
float2 uv,
float2 screenSize, 
float iterations, 
float kernel ,
out float4 color
*/

void GaussianBlur_float(Texture2D tex,SamplerState texSS, float4 uv,float2 screenSize, float iterations, float kernel ,out float4 color)
{

    //  color = tex.Sample(thisSS,uv);

    //distance from current pixel
    float d = 0.0;

    //current weight
    float cw = 0.0;

    float totalWeight = 0.0;

    float px = 1 / screenSize.x;
    float py = 1 / screenSize.y;

    float2 offset =  float2(0,0);

    color = float4(0,0,0,1);
    for (int x = -iterations; x <= iterations; x++) 
    {
        for (int y = -iterations; y <= iterations; y++) 
        {
            // color = float4(0,j,i,1);

            d = sqrt(pow(x,2) + pow(y,2));

            if (d == 0)
            {
                cw = 0;
            }
            else
            {
                cw = 1/d;
            }

            totalWeight += cw;
            offset = float2(x * px, y * py) * kernel;
            color += tex.Sample(texSS,float2(uv.x,uv.y) + offset) * cw;

        }
    }

    color /= totalWeight;
}


/*
void GaussianBlur_v2_float(
	Texture2D tex1, 
	SamplerState tex1SS,
	//Texture2D tex2, 
	//SamplerState tex2SS,
	//float4 objectUV,
	float4 ScreenUV, 
	float2 screenSize, 
	float iterations, 
	float kernel, 
	out float4 color)
{
	//tex1
	//the camera's texture

	//tex2
	//the texture that controlls the blur amount


	//distance from current pixel
	float d = 0.0;

	//current weight
	float cw = 0.0;

	float totalWeight = 0.0;

	float px = 1 / screenSize.x;
	float py = 1 / screenSize.y;

	float2 offset = float2(0, 0);

	//float4 t2 = tex2.Sample(tex2SS, float2(objectUV.x, objectUV.y));
	//iterations *= t2.r;
	//int i = iterations * (int)t2.r;
	

	color = float4(0, 0, 0, 1);

	for (int x = -iterations; x <= iterations; x++)
	//for (int x = -iterations; x <= iterations; x++)
	{
		for (int y = -iterations; y <= iterations; y++)
		//for (int y = -iterations; y <= iterations; y++)
		{
			// color = float4(0,j,i,1);

			d = sqrt(pow(x, 2) + pow(y, 2));

			if (d == 0)
			{
				cw = 0;
			}
			else
			{
				cw = 1 / d;
			}

			totalWeight += cw;
			offset = float2(x * px, y * py) * kernel;
			//offset = float2(x * px, y * py) * kernel * t2.r;
			color += tex1.Sample(tex1SS, float2(ScreenUV.x , ScreenUV.y ) + offset) * cw;

		}
	}

	//color *= t2;
	color /= totalWeight;
}
*/