﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HitMarkerBehaviour : MonoBehaviour {

    #region Singleton
    public static HitMarkerBehaviour Instance { get; private set; }

    private void Awake() {

        if (Instance != null && Instance != this) {

            Destroy(gameObject);

        } else {

            Instance = this;
        }
    }
    #endregion

    [SerializeField] private float duration;

    [SerializeField] private Color headShotColor = Color.red;

    [SerializeField] private Image marker;

    private IEnumerator coroutine;

    public void Hit(bool headShot) {

        if (headShot) {

            coroutine = EnableHitMarker(headShotColor, duration);

        } else {

            coroutine = EnableHitMarker(Color.white, duration);
        }

        if (marker.gameObject.activeSelf) {

            StopCoroutine(coroutine);
        }

        StartCoroutine(coroutine);
    }

    private IEnumerator EnableHitMarker(Color color, float waiTime) {

        marker.color = color;
        marker.gameObject.SetActive(true);

        yield return new WaitForSeconds(waiTime);

        marker.gameObject.SetActive(false);
        marker.color = Color.white;
    }
}
