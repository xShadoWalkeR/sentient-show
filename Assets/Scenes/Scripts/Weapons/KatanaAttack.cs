﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the short range katana attack
/// </summary>
public class KatanaAttack : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private int damage;
    [SerializeField] private string enemyTag;

    /// <summary>
    /// When a collision happens
    /// </summary>
    /// <param name="other">What we collided with</param>
    private void OnTriggerEnter(Collider other)
    {
        if (!player.photonView.IsMine) return;

        // If we collided with an enemy
        if (other.tag == enemyTag)
        {
            // Deal damage
            other.GetComponent<Player>().photonView.RPC("RPC_TakeDamage", RpcTarget.All, damage, player.name);
        }
    }
}