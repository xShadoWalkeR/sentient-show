﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomImg : MonoBehaviour
{
    [SerializeField] private Sprite[] availableSprites;

    private void Start()
    {
        GetComponent<Image>().sprite = availableSprites[Random.Range(0, availableSprites.Length)];
    }
}
