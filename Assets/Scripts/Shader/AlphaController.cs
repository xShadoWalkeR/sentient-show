﻿using UnityEngine;

public class AlphaController : MonoBehaviour {

    // The duration of the 'alpha lerp'
    public float Duration { get; set; } = 5.0f;

    // The materials present on a GameObject
    private Material[] mats;

    // The colors to be set on the shader
    private Color mainColor;
    private Color secondColor;

    // The current time (for lerping the alpha)
    private float currentTime;

    private void Start() {

        // Get all materials
        mats = GetComponent<Renderer>().materials;

        // Set the currentTime as 0
        currentTime = 0.0f;

        // Get both colors on the shader
        mainColor = GetComponent<Renderer>().material.GetColor("_MainColor");
        secondColor = GetComponent<Renderer>().material.GetColor("_SecondaryColor");
    }

    void Update() {

        // Update the alpha
        SetAlpha();
    }

    /// <summary>
    /// Updates the alpha value of all colors on the shader
    /// </summary>
    private void SetAlpha() {

        // Gets a value from (0 - 1) based on our timer
        float perc = GetCurrentTime();

        // Lerp the alpha of both colors (from 1 to 0)
        mainColor.a = Mathf.Lerp(1.0f, 0.0f, perc);
        secondColor.a = Mathf.Lerp(1.0f, 0.0f, perc);

        // Set the previous colors and the new ones on the shader
        SetMaterialsColor("_MainColor", mainColor);
        SetMaterialsColor("_SecondaryColor", secondColor);

        // If the timer 'runs out' destroy the whole object
        if (currentTime > Duration) {

            Destroy(transform.parent.gameObject);
        }
    }

    /// <summary>
    /// Updates the alpha 'time' percentage
    /// </summary>
    /// <returns>The percentage (0 - 1)</returns>
    private float GetCurrentTime() {

        // Increases the current time
        currentTime += Time.deltaTime;

        // Returns said percentage
        return currentTime / Duration;
    }

    /// <summary>
    /// Updates the color on all materials
    /// </summary>
    /// <param name="colorName">The color's name on the shader</param>
    /// <param name="newColor">The new color to apply</param>
    private void SetMaterialsColor(string colorName, Color newColor) {

        for (int i = 0; i < mats.Length; i++) {

            GetComponent<Renderer>().materials[i].SetColor(colorName, newColor);
        }
    }

    /// <summary>
    /// Reset the colors on the Material
    /// </summary>
    private void OnDisable() {

        // Resets the colors' alpha
        mainColor.a = 1.0f;
        secondColor.a = 1.0f;

        // Resets the color's... well, color
        GetComponent<Renderer>().material.SetColor("_MainColor", mainColor);
        GetComponent<Renderer>().material.SetColor("_SecondaryColor", secondColor);
    }
}
