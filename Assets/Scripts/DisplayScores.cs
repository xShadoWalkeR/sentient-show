﻿using UnityEngine;

/// <summary>
/// Displays or Removes the stats tab
/// </summary>
public class DisplayScores : MonoBehaviour
{
    // The stats tab objects
    [SerializeField] private GameObject displayScores;

    /// <summary>
    /// Called once every frame
    /// </summary>
    private void Update()
    {
        // Display the scores when Tab is pressed
        if (Input.GetKey(KeyCode.Tab))
        {
            displayScores.SetActive(true);
        } 
        else if (displayScores.activeSelf)
        {
            displayScores.SetActive(false);
        }
    }
}
