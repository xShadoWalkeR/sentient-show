﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmsBob : MonoBehaviour
{
    // Fix Player escaping camera bounds
    private string horizontal = "Horizontal";
    private string vertical = "Vertical";
    
    [SerializeField] private float smoothValue;
    [SerializeField] private float extrasSmooth;

    [Header("Base offset")]
    [SerializeField] private Vector3 baseOffset;

    [Header("Other offsets")]
    [SerializeField] private Vector3 jumpOffset;
    [SerializeField] private Vector3 fallOffset;
    [SerializeField] private Vector3 moveMaxPositive;
    [SerializeField] private Vector3 moveMaxNegative;
    [SerializeField] private Vector3 moveLeft;
    [SerializeField] private Vector3 moveRight;
    [SerializeField] private Vector3 moveForward;
    [SerializeField] private Vector3 moveBackwards;


    private Vector3 horizontalExtras;
    private Vector3 verticalExtras;
    private Vector3 baseOff;


    private float horizontalTime;
    private float verticalTime;

    private bool rightLock;
    private bool leftLock;
    private bool forwardLock;
    private bool backwardLock;
    private bool movingPositive;

    // Stuff for y velocity
    private float previousY;

    private void Start()
    {
        baseOff = baseOffset;
        horizontalExtras = Vector3.zero;
        verticalExtras = Vector3.zero;

        horizontalTime = 0;
        verticalTime = 0;

        previousY = transform.parent.transform.parent.position.y;

        movingPositive = true;
    }

    private void Update()
    {
        if (previousY < transform.parent.transform.parent.position.y)
        {
            baseOff = Mathfx.Hermite(baseOff, baseOff + jumpOffset, extrasSmooth);
        } else if (previousY > transform.parent.transform.parent.position.y)
        {
            baseOff = Mathfx.Hermite(baseOff, baseOff + fallOffset, extrasSmooth);
        } else
        {
            baseOff = Mathfx.Hermite(baseOff, baseOffset, extrasSmooth);
        }

        previousY = transform.parent.transform.parent.position.y;
    }

    private void LateUpdate()
    {
        if (Input.GetAxis(horizontal) != 0 || Input.GetAxis(vertical) != 0)
        {
            if (movingPositive)
            {
                Vector3 smoothedPos = Mathfx.Hermite(transform.localPosition, (baseOff + moveMaxPositive) + (horizontalExtras + verticalExtras), smoothValue);

                transform.localPosition = smoothedPos;

                if (Vector3.Distance(transform.localPosition, (baseOff + moveMaxPositive) + (horizontalExtras + verticalExtras)) < 0.005f)
                {
                    movingPositive = false;
                }
            } else
            {
                Vector3 smoothedPos = Mathfx.Hermite(transform.localPosition, (baseOff + moveMaxNegative) + (horizontalExtras + verticalExtras), smoothValue);
                transform.localPosition = smoothedPos;

                if (Vector3.Distance(transform.localPosition, (baseOff + moveMaxNegative) + (horizontalExtras + verticalExtras)) < 0.005f)
                {
                    movingPositive = true;
                }
            }
        } else
        {
            Vector3 smoothedPos = Mathfx.Hermite(transform.localPosition, baseOff, smoothValue);
            transform.localPosition = smoothedPos;

            horizontalExtras = Mathfx.Hermite(horizontalExtras, Vector3.zero, extrasSmooth);
            verticalExtras = Mathfx.Hermite(horizontalExtras, Vector3.zero, extrasSmooth);

            rightLock = false;
            leftLock = false;
            forwardLock = false;
            backwardLock = false;
            horizontalTime = 0;
            verticalTime = 0;
        }

        if (Input.GetAxis(horizontal) > 0 && !rightLock)
        {
            leftLock = false;
            horizontalExtras = Mathfx.Hermite(horizontalExtras, moveRight, extrasSmooth);

            horizontalTime += Time.deltaTime;

            if (horizontalTime > 0.2f)
            {
                rightLock = true;
                horizontalTime = 0;
            }
        } 
        else if (Input.GetAxis(horizontal) < 0 && !leftLock)
        {
            rightLock = false;
            horizontalExtras = Mathfx.Hermite(horizontalExtras, moveLeft, extrasSmooth);

            horizontalTime += Time.deltaTime;

            if (horizontalTime > 0.2f)
            {
                leftLock = true;
                horizontalTime = 0;
            }
        } else
        {
            horizontalExtras = Mathfx.Hermite(horizontalExtras, Vector3.zero, extrasSmooth);
        }

        if (Input.GetAxis(vertical) > 0 && !forwardLock)
        {
            backwardLock = false;
            horizontalExtras = Mathfx.Hermite(horizontalExtras, moveForward, extrasSmooth);

            verticalTime += Time.deltaTime;

            if (verticalTime > 0.2f)
            {
                forwardLock = true;
                verticalTime = 0;
            }
        }
        else if (Input.GetAxis(vertical) < 0 && !backwardLock)
        {
            forwardLock = false;
            horizontalExtras = Mathfx.Hermite(horizontalExtras, moveBackwards, extrasSmooth);

            verticalTime += Time.deltaTime;

            if (verticalTime > 0.2f)
            {
                backwardLock = true;
                verticalTime = 0;
            }
        } else
        {
            verticalExtras = Mathfx.Hermite(horizontalExtras, Vector3.zero, extrasSmooth);
        }

        // Fix bob offset going crazy
        if (transform.localPosition.y > -0.10f)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, -0.10f, transform.localPosition.z);

            baseOff = baseOffset;
            horizontalExtras = Vector3.zero;
            verticalExtras = Vector3.zero;
        }
        else if (transform.localPosition.y < -0.14f)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, -0.14f, transform.localPosition.z);

            baseOff = baseOffset;
            horizontalExtras = Vector3.zero;
            verticalExtras = Vector3.zero;
        }
    }
}
