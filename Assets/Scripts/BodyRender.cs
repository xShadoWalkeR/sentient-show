﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.Rendering;

public class BodyRender : MonoBehaviourPunCallbacks
{

    [SerializeField] private GameObject armsObject;

    [SerializeField] private SkinnedMeshRenderer bodyObject;
    [SerializeField] private SkinnedMeshRenderer hairObject;
    [SerializeField] private SkinnedMeshRenderer bowGeo;
    [SerializeField] private MeshRenderer arrowGeo;
    [SerializeField] private MeshRenderer katanaGeo;
    [SerializeField] private MeshRenderer holder;
    [SerializeField] private MeshRenderer hilt;

    private void Start()
    {
        if (photonView.IsMine)
        {
            armsObject.SetActive(true);
            bodyObject.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            hairObject.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            bowGeo.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            arrowGeo.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            katanaGeo.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            holder.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            hilt.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
        }
    }
}
