﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// The ninja's create specter ability
/// </summary>
public class NinjaCreateSpecter : Ability
{
    // Support variable so we can only chage it's values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // Create Specter total cooldown
    public override float AbilityCooldown { get; } = 13f;

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // The speed at which the after image will move
    private float specterSpeed = 6f;

    // How long for the specter will last
    private WaitForSeconds specterDuration = new WaitForSeconds(4f);

    // Saves an instance of our after image
    private GameObject specter;

    // CloakingBehaviour
    private CloakingBehaviour cloaking;

    // The code for event instantiation
    byte instantiationEventCode;

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        // CloakingBehaviour
        cloaking = gameObject.AddComponent<CloakingBehaviour>();

        // Set the cloaking duration to 3 seconds
        cloaking.CloakingDuration = 3f;

        // Set the ability current cooldown
        currentCooldown = AbilityCooldown;

        instantiationEventCode = (byte)photonView.OwnerActorNr;
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (!photonView.IsMine)
            return;

        // If our ability is on cooldown
        if (IsOnCooldown)
        {
            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0)
            {
                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }

        if (specter != null)
        {
            // If we have a specter in the game move it forwars
            specter.transform.Translate(0, 0, Time.deltaTime * specterSpeed);
        }
    }

    /// <summary>
    /// Casts the Ability
    /// </summary>
    /// <returns>Waits for a certain ammount of time</returns>
    public override IEnumerator Cast()
    {
        // Set the ability to be on cooldown
        isOnCooldown = true;

        // Raise a Cloaking event to cloak all the player instances across the network
        PhotonNetwork.RaiseEvent(instantiationEventCode, null , 
            new RaiseEventOptions() { 
                Receivers = ReceiverGroup.All, CachingOption = EventCaching.AddToRoomCache 
            }, SendOptions.SendReliable);

        // Instanciate the Smoke Bomb particle effects
        specter = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "NinjaClone"),
            transform.position, transform.rotation);

        // Ignores the collision between the specter and the player
        Physics.IgnoreCollision(GetComponent<CharacterController>(),
            specter.GetComponent<CharacterController>());

        specter.transform.GetChild(0).GetComponentInChildren<Animator>().SetBool(AnimationTags.IS_WALKING, true);

        // Waits for the frame to be rendered before continuing
        yield return specterDuration;

        // Destroy the specter
        PhotonNetwork.Destroy(specter);
    }
}
