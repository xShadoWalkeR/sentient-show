﻿using System.Collections;
using UnityEngine;
using Enums;

/// <summary>
/// Handles the player's weapons
/// </summary>
public class WeaponHandler : MonoBehaviour
{
    /// <summary>
    /// Show in the Editor Stuff
    /// </summary>

    // Our Muzzle Flash effect (can be null)
    [SerializeField] private GameObject muzzleFlash;

    // Audio Source for the Shoot Sound
    [SerializeField] private AudioSource shootSound;

    // Audio Source for the Reaload Sound
    [SerializeField] private AudioSource reloadSound;

    // The attack point (For melee weapons only)
    [SerializeField] private GameObject attackPoint;
    public GameObject AttackPoint { get => attackPoint; }


    /// <summary>
    /// Properties---->
    /// </summary>

    // Ammo available
    [SerializeField] private int ammo = 3;
    public int Ammo { get => ammo; set => ammo = value; }
    public int BaseAmmo { get; private set; }

    // Reload Timer
    [SerializeField] private float reloadSpeed = 1.5f;
    public float ReloadSpeed { get => reloadSpeed; }

    // The Fire Rate of our weapon (RPM (Rounds Per Minute))
    [SerializeField] private float fireRate = 750f;
    public float FireRate { get => fireRate; }

    // The damage of our weapon
    [SerializeField] private float damage = 15f;
    public float Damage { get => damage; set => damage = value; }
    public float BaseDamage { get; private set; }

    // The maximum amount of damage this weapon can do
    // For scaling weapons (Sniper & Tank)
    [SerializeField] private float maxDamage = 120.0f;
    public float MaxDamage { get => maxDamage; private set => maxDamage = value; }

    // The time it takes to reach MaxDamage
    // For scaling weapons (Sniper & Tank) 
    [SerializeField] private float chargeRate = 144.0f;
    public float ChargeRate { get => chargeRate; private set => chargeRate = value; }

    // The type of aim the wepon has
    [SerializeField] private WeaponAim weaponAim;
    public WeaponAim WeaponAim { get => weaponAim; }

    // The type of fire the wepon has
    [SerializeField] private WeaponFireType weaponFireType;
    public WeaponFireType FireType { get => weaponFireType; }

    // The type of bullet the weapon has
    [SerializeField] private WeaponBulletType bulletType;
    public WeaponBulletType BulletType { get => bulletType; }

    /// <summary>
    /// Private Stuff
    /// </summary>

    // Instance of our animator
    [SerializeField] private Animator anim;
    public Animator Anim { get => anim; }

    // Use if there's an additional animator for 3rd person
    [SerializeField] private Animator secondAnim;

    // The coroutine for zooming in/out
    private Coroutine zoomCoroutine;

    /// <summary>
    /// Awake method called when the class is loaded
    /// </summary>
    void Awake()
    {
        if (anim == null)
        {
            // Gets our animator;
            anim = GetComponent<Animator>();
        }

        BaseAmmo = ammo;
        BaseDamage = damage;
    }

    /// <summary>
    /// Plays the Shoot Animation
    /// </summary>
    public void ShootAnimation()
    {
        anim.SetBool(AnimationTags.SHOOT_TRIGGER, true);
        if (secondAnim != null) secondAnim.SetBool(AnimationTags.SHOOT_TRIGGER, true);
    }

    /// <summary>
    /// Play the aim animation
    /// </summary>
    /// <param name="canAim">If you can aim with the weapon</param>
    public void Aim(bool canAim)
    {
        Anim.SetBool(AnimationTags.AIM_PARAMETER, canAim);
    }

    /// <summary>
    /// Play a zoom animation
    /// </summary>
    /// <param name="zoomValue">The value for the blending (0 to 1)</param>
    /// <param name="animSpeed">Controls the blending speed</param>
    public void Zoom(float zoomValue, float animSpeed) {

        if (zoomCoroutine != null) StopCoroutine(zoomCoroutine);

        zoomCoroutine = StartCoroutine(BlendZoom(zoomValue, animSpeed));
    }

    /// <summary>
    /// Play the reload animation
    /// </summary>
    public void Reload(bool canReload) 
    {
        Anim.SetBool(AnimationTags.RELOAD, canReload);
        if (secondAnim != null) secondAnim.SetBool(AnimationTags.RELOAD, canReload);
    }

    /// <summary>
    /// Updates the shooting animation
    /// </summary>
    public void CanShoot(bool canShoot) {
        Anim.SetBool(AnimationTags.CAN_SHOOT, canShoot);
    }

    /// <summary>
    /// Turns on the MuzzleFlash Effect
    /// </summary>
    private void TurnOnMuzzleFlash()
    {
        muzzleFlash.SetActive(true);
    }

    /// <summary>
    /// Turns off the MuzzleFlash Effect
    /// </summary>
    private void TurnOffMuzzleFlash()
    {
        muzzleFlash.SetActive(false);
    }

    /// <summary>
    /// Plays the Shoot Sound
    /// </summary>
    private void Play_ShootSound()
    {
        shootSound.Play();
    }

    /// <summary>
    /// Plays the Reload Sound
    /// </summary>
    private void Play_ReloadSound()
    {
        reloadSound.Play();
    }

    /// <summary>
    /// This will be used for melee weapons
    /// </summary>
    public void TurnOnAttackPoint()
    {
        // Turn on the attackPoint
        attackPoint.SetActive(true);
    }

    /// <summary>
    /// This will be used for melee weapons
    /// </summary>
    public void TurnOffAttackPoint()
    {
        // Turn off the attackPoint
        attackPoint.SetActive(false);
    }

    /// <summary>
    /// Changes damage based on a multiplier
    /// </summary>
    /// <param name="increase">True if it will increase the damage</param>
    /// <param name="multiplier">The damage multiplier</param>
    public void ChangeDamage(bool increase, float multiplier) {

        if (increase) {

            BaseDamage *= multiplier;
            MaxDamage *= multiplier;
            ChargeRate *= multiplier;

        } else {

            BaseDamage /= multiplier;
            MaxDamage /= multiplier;
            ChargeRate /= multiplier;
        }
    }

    private IEnumerator BlendZoom(float targetBlend, float animTime) {

        while (Mathf.FloorToInt(Anim.GetFloat(AnimationTags.ZOOM)) != 1) {

            Anim.SetFloat(AnimationTags.ZOOM, targetBlend, 0.1f, animTime * Time.deltaTime);

            yield return null;
        }
    }
}
