﻿using UnityEngine;
using System;

/// <summary>
/// Struct for the selection buttons of the ninja
/// </summary>
[Serializable]
public struct NinjaSelection
{
    [SerializeField] private GameObject[] mySelectionButtons;
    public GameObject[] MySelectionButtons { get => mySelectionButtons; }

    [SerializeField] private GameObject[] mySelectedButtons;
    public GameObject[] MySelectedButtons { get => mySelectedButtons; }
}
