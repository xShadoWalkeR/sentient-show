﻿using UnityEngine;
using TMPro;

/// <summary>
/// Updates the values to be displayed in the scores tab
/// </summary>
public class SetRowInfo : MonoBehaviour
{
    /// <summary> The player's ping </summary>
    [SerializeField] private TextMeshProUGUI ping;
    public TextMeshProUGUI Ping { get => ping; }

    /// <summary> The player's name </summary>
    [SerializeField] private TextMeshProUGUI name;
    public TextMeshProUGUI Name { get => name; }

    /// <summary> The player's number of kills </summary>
    [SerializeField] private TextMeshProUGUI kills;
    public TextMeshProUGUI Kills { get => kills; }

    /// <summary> The player's number of deaths </summary>
    [SerializeField] private TextMeshProUGUI deaths;
    public TextMeshProUGUI Deaths { get => deaths; }

    /// <summary> The player's KD ratio </summary>
    [SerializeField] private TextMeshProUGUI kd;
    public TextMeshProUGUI KD { get => kd; }

    /// <summary> The player's score </summary>
    [SerializeField] private TextMeshProUGUI score;
    public TextMeshProUGUI Score { get => score; }

    /// <summary>
    /// Updates all the values
    /// </summary>
    /// <param name="playerStats">The player stats</param>
    public void UpdateMyStats(PlayerStats playerStats)
    {
        Name.text = playerStats.Name;
        Kills.text = playerStats.Kills.ToString();
        Deaths.text = playerStats.Deaths.ToString();
        KD.text = playerStats.KD.ToString();
        Score.text = playerStats.Score.ToString();
    }
}
