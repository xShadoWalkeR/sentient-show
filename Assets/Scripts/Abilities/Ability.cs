﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

/// <summary>
/// Ability main Class
/// </summary>
public abstract class Ability : MonoBehaviourPunCallbacks
{
    // Create a new event to be invoked when i shoot
    public virtual UnityEngine.Events.UnityEvent PlayerCast { get; set; }

    /// <summary>
    /// If the ability requires dual casting (pressing 2 buttons)
    /// </summary>
    public virtual bool IsDualCast => false;

    /// <summary>
    /// If the ability is of the charging type
    /// </summary>
    public virtual bool IsCharged => false;

    /// <summary>
    /// If the ability is on cooldown
    /// </summary>
    public abstract bool IsOnCooldown { get;}

    /// <summary>
    /// The cooldown an ability as
    /// </summary>
    public abstract float AbilityCooldown { get; }

    /// <summary>
    /// The current cooldown on an ability
    /// </summary>
    public virtual float CurrentCooldown { get; }

    /// <summary>
    /// Charges the ability
    /// </summary>
    public virtual void ChargeAbility() { }

    /// <summary>
    /// Resets all of the charge values
    /// </summary>
    public virtual void ResetCharge() { }

    /// <summary>
    /// Called when the projectile thrown by an ability hits something
    /// </summary>
    public virtual void ProjectileHit(Vector3 position) { }

    /// <summary>
    /// Detects the current state 
    /// of whatever we want to detect with the first button press
    /// </summary>
    /// <returns>The current goal's state</returns>
    public virtual bool FirstCast() { return false; }

    /// <summary>
    /// Disables effects of the current ability
    /// of the current ability
    /// </summary>
    public virtual void DisableEffects() { }

    /// <summary>
    /// Cast an ability
    /// </summary>
    /// <returns>Waits for something</returns>
    public abstract IEnumerator Cast();
}
