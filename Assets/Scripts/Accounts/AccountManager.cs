﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Enums;

// Contains variables and methods that update certain stats on all scenes
public class AccountManager : MonoBehaviour {

    [Header("Player's Level and XP Settings")]
    [SerializeField] private int maxLevel = 40;
    [SerializeField] private int mapXP = 10000; // Max XP per level
    [Space]
    [SerializeField] private int elimXP = 50;
    [SerializeField] private int winXP = 1000;
    [SerializeField] private int lossXP = 250;

    public static AccountManager am;

    public string PathString { get; set; }

    public string Formatting { get; private set; } = "username,password," +
                                                     "accountLevel,levelXP,tokens," +
                                                     "ExplosiveKunai,CreateSpecter,ShockTherapy,TacticalRepositioning," +
                                                     "NinjaOffensive,NinjaDefensive,SniperOffensive,SniperDefensive";

    private string userName;

    public string UserName {

        get => userName;

        set {

            userName = value;
            PhotonNetwork.NickName = value;
        }
    }

    public List<string> UserSettings { get; private set; } = new List<string>();

    private void Awake() {

        #region Singleton
        if (am == null) {

            am = this;

        } else {

            if (am != this) {

                Destroy(gameObject);
            }
        }

        DontDestroyOnLoad(gameObject);
        #endregion
    }

    private int GetSettingIndex(string setting) {

        string[] formatValues = Formatting.Split(',');

        // Get the index on the first (formatting) line corresponding to the unlocked ability
        for (int i = 0; i < formatValues.Length; i++) {

            if (formatValues[i] == setting) {

                return i;
            }
        }

        return 0;
    }

    public void UnlockAbility(string ability) {

        int myTokens = GetAccountValue("tokens");

        // Verifies if the account has sufficient tokens
        if (myTokens > 0) {

            int abilityIndex = GetSettingIndex(ability);

            // "Catch" the exception of the ability not existing
            if (abilityIndex == 0) {

                print($"'{ability}' not found! Make sure it's correctly written!");

                return;
            }

            SetAccountValue("tokens", myTokens - 1);
            SetAccountValue(ability, "true");

            // Unlock Button...

        } else {

            print("Not enough tokens!");

            // Show some Warning...
        }

        UpdateUserSettings(userName);
    }

    // Use this method to lock/unlock abilities on startup
    public bool AbilityUnlocked(string ability) {

        int abilityIndex = GetSettingIndex(ability);

        // "Catch" the exception of the ability not existing
        if (abilityIndex == 0) {

            print($"'{ability}' not found! Make sure it's correctly written!");

            return false;
        }

        if (Convert.ToBoolean(UserSettings[abilityIndex])) {

            //print($"{ability} is unlocked!");
            return true;
        }

        return false;
    }

    // Increases XP on the settings list
    public void GainXP(XPGainType xpType) {

        // Get account level
        int accLvl = Convert.ToInt32(UserSettings[2]);

        // If maxLevel was reached, leave method
        if (accLvl == maxLevel) return;

        // Get account XP and tokens
        int lvlXP = Convert.ToInt32(UserSettings[3]);
        int tokens = Convert.ToInt32(UserSettings[4]);

        // Increase XP
        switch (xpType) {

            case XPGainType.ELIM:
                lvlXP += elimXP;
                break;

            case XPGainType.WIN:
                lvlXP += winXP;
                break;

            case XPGainType.LOSS:
                lvlXP += lossXP;
                break;
        }

        // Reset XP if max threshhold was reached
        // and increase account level
        if (lvlXP > mapXP) {

            accLvl++;
            lvlXP -= mapXP;

            // If the account level is divisible by '5'
            // add a token
            if (accLvl % 5 == 0) {

                tokens++;
            }
        }

        // Reset XP if max level was reached
        if (accLvl == maxLevel) {

            lvlXP = 0;
        }

        // Save values temporarily
        UserSettings[2] = accLvl.ToString();
        UserSettings[3] = lvlXP.ToString();
        UserSettings[4] = tokens.ToString();
    }

    // Use when match ends!
    // Writes gained XP to the file
    public void SaveXP() {

        // Do some extra verifications to check if anything increased?

        SetAccountValue("accountLevel", UserSettings[2]);
        SetAccountValue("levelXP", UserSettings[3]);
        SetAccountValue("tokens", UserSettings[4]);

        UpdateUserSettings(userName);
    }

    // Works for 'int' values
    public int GetAccountValue(string setting) {

        int valueIndex = GetSettingIndex(setting);

        //print($"Index '{valueIndex}' = '{UserSettings[valueIndex]}'");

        return Convert.ToInt32(UserSettings[valueIndex]);
    }

    // Works for 'int' values
    private void SetAccountValue(string setting, object value) {

        int valueIndex = GetSettingIndex(setting);

        WriteUserSettings(valueIndex, value.ToString());
    }

    public void UpdateUserSettings(string newUser) {

        UserName = newUser;

        using (TextReader reader = File.OpenText(PathString)) {

            // Skips the first line
            string firstLine = reader.ReadLine();

            // Encrypt outside of while to prevent repetition
            string hexUser = Encrypt(UserName);

            while (reader.Peek() > -1) {

                string[] values = reader.ReadLine().Split(',');

                if (values[0] == hexUser) {

                    UserSettings = values.ToList();

                    reader.Close();

                    return;
                }
            }

            reader.Close();
        }
    }

    public void WriteUserSettings(int valueIndex, string value) {

        using (TextReader reader = File.OpenText(PathString)) {

            // Skips the first line
            string firstLine = reader.ReadLine();

            // The lines that will overwrite the text file
            string lines = firstLine;

            // Encrypt outside of while to prevent repetition
            string hexUser = Encrypt(UserName);

            while (reader.Peek() > -1) {

                string[] values = reader.ReadLine().Split(',');

                // Get the user
                if (values[0] == hexUser) {

                    values[valueIndex] = value;

                    print($"Index '{valueIndex}' got changed to '{value}'!");
                }

                lines += "\n" + string.Join(",", values);
            }

            // Close the reader in order to write
            reader.Close();

            ReplaceLine(lines);
        }
    }

    // Actually replaces the whole file...
    public void ReplaceLine(string newLines) {

        // Append to 'false' because we're overwriting the entire file
        using (StreamWriter sw = new StreamWriter(PathString, append: false)) {

            sw.Write(newLines);

            sw.Close();
        }
    }

    public string Encrypt(string stringToConvert) {

        return string.Join("", stringToConvert.Select(c => ((int)c).ToString("X2")));
    }

    private void OnApplicationQuit() {
        
        // Save stuff that didn't save automatically for some reason
    }
}
