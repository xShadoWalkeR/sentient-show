﻿using Photon.Pun;
using System.IO;
using UnityEngine;

namespace Controller {
    /// <summary>
    /// The main controller for the player
    /// </summary>
    public class MainController : MonoBehaviourPunCallbacks
    {
        [SerializeField] protected float mass = 1f; // Player mass
        [SerializeField] protected float damping = 5f; // Air Resistance

        [Header("Movement")]
        [SerializeField] protected float moveSpeed = 2.5f;  // Player move speed
        public float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }

        [SerializeField] protected float jumpForce = 5f;    // Player jump force
        [SerializeField] protected float fallMultiplier;    // Falling multiplier

        public bool EnableLocomotion { get; set; } = true;  // Enable our locomotion
        public float BaseMoveSpeed { get; private set; } // Save the base value for the move speed

        private float camRotation;  // Angle to rotate the camera on the Y
        private float curCameraRotation;    // Current camera rotation

        protected readonly float gravity = Physics.gravity.y; // Gravity value

        protected bool onGround;    // If we're on the ground
        protected float yVelocity;  // Our velocity on the Y axis

        protected bool isWalking; // If we're walking
        protected bool isRunning; // If we're running

        protected Vector3 currentImpact;    // Controls External forces
        public Vector3 movementInput;  // A vector3 containing our speed based on our input
        protected CharacterController charController;   // Character Controller instance

        /// <summary>
        /// Start is called before the first frame update
        /// </summary>
        protected virtual void Start()
        {
            // Instanciate our caracter controller
            charController = GetComponent<CharacterController>();

            BaseMoveSpeed = moveSpeed;

            // Cursor settings //
            // Hide the cursor
            Cursor.visible = false;
            // Lock the cursor to the middle of the screen
            Cursor.lockState = CursorLockMode.Locked;
        }

        /// <summary>
        /// Called once every frame
        /// </summary>
        protected virtual void Update()
        {
            if(photonView.IsMine)
            {
                // Save the isGrounded from the Character Controller to an easier to use variable
                onGround = charController.isGrounded;

                UpdateLocomotionState();
                if (EnableLocomotion)
                    JumpCheck();
            }
        }

        /// <summary>
        /// Called once every physics update
        /// </summary>
        protected virtual void FixedUpdate()
        {
            if (photonView.IsMine)
            {
                if (EnableLocomotion)
                {
                    FallMultiplier();
                }

                Move();
            }
            
        }

        /// <summary>
        /// Moves the player
        /// </summary>
        private void Move()
        {
            if (EnableLocomotion)
            {
                // Create a new vector3 to store user input (Normalize it so the character moves correctly diagonally)
                movementInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

                // Transform the direction to world space
                movementInput = transform.TransformDirection(movementInput);
            } else
            {
                movementInput = Vector3.zero;
            }
            

            if (onGround && yVelocity < 0f)
            {
                yVelocity = -1f; // Reset the falling velocity to 0;
            }

            yVelocity += gravity * Time.deltaTime; // Apply gravity

            // Get the correct movement speed
            movementInput *= Mathf.Max(moveSpeed, 0.3f);

            // Add the gravity to our current velocity
            movementInput.y += yVelocity;

            if (currentImpact.magnitude > 0.2f)
            {
                // If we get it from any external force add that to our current velocity
                movementInput += currentImpact;
            }

            if (charController.enabled)
            {
                // Move the player
                charController.Move(movementInput * Time.deltaTime);
            }
            
            // Decrease the impact force over time
            currentImpact = Vector3.Lerp(currentImpact, Vector3.zero, damping * Time.deltaTime);
        }

        /// <summary>
        /// Resets all the impacts affecting the player
        /// </summary>
        /// <param name="direction"></param>
        public void ResetImpact(string direction = "")
        {
            // Check on which direction we want to reset the impact
            switch (direction)
            {
                case "":
                    // If no Axis was passed we want to stop all movement
                    currentImpact = Vector3.zero;
                    movementInput = Vector3.zero;
                    break;
                case "X":
                    // Resets the impact on the X axis
                    currentImpact.x = 0f;
                    break;
                case "Y":
                    // Resets the impact on the Y axis
                    currentImpact.y = 0f;
                    // Resets yVelocity to account for gravity
                    yVelocity = 0;
                    break;
                case "Z":
                    // Resets the impact on the Z axis
                    currentImpact.z = 0f;
                    break;
            }
        }

        /// <summary>
        /// Checks for a jump input
        /// </summary>
        protected virtual void JumpCheck()
        {
            // If we're on the ground and press to jump
            if (onGround && Input.GetButtonDown("Jump"))
            {
                // Adds a force making the player jump
                AddForce(Vector3.up, jumpForce);
            }
        }

        /// <summary>
        /// Handles faster falling
        /// </summary>
        private void FallMultiplier()
        {
            if (yVelocity < 0)
            {
                // Makes the character fall faster over time
                yVelocity += Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
        }

        /// <summary>
        /// Updates the locomotion state
        /// </summary>
        protected virtual void UpdateLocomotionState()
        {
            // Updates our Locomotion to check if we're walking running or stoped
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                if (Input.GetButtonDown("Run"))
                {
                    isWalking = false;
                    isRunning = true;
                }
                else
                {
                    isRunning = false;
                    isWalking = true;
                }
            }
            else
            {
                isRunning = false;
                isWalking = false;
            }
        }

        /// <summary>
        /// RPC to add force to the player
        /// </summary>
        /// <param name="direction">The direction of impact</param>
        /// <param name="magnitude">The magnitude of the impact</param>
        [PunRPC]
        public void AddForce(Vector3 direction, float magnitude)
        {
            // Calculate the impact force
            currentImpact += direction.normalized * magnitude / mass;
        }
    }
}