﻿using UnityEngine;
using Photon.Pun;

/// <summary>
/// Holds all the player related stats
/// </summary>
public class PlayerStats : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] private Sprite[] mySprites;
    public Sprite[] MySprites { get => mySprites;}

    public string Name { get; set; }
    public int Kills { get; private set; }
    public int Deaths { get; private set; }
    public int Score { get => Kills * 100; }
    public int KD { get => Kills / (Deaths == 0 ? 1 : Deaths); }
    public bool IsDead { get => myPlayer.IsDead; }
    public int? Team { get => myPlayer.MyTeam; }
    public string ChampionName { get; private set; }
    public string AttackAbility { get; private set; }
    public string DefenseAbility { get; private set; }

    private Player myPlayer;

    /// <summary>
    /// Runs before the start
    /// </summary>
    private void Awake()
    {
        myPlayer = GetComponent<Player>();
        Name = PhotonNetwork.NickName;

        DefineChampion();
    }

    private void DefineChampion()
    {
        switch(PlayerPrefs.GetString("MyCharacter"))
        {
            case "Ninja":
                ChampionName = "Saya";
                AttackAbility = PlayerPrefs.GetString("AttackAbility").Remove(0, 5);
                DefenseAbility = PlayerPrefs.GetString("DefenseAbility").Remove(0, 5);
                break;
            case "Sniper":
                ChampionName = "S.U.R.G.E.";
                AttackAbility = PlayerPrefs.GetString("AttackAbility");
                DefenseAbility = PlayerPrefs.GetString("DefenseAbility");
                break;
        }
    }

    /// <summary>
    /// Increments the player's kills
    /// </summary>
    /// <param name="player">The player</param>
    public void IncrementKills(Player player)
    {
        if (myPlayer == player)
        {
            Kills++;
        }
    }

    /// <summary>
    /// Increments the player's deaths
    /// </summary>
    /// <param name="player">The player</param>
    public void IncrementDeaths(Player player)
    {
        if (myPlayer == player)
        {
            Deaths++;
        }
    }

    /// <summary>
    /// Serializes and deserializes data to be sent over the network
    /// </summary>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(Name);
            stream.SendNext(Kills);
            stream.SendNext(Deaths);
            stream.SendNext(ChampionName);
            stream.SendNext(AttackAbility);
            stream.SendNext(DefenseAbility);
        }
        else if (stream.IsReading)
        {
            Name = (string)stream.ReceiveNext();
            Kills = (int)stream.ReceiveNext();
            Deaths = (int)stream.ReceiveNext();
            ChampionName = (string)stream.ReceiveNext();
            AttackAbility = (string)stream.ReceiveNext();
            DefenseAbility = (string)stream.ReceiveNext();
        }
    }
}
