﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Enums;
using TMPro;

/// <summary>
/// Handles the main ui
/// </summary>
public class UiManager : MonoBehaviour
{
    /// <summary> Variables for the Health and player icons </summary>
    [Header("Health and player Icon")]
    [SerializeField] private TextMeshProUGUI health;
    [SerializeField] private Image damaged;
    [SerializeField] private Image healed;
    [SerializeField] private Color damageColor;
    [SerializeField] private Color healColor;
    [SerializeField] private Image playerIcon;

    /// <summary> Variables for the first ability </summary>
    [Header("Ability one")]
    [SerializeField] private Image abilityOneIcon;
    [SerializeField] private TextMeshProUGUI ablOneKey;
    [SerializeField] private Image ablOneCooldownOverlay;
    [SerializeField] private TextMeshProUGUI ablOneCooldownText;

    /// <summary> Variables for the second ability </summary>
    [Header("Ability two")]
    [SerializeField] private Image abilityTwoIcon;
    [SerializeField] private TextMeshProUGUI ablTwoKey;
    [SerializeField] private Image ablTwoCooldownOverlay;
    [SerializeField] private TextMeshProUGUI ablTwoCooldownText;

    /// <summary> Variables for the ultimate ability </summary>
    [Header("Ability three")]
    [SerializeField] private Image ultimateIcon;
    [SerializeField] private TextMeshProUGUI ablUltKey;
    [SerializeField] private Image ablUltCooldownOverlay;
    [SerializeField] private TextMeshProUGUI ablUltCooldownText;

    /// <summary> Variable for the player ammo </summary>
    [Header("Ammo")]
    [SerializeField] private TMP_Text ammoCount;

    [Header("Hits")]
    [SerializeField] private GameObject hitObject;
    [SerializeField] private Transform hitsParent;

    // Gets a reference to the player object
    [HideInInspector]
    public GameObject myPlayerObject;

    // Has a referece to the player script
    private Player myPlayer;

    // Gets a reference to the player stats
    private PlayerStats myStats;

    // Has a reference to the Player Shoot script
    private PlayerShoot pShoot;

    // Has a reference to the Weapon Manager script
    private WeaponManager manager;

    private float hp = 0;


    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        // Initialize our player variable
        myPlayer = myPlayerObject.GetComponent<Player>();

        // Initialize our player stats variable
        myStats = myPlayerObject.GetComponent<PlayerStats>();

        // Get our Player Shoot
        pShoot = myPlayerObject.GetComponent<PlayerShoot>();

        // Get our Weapon Manager
        manager = myPlayerObject.GetComponent<WeaponManager>();

        health.text = myPlayer.MaxHealth.ToString();

        hp = myPlayer.CurrentHealth;

        SetSprites();
    }

    private void SetSprites()
    {
        playerIcon.sprite = myStats.MySprites[0];
        ultimateIcon.sprite = myStats.MySprites[1];

        for (int i = 2; i < myStats.MySprites.Length; i++)
        {
            if (myStats.MySprites[i].name == myStats.AttackAbility)
            {
                abilityOneIcon.sprite = myStats.MySprites[i];
            }

            if (myStats.MySprites[i].name == myStats.DefenseAbility)
            {
                abilityTwoIcon.sprite = myStats.MySprites[i];
            }
        }
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        UpdateHPSlider();
        UpdateAbilityOneStats();
        UpdateAbilityTwoStats();
        UpdateUltAbilityStats();

        UpdateAmmo();
    }

    /// <summary>
    /// Updates the values on the HP slider
    /// </summary>
    private void UpdateHPSlider()
    {
        if (hp > myPlayer.CurrentHealth)
        {
            damaged.gameObject.SetActive(true);
            damageColor.a -= Time.deltaTime * 0.75f;
            damaged.color = damageColor;

            health.color = Color.red;
            health.outlineColor = Color.white;
            hp -= Time.deltaTime * 50;
            health.text = ((int)hp).ToString();
        } 
        else if (hp < myPlayer.CurrentHealth - 3)
        {
            healed.gameObject.SetActive(true);
            healColor.a -= Time.deltaTime * 0.75f;
            healed.color = healColor;

            health.color = Color.cyan;
            health.outlineColor = Color.white;
            hp += Time.deltaTime * 50;
            health.text = ((int)hp).ToString();

        } else
        {
            if (damageColor.a <= 0) {
                damaged.gameObject.SetActive(false);
                damageColor.a = 1f;
            } else if (damaged.gameObject.activeSelf)
            {
                damageColor.a -= Time.deltaTime * 0.75f;
                damaged.color = damageColor;
            }

            if (healColor.a <= 0)
            {
                healed.gameObject.SetActive(false);
                healColor.a = 1f;
            }
            else if (healed.gameObject.activeSelf)
            {
                healColor.a -= Time.deltaTime * 0.75f;
                healed.color = healColor;
            }

            health.color = Color.white;
            health.outlineColor = Color.black;
            hp = myPlayer.CurrentHealth;
            health.text = myPlayer.CurrentHealth.ToString();
        }
    }

    /// <summary>
    /// Updates the ability one stats
    /// </summary>
    private void UpdateAbilityOneStats() {
        if (myPlayer.Abilities[1].CurrentCooldown < myPlayer.Abilities[1].AbilityCooldown)
        {
            ablOneCooldownOverlay.fillAmount = 
                myPlayer.Abilities[1].CurrentCooldown / myPlayer.Abilities[1].AbilityCooldown;

            ablOneCooldownText.text = ((int)myPlayer.Abilities[1].CurrentCooldown).ToString();
        } else
        {
            ablOneCooldownOverlay.fillAmount = 0;
            ablOneCooldownText.text = null;
        }
    }

    /// <summary>
    /// Updates the ability two stats
    /// </summary>
    private void UpdateAbilityTwoStats()
    {
        if (myPlayer.Abilities[2].CurrentCooldown < myPlayer.Abilities[2].AbilityCooldown)
        {
            ablTwoCooldownOverlay.fillAmount =
                myPlayer.Abilities[2].CurrentCooldown / myPlayer.Abilities[2].AbilityCooldown;

            ablTwoCooldownText.text = ((int)myPlayer.Abilities[2].CurrentCooldown).ToString();
        } else
        {
            ablTwoCooldownOverlay.fillAmount = 0;
            ablTwoCooldownText.text = null;
        }
    }

    /// <summary>
    /// Updates the ultimate ability stats
    /// </summary>
    private void UpdateUltAbilityStats()
    {
        if (myPlayer.Abilities[0].IsOnCooldown)
        {
            ablUltCooldownOverlay.fillAmount =
                myPlayer.Abilities[0].CurrentCooldown / myPlayer.Abilities[0].AbilityCooldown;
            ablUltCooldownText.text = ((int)myPlayer.Abilities[0].CurrentCooldown).ToString();
        } else
        {
            ablUltCooldownOverlay.fillAmount = 0;
            ablUltCooldownText.text = null;
        }

    }

    /// <summary>
    /// Updates the weapon ammo
    /// </summary>
    private void UpdateAmmo() {

        if (pShoot.CurrentWeapon.FireType == WeaponFireType.SINGLE) {

            ammoCount.text = "∞";
            return;

        } else if (pShoot.CurrentWeapon.FireType == WeaponFireType.AKIMBO) {

            int currentAmmo = manager.Weapons[1].Ammo + manager.Weapons[2].Ammo;
            int totalAmmo = manager.Weapons[1].BaseAmmo + manager.Weapons[2].BaseAmmo;

            ammoCount.text = $"{currentAmmo} / {totalAmmo}";
        }
    }

    // Creates a hit indicator that points at the location of the other player
    public void CreateHit(Player enemy) {

        HitManager hit = Instantiate(hitObject, hitsParent).GetComponent<HitManager>();
        hit.OtherPlayer = enemy.transform;
    }
}