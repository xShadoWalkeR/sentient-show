﻿using UnityEngine;
using TMPro;
using Controller;
using Enums;
using Photon.Pun;
using UnityEngine.SceneManagement;

/// <summary>
/// Responsible for ending the round and displaying who won the game
/// </summary>
public class RoundEnder : MonoBehaviourPunCallbacks
{
    // Constant strings to represent who won
    private const string TEAM_ONE_WINS = "Team One Wins!!!";
    private const string TEAM_TWO_WINS = "Team Two Wins!!!";
    private const string ITS_A_DRAW = "It's A Draw!";
    private const int TIME_TO_WAIT = 10;

    // The end screen object
    [SerializeField] private GameObject endScreen;

    // The game timer object
    [SerializeField] private GameTimer gameTimer;

    // The final text object
    [SerializeField] private TextMeshProUGUI finalText;

    // The time counter object
    [SerializeField] private TextMeshProUGUI timeCounter;

    // The winner
    private Winner winner;

    // If we want to display the end screen
    private bool displayingEndScreen;

    // The player
    private GameObject player;

    // The timer
    private float timer;

    // The account manager script reference
    private AccountManager am;

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        // Set the displayingEndScreen to false
        displayingEndScreen = false;

        // Initialize the timer
        timer = TIME_TO_WAIT;

        // Get the 'am' singleton
        am = AccountManager.am;
    }
    
    /// <summary>
    /// Called once every frame
    /// </summary>
    private void Update()
    {
        // Check if the round has ended
        CheckForRoundEnd();

        // If there's a winner and we're not yet displaying the end screen
        if (winner != Winner.NONE && !displayingEndScreen)
        {
            // Add the player to the list of players
            player = GameObject.FindGameObjectWithTag(Tags.PLAYER_TAG);

            // Increase and save the player's XP/Lvl
            IncreaseXPAndLevel();

            // Display the end screen
            DisplayEndScreen();
        }
        else if (displayingEndScreen)
        {
            // Else if we are displaying the end screen
            // Run the timer to exit the match
            TimeEnding();
        }
    }

    /// <summary>
    /// Displays a timer of how long to leave the match
    /// </summary>
    private void TimeEnding()
    {
        // Decrease the timer 1 each second
        timer -= Time.deltaTime;

        // Update the Display Timer
        timeCounter.text = string.Format($"{(int)timer}");

        // If the timer has reached 0 leaves the game
        if (timer <= 0)
        {
            PhotonNetwork.DestroyPlayerObjects(player.GetComponent<Player>().photonView.OwnerActorNr);
            Destroy(PhotonRoom.room.gameObject);
            PhotonNetwork.LeaveRoom();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            SceneManager.LoadScene("Menu");
        }
    }

    /// <summary>
    /// Checks if the round has ended either by kills or time
    /// </summary>
    private void CheckForRoundEnd()
    {
        // If the round has ended by time
        if (gameTimer.Timer >= gameTimer.TimeLimit + gameTimer.OFFSET_SECONDS)
        {
            // Set the team with the most kills as the winner
            if (KillsManager.km.TeamOneKills > KillsManager.km.TeamTwoKills)
            {
                winner = Winner.TEAM_ONE;
            }
            else if (KillsManager.km.TeamTwoKills > KillsManager.km.TeamOneKills)
            {
                winner = Winner.TEAM_TWO;
            } 
            else
            {
                // Or draw if both have the same number of kills
                winner = Winner.DRAW;
            }
        } 
        // Else if the team one has more kills
        else if (KillsManager.km.TeamOneKills >= 60)
        {
            // Set the team one as the winner
            winner = Winner.TEAM_ONE;
        } 
        else if (KillsManager.km.TeamTwoKills >= 60)
        {
            // Or the team two if they have more kills
            winner = Winner.TEAM_TWO;
        }
    }

    /// <summary>
    /// Displays the end screen
    /// </summary>
    private void DisplayEndScreen()
    {
        displayingEndScreen = true;

        // Display the end screen
        endScreen.SetActive(true);

        // Check what text to display
        switch (winner)
        {
            case Winner.DRAW:
                finalText.text = ITS_A_DRAW;
                break;
            case Winner.TEAM_ONE:
                finalText.text = TEAM_ONE_WINS;
                break;
            case Winner.TEAM_TWO:
                finalText.text = TEAM_TWO_WINS;
                break;
        }

        // Disable all player input
        player.GetComponent<Player>().enabled = false;
        player.GetComponent<PlayerShoot>().enabled = false;
        player.GetComponent<MainController>().enabled = false;
        player.GetComponent<CameraController>().enabled = false;
    }

    /// <summary>
    /// Increases the player's XP and level
    /// </summary>
    private void IncreaseXPAndLevel() {

        // Check if the player's team won
        if ((int)winner - 1 == player.GetComponent<Player>().MyTeam) {

            // If so, gain a lot of XP
            am.GainXP(XPGainType.WIN);

            print("Player WON");

        } else {

            // If not, gain less XP
            am.GainXP(XPGainType.LOSS);

            print("Player LOST/DRAW");
        }

        // Save the accumulated XP onto the file
        am.SaveXP();
    }
}
