﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HeaderController : MonoBehaviour {

    [Header("Header Components")]
    [SerializeField] private Player player;
    [SerializeField] private Slider healthSlider;
    [SerializeField] private TextMeshProUGUI playerName;
    [Space]
    [SerializeField] private Image sliderFill;

    [Header("Slider Colors")]
    [SerializeField] private Color allyColor = Color.blue;
    [SerializeField] private Color enemyColor = Color.red;

    void Start() {

        playerName.text = player.name;

        // Set color as ally so the players don't see the color transition below :P
        sliderFill.color = allyColor;

        if (player.photonView.IsMine) {

            gameObject.SetActive(false);
        }

        Invoke("FillColor", 1.5f);
    }

    private void FillColor() {

        if (player.CompareTag(Tags.ENEMY_TAG)) {

            sliderFill.color = enemyColor;

        } else {

            sliderFill.color = allyColor;
        }
    }

    void Update() {

        healthSlider.value = player.CurrentHealth;

        LookAtPlayer();
    }

    private void LookAtPlayer() {

        if (Camera.main != null) 
            transform.LookAt(Camera.main.transform);
    }
}
