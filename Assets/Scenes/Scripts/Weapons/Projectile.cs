﻿using Photon.Pun;
using UnityEngine;
using Enums;

/// <summary>
/// The arrows on a Bow are physicly based so we can't just
/// do a rayCast like in most weapons...
/// </summary>
public class Projectile : MonoBehaviourPunCallbacks
{
    // The projectile speed
    [SerializeField] private float speed = 30f;

    // Does the projectile get destroyed on hit?
    [SerializeField] private bool destroyOnHit = false;

    // Does this projectile get disabled
    [SerializeField] private bool dontDisable;

    // Does this projectile needs to rotate
    [SerializeField] private bool isArrow;

    // The projectile destroy timer
    [SerializeField] private float destroyTimer = 5f;

    // The projectile damage
    [SerializeField] private float damage = 50f;
    public float Damage { get => damage; set => damage = value; }

    // HeadShot Multiplier
    [SerializeField] private float hsMultiplier = 2f;

    // Buff type of the projectile
    [SerializeField] private StatusEffectType effectType = StatusEffectType.NONE;

    // Value for the buff or debuff
    [SerializeField] [Tooltip("Value for the buff or debuff")] private float dbValues;

    // The duration for the buff or debuff
    [SerializeField] [Tooltip("duration for the buff or the debuff")] private float dbDuration;

    // The projectile rigid body
    private Rigidbody myBody;

    // Enemy i'll hit
    private Player enemy;

    // What ability casted me
    private Ability myCaster;

    // Who owns me
    public Player Owner { get; set; }

    // Our Hit Marker Instance
    private HitMarkerBehaviour hitMarker;

    /// <summary>
    /// Awake method called on load
    /// </summary>
    void Awake()
    {
        myBody = GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Start method called at the start
    /// </summary>
    void Start()
    {
        if (photonView.IsMine)
        {
            // Call the DestroyProjectile method after 'destoryTimer' seconds
            Invoke("DestroyProjectile", destroyTimer);
        }

        hitMarker = HitMarkerBehaviour.Instance;
    }
    
    /// <summary>
    /// Overrides the properties of the projectile
    /// </summary>
    /// <param name="multiplier">Projectile Speed</param>
    public void OverrideProjectileSettings(float multiplier)
    {
        if(multiplier < 0.2f)
        {
            multiplier = 0.2f;
        }

        // Convert the multiplier to be of base 1
        multiplier = multiplier * 100 / 75;

        speed *= multiplier;
        damage *= multiplier;
    }

    /// <summary>
    /// Recieves a reference to it's caster script
    /// </summary>
    /// <param name="ability">Caster script</param>
    public void CasterOverride(Ability ability)
    {
        myCaster = ability;
    }

    /// <summary>
    /// Lets the arrow loose
    /// </summary>
    /// <param name="direction">The direction to let loose</param>
    public void Loose(Vector3 direction)
    {
        // Apply a velocity to the arrow
        myBody.velocity = direction * speed;

        if (isArrow)
        {
            // Set it up so the arrow is rotated towards the direction
            transform.LookAt(transform.position + myBody.velocity);
        }
    }

    /// <summary>
    /// Destroys the projectile
    /// </summary>
    private void DestroyProjectile()
    {
        // Destroys the object across the network
        PhotonNetwork.Destroy(gameObject);
    }

    /// <summary>
    /// Called wen we collide with something
    /// </summary>
    /// <param name="target">The enemy this projectile hit</param>
    void OnTriggerEnter(Collider target)
    {
        if (photonView.IsMine)
        {
            if (target.CompareTag(Tags.PLAYER_TAG)) return;

            if (myCaster != null)
            {
                // Tell my caster i hit something and what position i had when i hit
                myCaster.ProjectileHit(transform.position);
            }

            // If we collided with an enemy
            if (target.CompareTag(Tags.ENEMY_TAG))
            {
                enemy = target.GetComponent<Player>();

                if (Vector3.Distance(transform.position, target.transform.position) >= 1.48f)
                {
                    // Call a headshot hit marker
                    hitMarker.Hit(true);

                    // Call an RPC method to deal damage to the enemy
                    enemy.photonView.RPC("RPC_TakeDamage", RpcTarget.All, (int)(damage * hsMultiplier), Owner.name);
                } else
                {
                    // Call a regular hit marker
                    hitMarker.Hit(false);

                    // Call an RPC method to deal damage to the enemy
                    enemy.photonView.RPC("RPC_TakeDamage", RpcTarget.All, (int)damage, Owner.name);
                }

                // If this projectile as an effect type
                if (effectType != StatusEffectType.NONE)
                {
                    // Apply the status effect on the player
                    enemy.photonView.RPC("RPC_ApplyStatusEffect", RpcTarget.All, effectType, dbValues, dbDuration);
                }
                
                if (!dontDisable)
                {
                    // Disable the projectile
                    gameObject.SetActive(false);
                }
            }

            if (!dontDisable)
            {
                // Disable all colliders
                Collider[] c = GetComponents<Collider>();
                for (int i = 0; i < c.Length; i++)
                {
                    c[i].enabled = false;
                }
            }

            if (destroyOnHit) {

                DestroyProjectile();
            }

            // Change the collision detection mode to avoid warnings
            myBody.collisionDetectionMode = CollisionDetectionMode.Discrete;

            if (isArrow)
            {
                // Stop all arrow movement
                myBody.isKinematic = true;
            }
        }
    }
}
