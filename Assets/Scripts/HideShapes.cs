﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HideShapes : MonoBehaviourPunCallbacks
{
    public Player myOwner;
    public GameObject myHair;

    private Renderer myRender;

    private void Start()
    {
        if (myOwner.photonView.IsMine)
        {
            myHair.SetActive(false);

            myRender = GetComponent<Renderer>();

            myRender.materials[1].SetFloat("_Mode", 1);

            myRender.materials[1].SetColor("_Color", new Color(myRender.materials[1].color.r, myRender.materials[1].color.g, myRender.materials[1].color.b, 0));
            myRender.materials[1].renderQueue = 3000;
            myRender.materials[1].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            myRender.materials[1].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            myRender.materials[1].SetInt("_ZWrite", 0);
            myRender.materials[1].DisableKeyword("_ALPHATEST_ON");
            myRender.materials[1].EnableKeyword("_ALPHABLEND_ON");
            myRender.materials[1].DisableKeyword("_ALPHAPREMULTIPLY_ON");
        }
    }
}
