﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ObjRenamer : MonoBehaviour
{
    [SerializeField] private string textToAdd;
    [SerializeField] private bool renameAll;

    // Update is called once per frame
    void Update()
    {
        if (renameAll)
        {
            renameAll = false;
            RenameChildRecursive(gameObject);
        }
    }

    private void RenameChildRecursive(GameObject obj)
    {

        if (obj == null || !obj.activeSelf)
            return;

        // For each child this has 
        foreach (Transform child in obj.transform)
        {

            // If it's null, skip
            if (child == null || !child.gameObject.activeSelf || child == obj.GetComponent<Transform>())
                continue;

            child.name = textToAdd + child.name;

            // Get all the childs of the current object
            RenameChildRecursive(child.gameObject);
        }
    }
}
