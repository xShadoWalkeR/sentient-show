﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class OptionsMenu : MonoBehaviour
{
    [Header("Video")]
    [SerializeField] private TMP_Dropdown displayMode;
    [SerializeField] private TMP_Dropdown resolution;
    [SerializeField] private Slider fieldOfView;
    [SerializeField] private TMP_Dropdown fpsLimit;
    [SerializeField] private TMP_Dropdown graphicsQuality;

    [Header("Sound")]
    [SerializeField] private AudioSource buttonSource;
    [SerializeField] private AudioSource musicSource;
    [Space]
    [SerializeField] private Slider masterVolume;
    [SerializeField] private Slider soundEffects;
    [SerializeField] private Slider musicVolume;
    

    [Header("Controls")]
    [SerializeField] private Slider sensitivity;
    [SerializeField] private TMP_InputField sensitivityInput;
    [SerializeField] private TMP_Dropdown invertLook;

    [Header("Multiplayer")]
    [SerializeField] private TMP_InputField maxPlayers;
    [SerializeField] private TMP_InputField timeToStart;

    private List<string> availableRes;
    private string[] res;
    private string currentRes;
    private string currentString;

    private string[] qualityNames;

    // Start is called before the first frame update
    void Start()
    {
        currentRes = Screen.currentResolution.ToString();

        resolution.captionText.text = currentRes;

        qualityNames = QualitySettings.names;

        resolution.ClearOptions();

        availableRes = new List<string>();

        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            currentString = Screen.resolutions[i].ToString();

            // Work arround to only show resolutions on the currently selected refresh rate
            if (currentString.Substring(currentString.Length - 5) != currentRes.Substring(currentRes.Length - 5)) continue;

            availableRes.Add(currentString.Remove(currentString.Length - StringBackCounter(currentString)));
        }

        // Add all available resolutions to the dropdown menu
        resolution.AddOptions(availableRes);

        PlayerPreferences();

        Preselections();
    }

    private void PlayerPreferences()
    {
        if (!PlayerPrefs.HasKey("GameFPSLock"))
        {
            PlayerPrefs.SetInt("GameFPSLock", 144);
        }

        if (!PlayerPrefs.HasKey("GameFOV"))
        {
            PlayerPrefs.SetInt("GameFOV", 70);
        }

        if (!PlayerPrefs.HasKey("MouseSense"))
        {
            PlayerPrefs.SetFloat("MouseSense", 3f);
        }

        if (!PlayerPrefs.HasKey("InvertYAxis"))
        {
            PlayerPrefs.SetInt("InvertYAxis", 0);
        }

        if (!PlayerPrefs.HasKey("MusicVolume"))
        {
            PlayerPrefs.SetFloat("MusicVolume", 0.5f);
        }

        if (!PlayerPrefs.HasKey("FXVolume"))
        {
            PlayerPrefs.SetFloat("FXVolume", 0.5f);
        }
    }

    /// <summary>
    /// Preselected settings at start
    /// </summary>
    private void Preselections()
    {
        // Preselect the current resolution
        for (int i = 0; i < resolution.options.Count; i++)
        {
            if (resolution.options[i].text == currentRes.Remove(currentRes.Length - StringBackCounter(currentRes)))
            {
                resolution.value = i;
                break;
            }
        }

        for (int i = 0; i < fpsLimit.options.Count; i++)
        {
            if (fpsLimit.options[i].text == PlayerPrefs.GetInt("GameFPSLock").ToString())
            {
                fpsLimit.value = i;
            }
        }

        fieldOfView.value = PlayerPrefs.GetInt("GameFOV");
        fieldOfView.GetComponentInChildren<TextMeshProUGUI>().text = PlayerPrefs.GetInt("GameFOV").ToString();

        graphicsQuality.value = QualitySettings.GetQualityLevel();

        soundEffects.value = PlayerPrefs.GetFloat("FXVolume");
        musicVolume.value = PlayerPrefs.GetFloat("MusicVolume");

        sensitivity.value = PlayerPrefs.GetFloat("MouseSense");
        sensitivityInput.text = PlayerPrefs.GetFloat("MouseSense").ToString();

        invertLook.value = PlayerPrefs.GetInt("InvertYAxis");


        timeToStart.text = PhotonRoom.room.startingTime.ToString();
        maxPlayers.text = MultiplayerSettings.mpSettings.MaxPlayer.ToString();
    }

    // Set the display mode
    public void ChangeDisplayMode()
    {
        Screen.fullScreenMode = Enum.TryParse<FullScreenMode>(displayMode.captionText.text, out FullScreenMode mode) ? mode : Screen.fullScreenMode;
    }

    /// <summary>
    /// Set the target resolution
    /// </summary>
    public void ChangeTargetResolution()
    {
        res = resolution.captionText.text.Split(' ', 'x');
        Screen.SetResolution(Convert.ToInt32(res[0]), Convert.ToInt32(res[3]), Screen.fullScreen);
    }

    /// <summary>
    /// Set the Field of View
    /// </summary>
    public void ChangeFieldOfView()
    {
        PlayerPrefs.SetInt("GameFOV", (int)fieldOfView.value);

        fieldOfView.GetComponentInChildren<TextMeshProUGUI>().text = ((int)fieldOfView.value).ToString();
    }

    /// <summary>
    /// Set the FPS Limit
    /// </summary>
    public void ChangeFPSLimit()
    {
        PlayerPrefs.SetInt("GameFPSLock", Convert.ToInt32(fpsLimit.captionText.text));
    }

    public void ChangeGraphicsQuality()
    {
        QualitySettings.SetQualityLevel(graphicsQuality.value);
    }

    public void ChangeSensitivity(bool isSlider)
    {
        if (isSlider)
        {
            PlayerPrefs.SetFloat("MouseSense", sensitivity.value);
            sensitivityInput.text = sensitivity.value.ToString("f2");
        } else
        {
            PlayerPrefs.SetFloat("MouseSense", sensitivity.value = Convert.ToSingle(sensitivityInput.text));
        }
    }

    public void ChangeYAxisInvert()
    {
        PlayerPrefs.SetInt("InvertYAxis", invertLook.value);
    }

    public void ChangeMaxPlayers()
    {
        MultiplayerSettings.mpSettings.MaxPlayer = Convert.ToInt32(maxPlayers.text);
    }

    public void ChangeTimeToStart()
    {
        PhotonRoom.room.startingTime = Convert.ToInt32(timeToStart.text);
    }

    public void ChangeMasterVolume()
    {
        PlayerPrefs.SetFloat("MasterVolume", masterVolume.value);

        ChangeFXVolume();
        ChangeMusicVolume();
    }

    public void ChangeFXVolume()
    {
        buttonSource.volume = soundEffects.value * masterVolume.value;
        PlayerPrefs.SetFloat("FXVolume", soundEffects.value * masterVolume.value);
    }

    public void ChangeMusicVolume()
    {
        musicSource.volume = musicVolume.value * masterVolume.value;
        PlayerPrefs.SetFloat("MusicVolume", musicVolume.value * masterVolume.value);
    }

    /// <summary>
    /// This will know how many spaces to cut from the string independent of the Hz
    /// </summary>
    /// <param name="s">string</param>
    /// <returns>number of chars to remove</returns>
    private int StringBackCounter(string s)
    {
        int spaceCount = 0;
        int charCount = 1;

        for (int i = s.Length - 1; i > 0; i--, charCount++)
        {
            if(s[i] == ' ')
            {
                spaceCount++;
            }

            if (spaceCount == 2) break;
        }

        return charCount;
    }
}
