﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AbilitiesUI : MonoBehaviour
{
    private const string ultimate = "Ultimate";
    private const string passive = "Passive";

    [SerializeField] private List<GameObject> underLayImages;
    [SerializeField] private List<GameObject> abilityDescriptions;

    

    public void UpdateUnderLayImages(GameObject myImage)
    {
        for(int i = 0; i < underLayImages.Count; i++)
        {
            if (underLayImages[i] != myImage)
                underLayImages[i].SetActive(false);
            else
                underLayImages[i].SetActive(true);
        }
    }


    public void UpdateAbilityDescriptions(GameObject descriptionOne)
    {
        for (int i = 0; i < abilityDescriptions.Count; i++)
        {
            if (abilityDescriptions[i] != descriptionOne) {
                abilityDescriptions[i].SetActive(false);
            }
            else
            {
                if (abilityDescriptions[i].name != ultimate &&
                    abilityDescriptions[i].name != passive)
                {
                    abilityDescriptions[i - 1].SetActive(true);
                }
                
                abilityDescriptions[i].SetActive(true);
            }
               
        }
    }
}
