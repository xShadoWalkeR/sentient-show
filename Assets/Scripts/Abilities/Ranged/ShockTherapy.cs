﻿using System.Collections;
using System.IO;
using UnityEngine;
using Controller;
using Photon.Pun;

public class ShockTherapy : Ability {

    [SerializeField] public override float AbilityCooldown { get; } = 8.0f;   // The ultimate total cooldown

    [SerializeField] private float dashForce = 30.0f;   // The backwards dash's speed

    [SerializeField] private float shockDamage = 15.0f;

    // The current remaining time on our cooldown
    private float currentCooldown;
    public override float CurrentCooldown { get => currentCooldown; }

    // Support variable so we can only change its values in private
    private bool isOnCooldown;
    public override bool IsOnCooldown => isOnCooldown;

    // Our player controller
    private SniperController controller;

    // The projectile to be created
    private GameObject projectile;

    // Our camera
    private Transform cam;

    // Our animator
    private Animator anim;

    private void Awake() {

        // Fetch the animator
        anim = transform.GetChild(1).GetComponentInChildren<Animator>();
    }

    void Start() {

        // Assing our current cooldown as the given cooldown
        currentCooldown = AbilityCooldown;

        // Fetch the 'SniperController' script
        controller = GetComponent<SniperController>();

        // Get our camera
        if (photonView.IsMine) cam = Camera.main.transform;
    }

    void Update() {

        if (!photonView.IsMine) return;

        // If our ability is on cooldown
        if (IsOnCooldown) {
            // Reduce it each second
            currentCooldown -= Time.deltaTime;

            // If our cooldown as reached 0
            if (currentCooldown <= 0) {

                // Reset the starting cooldown value
                currentCooldown = AbilityCooldown;

                // Say we're no longer on cooldown
                isOnCooldown = false;
            }
        }
    }

    public override IEnumerator Cast() {

        // Instantly throw shock object
        yield return new WaitForSeconds(0);

        // Dash backwards
        controller.AddForce(-cam.transform.forward, dashForce);

        projectile = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Shock"), cam.position + cam.forward, cam.rotation);

        projectile.GetComponent<Projectile>().Damage = shockDamage;

        projectile.GetComponent<Projectile>().Owner = GetComponent<Player>();

        // Shoot the projectile
        projectile.GetComponent<Projectile>().Loose(cam.transform.forward);

        // Start cooldown
        isOnCooldown = true;

        // Play the dash animation
        anim.SetBool(AnimationTags.DASH_BACK, true);
    }

    /// <summary>
    /// Changes damage based on a multiplier
    /// </summary>
    /// <param name="increase">True if it will increase the damage</param>
    /// <param name="multiplier">The damage multiplier</param>
    public void ChangeDamage(bool increase, float multiplier) {

        if (increase) {

            shockDamage *= multiplier;

            if (projectile != null) projectile.GetComponent<Projectile>().Damage = shockDamage;

        }
        else {

            shockDamage /= multiplier;

            // Don't reset the projectile's damage
            // Those things are hard to hit already xD
        }
    }
}
