﻿using ExitGames.Client.Photon;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Realtime;

/// <summary>
/// Handles the general behaviour for cloacking
/// </summary>
public class CloakingBehaviour : MonoBehaviourPunCallbacks, IPunObservable
{
    // List of all materials our player has
    [SerializeField] private List<Material> listOfMaterials;

    [SerializeField] private List<Material> materialBackUp;

    [SerializeField] private List<Material> cloakMaterials;

    private bool cloak;

    // The current renderer
    private Renderer currentRenderer;

    // How long the player stays cloaked
    public float CloakingDuration { get; set; } = 3f;

    // Cloaking timer
    private float cloakTimer = 0f;

    // If the player is currently cloaked
    public bool IsCloaked { get; private set; }

    // Timer to change the render queue and Alpha
    private float alphaTimer = 1f;

    // Reference to our shooting script
    PlayerShoot playerShoot;

    // Reference to my player script
    Player me;

    byte instantiationEventCode;

    float appearTimer = 1;
    float alpha = 1;

    private string rt = "rt";

    private void Awake()
    {
        GetComponent<PhotonView>().ObservedComponents.Add(this);
    }

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    void Start()
    {
        instantiationEventCode = (byte)photonView.OwnerActorNr;

        PhotonNetwork.AddCallbackTarget(this);

        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

        // Initialize the materials list
        listOfMaterials = new List<Material>();

        // Initialize the cloak materials list
        cloakMaterials = new List<Material>();

        // Initialize playerShoot
        playerShoot = GetComponent<PlayerShoot>();

        // Initialize all our abilities
        me = GetComponent<Player>();

        // Get the cloaking materials
        foreach (Material m in Resources.LoadAll("Materials/Cloak"))
        {
            cloakMaterials.Add(new Material(m));
        }

        // Recursevly gets all the childs of this object
        GetChildRecursive(gameObject);

        // Add ourselfs as a listner to some events
        AddMeAsListner();

        materialBackUp = new List<Material>(listOfMaterials);
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (photonView.IsMine)
            {
            // If the player is cloaked
            if (IsCloaked && CloakingDuration > 0)
            {
                // Increasse the cloakTimer by Time.deltaTime
                cloakTimer += Time.deltaTime;

                // If the cloakTimer is greater or equal to the cloakingDuration
                if (cloakTimer >= CloakingDuration)
                {
                    // Resets the cloak timer
                    cloakTimer = 0;

                    // Removes the cloaking
                    PhotonNetwork.RaiseEvent(instantiationEventCode, null,
                        new RaiseEventOptions()
                        {
                            Receivers = ReceiverGroup.All,
                            CachingOption = EventCaching.AddToRoomCache
                        }, SendOptions.SendReliable);
                }
            }
        }

        if (cloak)
        {
            if (!IsCloaked)
            {
                if (appearTimer <= 0.25f)
                {
                    alpha = Mathf.Clamp(alpha - Time.deltaTime, 0.1f, 1f);
                }
                
                appearTimer = Mathf.Clamp(appearTimer - Time.deltaTime, 0f, 1f);

                for (int i = 0; i < cloakMaterials.Count; i++)
                {
                    // If the current material is null go to the next one
                    if (cloakMaterials[i] == null) continue;

                    cloakMaterials[i].SetFloat("_Alpha", alpha);

                    //Play effect backwards
                    cloakMaterials[i].SetFloat("_AppearTime", appearTimer);
                }

                if (alpha <= 0.1f)
                {
                    // We are cloaked
                    IsCloaked = true;

                    cloak = false;
                }
            } else
            {
                alpha = Mathf.Clamp(alpha + Time.deltaTime, 0.2f, 1f);

                if (alpha >= 0.75f)
                {
                    appearTimer = Mathf.Clamp(appearTimer + Time.deltaTime, 0f, 1f);
                }

                // Go through all the materials and change it's setings
                for (int i = 0; i < cloakMaterials.Count; i++)
                {
                    // If the current material is null go to the next one
                    if (cloakMaterials[i] == null) continue;

                    cloakMaterials[i].SetFloat("_Alpha", alpha);

                    //Play effect forward
                    cloakMaterials[i].SetFloat("_AppearTime", appearTimer);
                }

                if (appearTimer >= 1f)
                {
                    // We are not cloaked
                    IsCloaked = false;

                    cloak = false;

                    SwitchMaterialRecursive(gameObject, false);
                }
            }
        }
    }

    /// <summary>
    /// Add this as a listner
    /// </summary>
    private void AddMeAsListner()
    {
        // Add ourself as a listner to the shoot event
        playerShoot.playerShoot.AddListener(RemoveCloaking);

        // For each ability the player has
        for (int i = 0; i < me.Abilities.Length; i++)
        {
            // Add ourself as a listner to the casting event
            me.Abilities[i]?.PlayerCast?.AddListener(RemoveCloaking);
        }
    }

    /// <summary>
    /// Removes the cloacking
    /// </summary>
    private void RemoveCloaking()
    {
        if (IsCloaked)
        {
            // Resets the cloak timer
            cloakTimer = 0;

            // Removes the cloaking
            PhotonNetwork.RaiseEvent(instantiationEventCode, null,
                new RaiseEventOptions()
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                }, SendOptions.SendReliable);
        }
    }

    /// <summary>
    /// Recursevly gets all the childs of this object
    /// </summary>
    /// <param name="obj"></param>
    private void GetChildRecursive(GameObject obj)
    {
        if (obj == null)
            return;

        // For each child this has 
        foreach (Transform child in obj.transform)
        {
            // If it's null return
            if (null == child)
                continue;

            // If this object has a renderer
            if (child.TryGetComponent(out currentRenderer))
            {
                if (currentRenderer.materials.Length >= 1)
                {
                    if (child.gameObject.layer == 16) continue;

                    // Go through all the materials in all childs
                    foreach (Material m in child.GetComponent<Renderer>().materials)
                    {
                        // If the material is not null
                        if (m != null)
                        {
                            // Add it material to the list
                            listOfMaterials.Add(m);
                        }
                    }
                }
            }

            // Get all the childs of the current object
            GetChildRecursive(child.gameObject);
        }
    }

    private void SwitchMaterialRecursive(GameObject obj, bool toCloak = false)
    {
        if (obj == null)
            return;

        // For each child this has 
        foreach (Transform child in obj.transform)
        {
            // If it's null return
            if (null == child)
                continue;

            // If this object has a renderer
            if (child.TryGetComponent(out currentRenderer))
            {
                if (currentRenderer.materials.Length >= 1)
                {
                    if (child.gameObject.layer == 16) continue;

                    Renderer childRenderer = child.GetComponent<Renderer>();

                    Material[] materials = childRenderer.materials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        // If the material is not null
                        if (materials[i] != null)
                        {
                            if (toCloak)
                            {
                                for (int a = 0; a < cloakMaterials.Count; a++)
                                {
                                    if (materials[i].name.Replace("(Instance)", "").Replace(" ", "") == cloakMaterials[a].name.Replace("(Instance)", "").Replace(" ", ""))
                                    {
                                        materials[i] = cloakMaterials[a];
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (int a = 0; a < materialBackUp.Count; a++)
                                {
                                    if (materials[i].name.Replace("(Instance)", "").Replace(" ", "") == materialBackUp[a].name.Replace("(Instance)", "").Replace(" ", ""))
                                    {
                                        materials[i] = materialBackUp[a];
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    childRenderer.materials = materials;
                }
            }

            // Get all the childs of the current object
            SwitchMaterialRecursive(child.gameObject, toCloak);
        }
    }

    /// <summary>
    /// RPC that Cloaks the player at a network level making him invisible
    /// </summary>
    public void Cloaking()
    {
        if (!IsCloaked)
        {
            SwitchMaterialRecursive(gameObject, true);
        }

        cloak = true;
    }

    /// <summary>
    /// OnEvent call
    /// </summary>
    /// <param name="photonEvent">The photon event</param>
    private void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == instantiationEventCode)
        {
            Cloaking();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(cloak);
            stream.SendNext(IsCloaked);
        }
        else if (stream.IsReading)
        {
            cloak = (bool)stream.ReceiveNext();
            IsCloaked = (bool)stream.ReceiveNext();
        }
    }
}
