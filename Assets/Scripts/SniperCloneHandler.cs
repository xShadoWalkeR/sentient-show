﻿using UnityEngine;
using UnityEngine.Rendering;
using Photon.Pun;

public class SniperCloneHandler : MonoBehaviourPunCallbacks {

    [SerializeField] private Renderer[] models;

    void Start() {

        if (photonView.IsMine) {

            ActivateRenderers(false);
        }
    }

    [PunRPC]
    private void RPC_SetParent(string parentName) {

        transform.SetParent(GameObject.Find(parentName).transform);

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

    public void ActivateRenderers(bool value) {

        if (value) {

            foreach (Renderer r in models) {

                r.shadowCastingMode = ShadowCastingMode.On;
            }

        } else {

            foreach (Renderer r in models) {

                r.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
            }
        }
    }
}
