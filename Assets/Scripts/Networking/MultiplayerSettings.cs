﻿using UnityEngine;

/// <summary>
/// Handles the multiplayer settings
/// </summary>
public class MultiplayerSettings : MonoBehaviour
{
    // Singleton
    public static MultiplayerSettings mpSettings;

    public bool DelayStart;
    public int MaxPlayer;
    public int MenuScene;
    public int MultiplayerScene;

    /// <summary>
    /// Called before the start
    /// </summary>
    private void Awake()
    {
        if (MultiplayerSettings.mpSettings == null)
        {
            MultiplayerSettings.mpSettings = this;
        } else
        {
            if (MultiplayerSettings.mpSettings != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
