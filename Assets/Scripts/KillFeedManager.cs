﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Pun;

/// <summary>
/// Manages and updates the killfeed
/// </summary>
public class KillFeedManager : MonoBehaviourPunCallbacks
{
    // How long it takes till the the feed despawns
    private const int TIME_TO_DESPAWN = 8;

    // The prefab for a feed row
    [SerializeField] private GameObject feedPrefab;

    // The killfeed grid
    [SerializeField] private GameObject killFeed;

    // List with all the feed rows
    private List<GameObject> feeds;

    // List with the timers to despawn the feed
    private List<float> timers;

    // A variable to hold a temporary game object
    private GameObject teamporary;

    /// <summary>
    /// Called at the start
    /// </summary>
    private void Start()
    {
        // Initialize both lists
        feeds = new List<GameObject>();
        timers = new List<float>();
    }

    /// <summary>
    /// Called once every frame
    /// </summary>
    private void Update()
    {
        // Go through every timer on the list
        for (int i = timers.Count - 1; i >= 0; i--)
        {
            // Decrease it by 1 each second
            timers[i] += Time.deltaTime;

            // If the timer has reached the limit
            if (timers[i] >= TIME_TO_DESPAWN)
            {
                // Destroys the feed row
                Destroy(feeds[i]);
            }
        }
    }

    /// <summary>
    /// RPC to add a new feed row to the killfeed
    /// </summary>
    /// <param name="killer">The killer's name</param>
    /// <param name="victim">The victim's</param>
    /// <param name="killerTag">The killer's tag</param>
    [PunRPC]
    public void AddToFeed(string killer, string victim, string killerTag)
    {
        feeds.Add(teamporary = Instantiate(feedPrefab, killFeed.transform));
        timers.Add(0f);

        teamporary.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = killer;
        teamporary.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = killerTag == Tags.ENEMY_TAG ? Color.blue : Color.red;

        teamporary.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = victim;
        teamporary.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = killerTag == Tags.ENEMY_TAG ? Color.red : Color.blue;
    }
}
