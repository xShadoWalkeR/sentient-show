﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GaussianBlurSettings : MonoBehaviour
{

    public float blurAmount = 0f;
    public float lightness = 1f;
    public float saturation = 1f;

    private Material m;

    // Start is called before the first frame update
    void Start()
    {
        m = gameObject.GetComponent<Image>().material;
    }

    // Update is called once per frame
    void Update()
    {
        m.SetFloat("_Iterations", blurAmount);
        m.SetFloat("_Lightness", lightness);
        m.SetFloat("_Saturation", saturation);
    }
}
